var map_canvas;
var markers = [];



function initMapCanvas(){
	var options = { 
          lat: -6.9486877,
          lng : 109.5421919,
          zoom : 11, 
          el: '#map_canvas',
          height : "300px",
          disableDoubleClickZoom : true,
          'width' : '100%',
          'streetViewControl' : false,
          'zoomControlOptions' : {'position' : google.maps.ControlPosition.LEFT_CENTER, 'style' : google.maps.ZoomControlStyle.SMALL}
    }; 
    return new GMaps(options);
}

function getModelActive() {
   return $('.tabs .active').data('model');   
}


function getDataFromTable(){
	return table[getModelActive()].data().toArray().map(function(o){
		var gambar = o.tempat.gambars[0] ? o.tempat.gambars[0].nama : 'guest.png';
		return {
			lat: o.tempat.latitude,
			lng: o.tempat.longitude,
			infoWindow : {
				content : "<img src='img/small/"+gambar+"' align='left'>"+
                  "<b>Nama</b> : "+o.tempat.nama+"<br>"+
                  "<b>Alamat</b> : "+o.tempat.alamat+"<br>"+
                  '<a href="#modal-hapus" data-id="'+o.id+'" class=" modal-trigger waves-effect waves-light btn-small btn-map-delete btn-flat btn-floating btn-small right"><i class="mdi-action-delete"></i></a>'+
                  '<a href="#modal-form" data-jenis="edit" data-id="'+o.id+'" data-lat="'+o.tempat.latitude+'"  data-lng="'+o.tempat.longitude+'" class="waves-effect waves-light btn-small btn-flat btn-floating btn-small right"><i class="mdi-editor-border-color"></i></a>'+
                  '<a href="#modal-detail" data-id="'+o.id+'" class="modal-trigger waves-effect waves-light btn-small btn-flat btn-floating btn-small right"><i class="mdi-action-visibility"></i></a>',
			}
		};
	});
}


function setMarkers(e){

	if (markers.length > 0 ) {
		clearMarkers();	
	}
	if (getDataFromTable().length > 0) {

		markers = map_canvas.addMarkers(getDataFromTable());
	}
}

function clearMarkers(){
	for (var i = markers.length-1, marker; marker=markers[i]; i--) {
		marker.setMap(null);
		markers.pop();
	}	
	markers = [];
}

$(document).ready(function() {
	map_canvas = initMapCanvas();
	table[getModelActive()].on('init.dt',setMarkers);
	$(document).on('click', '.tabs a',setMarkers);

});