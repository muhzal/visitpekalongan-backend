$(document).ready(function() {
	$('.carousel').carousel();
	review.init();

});



window.review = {
	init: function() {
		$(document).on('click', '.pagination li a', this.onPaginateClick).on('click', '.pindah-wisata',function(e){
			e.preventDefault();
			var id = $('#select-wisata').val();
			if (id) {
				window.location.replace(id);
			}
		});
		$(window).on('hashchange', this.onHashChange);
	},
	getReview: function(page) {
		review.show.loader();
		review.hide.review();
		$.ajax({
			url: '?page=' + page,
		}).done(function(data) {
			$('#review-item').html(data);
			location.hash = page;
			review.hide.loader();
			review.show.review();
		}).fail(function() {
			alert('Posts could not be loaded.');
		});
	},
	onPaginateClick: function(e) {
		var url = $(this).attr('href') || null;
		if (url) {			
			review.getReview(url.split('page=')[1]);	
		}
		e.preventDefault();
	},
	onHashChange: function() {
		if (window.location.hash) {
			var page = window.location.hash.replace('#', '');
			if (page == Number.NaN || page <= 0) {
				return false;
			} else {
				review.getReview(page);
			}
		}
	},
	show: {
		loader: function() {
			$('#review-loader').removeClass('hide');
		},
		review: function() {
			$('#review-item').removeClass('hide');
		},
	},
	hide: {
		loader: function() {
			$('#review-loader').addClass('hide');
		},
		review: function() {
			$('#review-item').addClass('hide');
		},
	}
}