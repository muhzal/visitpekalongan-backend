$(function() {

	$('.materialboxed').materialbox();


	$('select').addClass('browser-default');

	$('ul.tabs').tabs();


	$(window).load(function() {
		setTimeout(function() {
			$('body').addClass('loaded');
		}, 200);
	});


	$('.sidebar-collapse').sideNav({
		menuWidth: 80,
		edge: 'left', // Choose the horizontal origin      
		closeOnClick: false,
	});

});