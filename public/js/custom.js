$(function() {

  $('.materialboxed').materialbox();
  $('select').addClass('browser-default');
  $('ul.tabs').tabs();  

  $(window).load(function() {
    setTimeout(function() {
      $('body').addClass('loaded');
    }, 200);
  });


  /**
   *
   * Sidebar navigation
   *
   */


  $('.sidebar-collapse').sideNav({
    menuWidth: 80,
    edge: 'left', // Choose the horizontal origin      
    closeOnClick: false,
  });


  $(window).resize(function() {

    var table_dom = $('table');
    table_dom.each(function(index, el) {
      $(this).width($(this).parent().width());
    });
  });

}); //closed



function getModel() {
  return $('.tabs .active').data('model');
}



/**
 *
 * GOOGLE MAP
 *
 */

var marker, map, latitude, longitude, array, marker_open;
var url_edit;
var toas_msg;
var pekalongan = {
  lat: -6.9486877,
  lng: 109.5421919,
  zoom: 11,
  el: '#map_canvas',
  height: "300px",
  disableDoubleClickZoom: true,
  'width': '100%',
  'streetViewControl': false,
  'zoomControlOptions': {
    'position': google.maps.ControlPosition.LEFT_CENTER,
    'style': google.maps.ZoomControlStyle.SMALL
  }
};
var Pekalongan = function(el) {
  this.lat = -6.9486877;
  this.lng = 109.5421919;
  this.zoom = 11;
  this.el = el;
  this.height = "300px";
  this.disableDoubleClickZoom = true;
  this.width = '100%';
  this.streetViewControl = false;
  this.zoomControlOptions = {
    'position': google.maps.ControlPosition.LEFT_CENTER,
    'style': google.maps.ZoomControlStyle.SMALL
  };
};

function multipleMarker(map, data) {
  var opt = {},
    gambar;
  for (var i = 0; i < data.length; i++) {
    opt.lat = data[i].tempat.latitude;
    opt.lng = data[i].tempat.longitude;
    opt.click = setMarkerOpen;
    gambar = data[i].tempat.gambars[0] ? data[i].tempat.gambars[0].nama : 'guest.png';
    opt.infoWindow = {
      content: "<img src='/img/small/" + gambar + "' align='left'>" +
        "<b>Nama</b> : " + data[i].tempat.nama + "<br>" +
        "<b>Alamat</b> : " + data[i].tempat.alamat + "<br>" +
        '<a href="#modal-hapus-' + getModelName(data[i].tempat.tempatable_type).toLowerCase() + '" data-id="' + data[i].id + '" class=" modal-trigger waves-effect waves-light btn-small btn-map-delete btn-flat btn-floating btn-small right"><i class="mdi-action-delete"></i></a>' +
        '<a href="../' + getModelName(data[i].tempat.tempatable_type).toLowerCase() + '/' + data[i].id + '/edit" class="waves-effect waves-light btn-small btn-flat btn-floating btn-small right"><i class="mdi-editor-border-color"></i></a>' +
        '<a href="#modal-detail" data-id="' + data[i].id + '" class="modal-trigger waves-effect waves-light btn-small btn-flat btn-floating btn-small right"><i class="mdi-action-visibility"></i></a>',
    };
    map.addMarker(opt);
  }
}

function getModelName(data) {
  var x = data.split('\\');
  return x[x.length - 1];
}

function setMarkerOpen(e) {
  marker_open = e;
}

function removeMarkerOpen() {
  marker_open.setMap(null);
  marker_open = null;
}



function openToastForm() {
  $('#toast-container').html(' ');
  Materialize.toast(toas_msg + '<a href="#modal-tambah" class="btn btn-float btn-toast modal-trigger"> Ya </a> <a href="#!" onClick="History.back();"> Kembali</a>');
}


/**
 *
 * prosedur untuk re-inisialisasi corousel
 *
 */

function recallCarousel() {
  var car = $('.carousel');

  if (car.children().length > 0) {
    car.removeClass('initialized');
    car.carousel();
  }
}


var imagesName = [];
var storedFiles = [];

function handleFileSelect(e) {

  var preview_dom = $("#images-preview");
  var files = e.target.files;
  var filesArr = Array.prototype.slice.call(files);

  filesArr.forEach(function(f) {
    if (!f.type.match("image.*")) {
      return false;
    }
    if ((storedFiles.length + imagesName.length) < 5) {
      storedFiles.push(f);
      console.log(storedFiles.length);
      var reader = new FileReader();

      reader.onload = function(e) {
        var html = '<div class="col s6 m4 l2  image-item">' +
          '<button type="button" class="btn right btn-floating black btn-image-close" data-name="' + f.name + '"><i class="mdi-content-clear"></i></button>' +
          '<img src="' + e.target.result + '">' +
          '</div>';
        preview_dom.prepend(html);
        // $(html).ready(recallCarousel);
      };
      reader.readAsDataURL(f);

    } else {
      Materialize.toast(f.name + ' tidak bisa ditambahkan, maksimal 5 foto', 3000, 'red');

    }
  });
}

function removeFile(e) {
  var file = $(this).data("name");

  if (imagesName.length) {
    var j = imagesName.indexOf(file);
    if (j >= 0) {
      imagesName.splice(j, 1);
      $(this).parent().remove();
    }
  }

  for (var i = 0; i < storedFiles.length; i++) {
    if (storedFiles[i].name == file) {
      storedFiles.splice(i, 1);
      $(this).parent().remove();
      // recallCarousel();
      // console.log(storedFiles.length);
      break;
    }
  }
}

function ajaxForm(e) {
  e.stopPropagation();
  e.preventDefault();
  var url = $(this).attr("action");
  var form_data = document.querySelector('#form-tempat');
  // var form_data = $(this).serializeArray();

  ModalProgress.show();
  var data = new FormData(form_data);
  var xhr = new XMLHttpRequest();
  console.log(storedFiles.length);

  for (var i = 0, len = storedFiles.length; i < len; i++) {
    data.append('images[' + i + ']', storedFiles[i]);
  }


  if (imagesName.length > 0) {
    for (var c = imagesName.length - 1; c >= 0; c--) {
      data.append('imagesName[' + c + ']', imagesName[c]);
      imagesName.pop();
    }
  }

  xhr.open('POST', url, true);
  xhr.onload = function(e) {
    console.log(this);
    if (this.status == 200) {
      ajaxSuccess();
      // $('body').addClass('loaded');
      // window.location.replace(this.responseText);                          
    } else {
      ModalProgress.hide();
      // $('body').addClass('loaded');
      // $('html').html(this.responseText);
    }
  };
  xhr.send(data);

}

function ajaxSuccess(a) {

  ModalProgress.hide();
  $('.modal').closeModal({
    complete: onModalFormHide,
  });
  storedFiles = [];
  imagesName = [];
  reloadTable();


  setTimeout(function() {
    Materialize.toast('Selamat data berhasil di tambahkan', 3000, 'green');
    setMarkers();
  }, 1000);
}
/**
 *
 * Datatables
 *
 */

// var table_spbu, table_atm, table_wisata_alam, table_wisata_religi, table_wisata_budaya, table_hotel, table_restoran, table_event, table_belanja;
var table = {};

function btnAksi(data, type, full, meta) {
  var btn_show = '<a href="#modal-detail"  data-id="' + data.id + '" class=" modal-trigger btn btn-small btn-floating waves-effect waves-light blue"><i class="small mdi-action-pageview"></i></a>';
  var btn_edit = '<a href="#modal-form" data-jenis="edit" data-id="' + data.id + '" data-lat="' + data.tempat.latitude + '"  data-lng="' + data.tempat.longitude + '" class="modal-trigger btn btn-small btn-floating waves-effect waves-light green"><i class="small mdi-editor-mode-edit"></i></a>';
  var btn_hapus = '<a href="#modal-hapus" data-id="' + data.id + '" class="modal-trigger btn btn-small btn-floating modal-trigger waves-effect waves-light red"><i class="small mdi-action-delete"></i></a>';
  return btn_show + btn_edit + btn_hapus;
}

function initDatatables(argument) {

  var table_dom = $('table');
  table_dom.each(function(index, el) {
    $(this).width($(this).parent().width());
  });
}

function reloadTable() {
  table[getModel()].ajax.reload();

}


/*=============================
=            MODAL            =
=============================*/


var map_content = $('#modal-form .modal-map-content'),
  form_content = $('#modal-form .modal-form-content'),
  map_form, form_tempat = [];


function disableButton(x) { //boolean
  $('form button').prop('disabled', x);
}

function removeToast() {
  $('#toast-container').children().remove();
}


/**
 *
 * MODAL HAPUS
 *
 */

function onModalHapus(e) {
  var modal = $(this).attr('href');
  $(modal).openModal();
  var url = '' + getModel() + '/' + $(this).data('id');
  $(modal).find('form').attr('action', url);
}


/**
 *
 * MODAL Form
 *
 */



var marker_form_opt = {};
var modal_jenis;
var form_url, modal_form = $('#modal-form form');

function openModalForm(e) {
  modal_jenis = $(this).data('jenis');
  if (modal_jenis == 'edit') {
    marker_form_opt.lat = $(this).data('lat');
    marker_form_opt.lng = $(this).data('lng');
    form_url = '' + getModel() + '/' + $(this).data('id') + '/edit';
    $('#modal-form h5').html('Edit Data Lokasi');
  } else {
    form_url = '' + getModel() + '/create';
    $('#modal-form h5').html('Tambah Lokasi ' + getModel() + ' Baru');
  }
  $('#modal-form').openModal({
    ready: onModalFormReady,
    complete: onModalFormHide,
  });


}

function setImagesName() {
  imagesName = [];
  $('#images-preview button').each(function() {
    imagesName.push($(this).data('name'));
  });
}

function onModalFormReady() {
  if (map_form == null) {
    initMapForm();
  }

  showMapForm();
}

function onModalFormHide() {
  removeToast();
  // form_content.children().remove();  
  $('#modal-form form').remove();
  removeMarkerForm();
  $('.btn-next').prop('disabled', true);
  if (ModalProgress.status) ModalProgress.hide();
}

function loadForm(e) {

  // if (form_content.children().length == 0) { 
  if (
    $('#modal-form form').length == 0) {
    removeToast();
    $('.btn-next').prop('disabled', true);
    ModalProgress.show();
    // form_content.load(form_url, onLoadForm);
    $.get(form_url, onLoadForm).fail(function(err) {
      if (ModalProgress.status) ModalProgress.hide();
      alert('terjadi kesalahan, coba lagi');
      $('.btn-next').prop('disabled', false);
    });
  } else {
    showForm();
  }
}

function onLoadForm(e, t, x) {
  if (x.status == 200) {
    if ($('#modal-form form').length > 0) {
      $('#modal-form form').remove();
    }
    $('#modal-form').append(e);
    ModalProgress.hide();
    showForm();
    initForm();
  } else {
    alert('something wrong, try again or refresh the page');
  }
}

function initForm() {
  $("#images").on("change", handleFileSelect);
  $(document).on('click', '.btn-image-close', removeFile);
  // $(':input').characterCounter();
  $('.input-field label').addClass('active');
  if (modal_jenis == "edit") {
    setImagesName();
  }
  initSelectize();
}


function showForm() {
  removeToast();
  map_content.addClass('hiddendiv');
  // form_content.removeClass('hiddendiv');
  $('#modal-form form').removeClass('hiddendiv');
  $('input[name="tempat[latitude]"').val(marker_form.position.lat());
  $('input[name="tempat[longitude]"').val(marker_form.position.lng());
}



/**
 *
 * MAP FORM
 *
 */
var marker_form;

function initMapForm() {
  var a = new Pekalongan('#map_form');
  a.height = '100%';
  a.click = setMarkerForm;
  map_form = new GMaps(a);
}

function setMarkerForm(e) {
  latitude = e.latLng.lat();
  longitude = e.latLng.lng();
  var option = {
    lat: latitude,
    lng: longitude,
    map: map_form.map
  };
  removeMarkerForm();
  marker_form = map_form.createMarker(option);
  $('.btn-next').prop('disabled', false);
}

function removeMarkerForm() {
  if (marker_form) {
    marker_form.setMap(null);
    marker_form = null;
  }
}

function showMapForm() {

  if (modal_jenis == "edit" && marker_form == null) {
    marker_form_opt.map = map_form.map;
    marker_form = map_form.createMarker(marker_form_opt);
    Materialize.toast('Silahkan klik pada peta untuk mengubah tempat', 99999999);
  } else {
    Materialize.toast('Silahkan klik pada peta tempat yang akan ditambahkan', 99999999);
  }
  // form_content.addClass('hiddendiv');
  $('#modal-form form').addClass('hiddendiv');
  map_content.removeClass('hiddendiv');
  if (marker_form) {
    $('.btn-next').prop('disabled', false);
  }
}


/**
 *
 * Progress Bar
 *
 */

var ModalProgress = {
  dom: $('.modal > .progress'),
  show: showProgress,
  hide: hideProgress,
  status: false,
};

function showProgress() {
  disableButton(true);
  this.dom.removeClass('hiddendiv');
  this.status = true;
}

function hideProgress() {
  disableButton(false);
  this.dom.addClass('hiddendiv');

  this.status = false;
}



function initSelectize() {
  if (getModel() == 'restoran') {
    $('select.selectize').selectize({
      plugins: ['remove_button'],
      create: true,
    });
  } else if (getModel() == 'wisata') {
    $('.waktu, .jenis-wisata,.fasilitas').selectize({
      plugins: ['remove_button'],
      create: true,
    });
    if (modal_jenis == 'edit' && $(':input[name="is_tiket"]:checked').length > 0) {
      is_init = false;
      initOptiket();
      indextiket = parseInt($('.card-panel .tiket-item:last-child').data('id')) + 1;
    }

  } else {
    $('select.selectize').selectize();
  }
}



var indextiket = 0,
  opt = {},
  $selectize;

function showTiketPanel(e) {
  var tiket = $('#tiket-panel');
  var pan = $('#tiket-panel .card-panel');
  tiket.toggle();
  if (pan.children().length === 0) {
    tambahTiket();
  }
  // if ( $(':input[name="is_tiket"]:checked').length > 0 && modal_jenis != 'edit') {         
  //      tambahTiket();         
  //  }else{              
  //        indextiket=0;
  //        pan.children().remove();
  //  }  
}

function tambahTiket() {
  var pan = $('#tiket-panel .card-panel');
  var el = '<div class="row tiket-item">' +
    '<div class="input-field col s4">' +
    '<select id="selectize" placeholder="Masukkan jenis tiket..." class="selectize validate browser-default jenis-tiket" name="tiket[' + indextiket + '][nama]" required="required">' +
    // '<input type="text" class="validate" name="tiket['+indextiket+'][nama]" required="required">'+
    '  <option value="">pilih jenis tiket...</option>' +
    // '<option value="Tiket Masuk" selected>Tiket Masuk</option>'+
    // '<option value="Tiket Parkir">Tiket Parkir</option> '+
    '</select>' +
    '<label for="tiket[nama]" class="active">Jenis Tiket</label>' +
    '</div>' +
    '<div class="input-field col s4">' +
    '<input type="number" class="validate" name="tiket[' + indextiket + '][harga]" required="required">' +
    '<label for="harga">Harga</label>' +
    '</div>' +
    '<div class="input-field col s3">' +
    '<select id="selectize" class="selectize validate default-browser operasional" placeholder="Masukkan jenis tiket..."  name="tiket[' + indextiket + '][operasional]" required="required"></select>' +
    '<label class="active">Hari</label>' +
    '</div>' +
    '<div class="input-field col s1">' +
    '<button type="button" class="btn-small btn-tiket-clear btn btn-floating red"><i class="mdi-content-clear"></i></button>' +
    '</div>' +
    '</div>';
  pan.append(el);
  initOptiket();
  indextiket++;
}

function hapusTiket(e) {
  if ($('#tiket-panel .card-panel .tiket-item').length > 1) {
    $(e.target).closest('.tiket-item').remove();
  }
}

var operasional = [],
  newop = [],
  tikets = [],
  newtikets = [],
  is_init = false;

function initOptiket() {
  var o = '#tiket-panel .card-panel .tiket-item:last-child select.operasional';
  var t = '#tiket-panel .card-panel .tiket-item:last-child select.jenis-tiket';
  if (modal_jenis == 'edit' && is_init === false) {
    is_init = true;
    o = 'select.operasional';
    t = 'select.jenis-tiket';
  }
  var x = $(o).selectize({
    valueField: 'operasional',
    labelField: 'operasional',
    searchField: 'operasional',
    // create: true,
    preload: true,
    create: function(n) {
      var a = {
        'operasional': n
      };
      newop.push(a);
      addOptionOp(a);
      return a;
    },
    load: selectizeTiket,
  });
  newop.forEach(function(element, index) {
    x[0].selectize.addOption(element);
  });
  operasional.push(x[0].selectize);

  var y = $(t).selectize({
    valueField: 'nama',
    labelField: 'nama',
    searchField: 'nama',
    // create: true,
    preload: true,
    create: function(n) {
      var a = {
        'nama': n
      };
      newtikets.push(a);
      addOptionTiket(a);
      return a;
    },
    load: selectizeTiket,
  });
  newtikets.forEach(function(element, index) {
    y[0].selectize.addOption(element);
  });
  tikets.push(y[0].selectize);
}

function addOptionOp(x) {
  operasional.forEach(function(e, i, arr) {
    e.addOption(x);
  });
}

function addOptionTiket(x) {
  tikets.forEach(function(e, i, arr) {
    e.addOption(x);
  });
}

function selectizeTiket(query, callback) {
  $.ajax({
    url: '/json/optiket',
    type: 'GET',
    error: function() {
      callback();
    },
    success: function(res) {
      callback(res);
    }
  });
}