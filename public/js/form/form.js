String.prototype.capitalize = function() {
	var arr = this.split(' ');
	var result = arr.map(function(v) {
		v = v.toLowerCase();
		return v.charAt(0).toUpperCase() + v.slice(1);
	});

	return result.join(' ');
};

$(function() {
	var current = location.pathname;
	$('.side-nav li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href').indexOf(current) !== -1) {
			$this.parent().addClass('active');
		}
	});
	Form.init();
});

window.Form = {
	init: function() {
		// peta.init();
		table.init('atm');
		table.init('event');
		table.init('restoran');
		table.init('penginapan');
		table.init('spbu');
		table.init('wisata');
		table.init('transportasi');
		table.init('belanja');
		$(document).on('click', '.tabs a', this.map.main.marker.change);
		$(document).on('click', '.btn-next', this.modal.button.next);
		$(document).on('submit', '#modal-hapus form', Form.hapus.submit);
		$(document).on('click', '.modal-trigger[href="#modal-hapus"]', Form.hapus.button);
		this.modal.init();
		$(document).on("click", '.btn-back', Form.modal.button.back);
		$(document).on("submit", '#form-tempat', Form.modal.submit);
		$(document).on("change", '#images', Form.file.handle);
		$(document).on('click', '.btn-image-close', Form.file.remove);
		$(document).on('click', '#is_tiket', Form.modal.button.tiket);
		$(document).on('click', '.btn-tiket-clear', Form.modal.button.hapusTiket);
		$(document).on('click', '.btn-tiket-add', Form.modal.button.addTiket);
		$(document).on('click', '.publish', Form.publish.onClick);

		$(document).on('click', '.modal-trigger[href="#modal-detail"]', Form.detail.init);
		$(document).on('click', 'a[href="#tab-peta"]', Form.detail.tabmap);
		$(document).on('click', '#btn-edit-detail', Form.detail.close);
		$(document).on('click', '.btn-delete-review', Form.onDeleteReview);
		if (typeof google !== 'undefined')
			this.map.main.initMap();

	},
	publish: {
		onClick: function(e) {
			var self = Form.publish;
			self.dom = $(this);
			self.v = self.dom.is(':checked') ? 1 : 0;
			var id = self.dom.data('id');
			$.post('terbitkan', {
					_method: 'POST',
					id: id,
					status: self.v,
				}).done(self.done)
				.fail(self.fail);
		},
		done: function(a) {
			if (a.publish == 1)
				self.dom.prop('checked', true);
			else
				self.dom.prop('checked', false);
		},
		fail: function(a) {
			var self = Form.publish;
			if (self.v === 1) {
				self.dom.prop('checked', false);
			} else {
				self.dom.prop('checked', true);
			}
		},
	},
	detail: {
		init: function(e) {
			var a = Form.detail;
			a.model = $(this).data('model');
			a.id = $(this).data('id');
			a.lat = $(this).data('lat');
			a.lng = $(this).data('lng');
			$('#modal-detail').openModal({
				dismissible: false,
				opacity: '.5',
				in_duration: '300',
				out_duration: '200',
				ready: a.ready,
				complete: a.exit,
			});
		},
		ready: function() {
			var _ = Form.detail;
			$('#modal-detail .tabs').tabs('select_tab', 'tab-info');
			$('#modal-detail .modal-header h5').html('Detail ' + _.model);
			_.load();
			if (_.model === 'wisata') {
				$('a[href="#tab-review"]').removeClass('hide');
				_.loadReview();
			}

		},
		exit: function() {
			$('#tab-info,#tab-review ul').html(' ');
			$('#peta-detail').html(' ');
			// $('#modal-detail .modal-loader').removeClass('hide');
			$('a[href="#tab-review"]').addClass('hide');
			Form.detail.map = null;
		},
		close: function() {
			$('#modal-detail').closeModal({
				complete: Form.detail.exit,
			});
		},
		swiper: function() {
			this.objswiper = new Swiper('.swiper-container', {
				nextButton: '.swiper-button-next',
				prevButton: '.swiper-button-prev',
				pagination: '.swiper-pagination',
				paginationClickable: true,
				// Disable preloading of all images
				preloadImages: false,
				autoplay: 3000,
				effect: 'slide',
				// Enable lazy loading
				lazyLoading: true
			});
		},
		load: function() {
			var url = this.model + '/' + this.id;
			$('#modal-detail .modal-loader').removeClass('hide');
			$('#tab-info').load(url, {
				_method: 'GET'
			}, function() {
				Form.detail.swiper();
				$('#modal-detail .modal-loader').addClass('hide');
				$('.collapsible').collapsible({
					accordion: false
				});
			});
		},
		loadReview: function() {
			var url = 'wisata/reviews';
			var id = this.id;
			$('#modal-detail .modal-loader').removeClass('hide');
			$('#tab-review ul').html(' ');
			$('#tab-review ul').load(url, {
				_method: 'POST',
				id: id
			}, function() {

				$('#modal-detail .modal-loader').addClass('hide');
			});
		},
		tabmap: function() {
			if (Form.detail.map === null || typeof Form.detail.map === 'undefined') {
				Form.detail.initmap();
			}
		},
		initmap: function() {
			var icon = peta.options.icon[Form.detail.model.toLowerCase()] || peta.options.icon.me;
			this.map = peta.init('peta-detail');
			this.marker = peta.marker.create(peta.latLng(this.lat, this.lng));
			this.marker.setMap(this.map);
			this.marker.setAnimation(google.maps.Animation.DROP);
			this.marker.setIcon(icon);
			this.map.setCenter(this.marker.getPosition());
			this.map.setZoom(18);
		},
	},
	file: {
		current: [],
		new: [],
		getCurrentImage: function() {
			var x = [];
			$('.image-item').map(function(a) {
				return x.push($(this).data('name'));
			});
			return x;
		},
		preview: function() {
			return $('#images-preview');
		},
		dom: function(name, path) {
			return '<div class="col s6 m4 l4  image-item">' +
				'<button type="button" class="btn right btn-floating black btn-image-close" data-name="' + name + '">' +
				'<i class="material-icons">cancel</i></button>' +
				'<img src="' + path + '" class="hoverable">' +
				'</div>';
		},
		handle: function(e) {
			var files = Array.prototype.slice.call(e.target.files);
			var self = Form.file;
			files.forEach(function(file, index) {

				var reader = new FileReader();
				if (!file.type.match("image.*"))
					Form.modal.show.fotoType(file.name);
				else if (file.size > 2000000) {
					Form.modal.show.fotoSize(file.name);
				} else if (self.current.length + self.new.length < 5) {
					self.new.push(file);
					reader.onload = function(e) {
						var html = self.dom(file.name, e.target.result);
						self.preview().prepend(html);
					};
					reader.readAsDataURL(file);
				} else {
					Form.modal.show.foto(file.name);
				}

			});
		},
		remove: function(e) {
			var file = $(this).data("name");
			var imagesName = Form.file.current || [];
			var storedFiles = Form.file.new || [];
			if (imagesName.length) {
				var j = imagesName.indexOf(file);
				if (j >= 0) {
					imagesName.splice(j, 1);
					$(this).parent().remove();
				}
			}

			for (var i = 0; i < storedFiles.length; i++) {
				if (storedFiles[i].name == file) {
					storedFiles.splice(i, 1);
					$(this).parent().remove();
					// recallCarousel();
					// console.log(storedFiles.length);
					break;
				}
			}
		},
	},
	hapus: {
		init: function() {
			$('.modal-trigger[href="#modal-hapus"]').leanModal({
				dismissible: false,
				opacity: '.5',
				in_duration: '300',
				out_duration: '200',
				// ready: Form.hapus.ready,
				complete: Form.hapus.exit,
			});

		},
		button: function(e) {
			//dipanggil di table.kolom.initButton 
			Form.hapus.id = $(this).data('id');
			Form.modal.show.modalHapus();
		},
		exit: function() {},
		submit: function(e) {
			e.stopPropagation();
			e.preventDefault();
			var action = Form.page.active() + "\\" + Form.hapus.id;
			console.log(action);
			var form_data = $(this).serializeArray();
			$.post(action, form_data).done(Form.hapus.sukses).fail(Form.hapus.gagal);
		},
		sukses: function(d, m, x) {
			if (x.status === 200) {
				Form.modal.hide.modalHapus();
				Form.modal.show.hapus();
				table[Form.page.active()].ajax.reload();
			}
		},
		gagal: function(e) {
			console.log(e.responseText);
		},
	},
	page: {
		active: function() {
			return $(".data-tempat.tabs .tab a.active").data('model');
		},
	},
	map: {
		main: {
			id: 'map_canvas',
			initMap: function() {
				this.canvas = peta.init(this.id);

			},
			marker: {
				markers: {
					atm: [],
					spbu: [],
					wisata: [],
					penginapan: [],
					restoran: [],
					event: [],
					belanja: [],
					transportasi: [],
				},
				init: function(name, data) {
					var m = this.markers[name];
					if (m.length > 0) {
						peta.marker.hide(m);
					}

					this.markers[name] = this.create(data);

					if (name == Form.page.active()) {
						peta.marker.show(this.markers[name], Form.map.main.canvas);
					}
				},
				reset: function() {
					var markers = this.markers;
					for (var k in markers) {
						if (markers.hasOwnProperty(k)) {
							peta.marker.hide(markers[k]);
							markers[k] = [];
						}
					}
				},
				create: function(json) {
					var d = json.data || [];
					var lat, lng, latLng, marker = [];
					for (var i = 0; i < d.length; i++) {
						lat = d[i].tempat.latitude;
						lng = d[i].tempat.longitude;
						latLng = peta.latLng(lat, lng);
						marker[i] = peta.marker.create(latLng, d[i].tempat);
						marker[i].data = d[i];
						marker[i].setTitle(d[i].tempat.nama);
						marker[i].addListener('click', Form.map.main.marker.onClick);
					}
					return marker;
				},
				onClick: function(e) {
					var a = Form.detail;
					a.model = Form.page.active();
					a.id = this.data.id;
					a.lat = this.data.tempat.latitude;
					a.lng = this.data.tempat.longitude;
					$('#modal-detail').openModal({
						dismissible: false,
						opacity: '.5',
						in_duration: '300',
						out_duration: '200',
						ready: a.ready,
						complete: a.exit,
					});
				},
				change: function() {
					var markers = Form.map.main.marker.markers;
					var now = Form.page.active();
					for (var k in markers) {
						if (markers.hasOwnProperty(k)) {
							if (now != k) {
								peta.marker.hide(markers[k]);
							} else {
								peta.marker.show(markers[k], Form.map.main.canvas);
							}
						}
					}
				}
			},
		},
		form: {
			id: "map_form",
			initMap: function() {
				this.canvas = peta.init(this.id);
				this.event.click(this.canvas);
				if (Form.modal.jenis == 'edit')
					Form.map.form.editSetMap();
			},
			marker: null,
			event: {
				click: function(map) {
					google.maps.event.addListener(map, 'click', this.onClick);
				},
				onClick: function(e) {
					var f = Form.map.form;
					var icon = peta.options.icon[Form.page.active()] || peta.options.icon.me;
					if (f.marker !== null) {
						f.marker.setMap(null);
						f.marker = null;
					}
					f.marker = peta.marker.create(e.latLng);
					f.marker.setMap(f.canvas);
					f.marker.setIcon(icon);
					// f.marker.setAnimation(google.maps.Animation.BOUNCE);
					Form.modal.button.enable('next');
				},

			},
			edit: function(lat, lng) {
				var f = Form.map.form;
				var latLng = peta.latLng(lat, lng);
				var icon = peta.options.icon[Form.page.active()] || peta.options.icon.me;
				f.marker = peta.marker.create(latLng);
				f.marker.setIcon(icon);
				f.marker.setAnimation(google.maps.Animation.DROP);
			},
			editSetMap: function() {
				var f = Form.map.form;
				f.marker.setMap(f.canvas);
				Form.modal.button.enable('next');
			},
		}
	},
	modal: {
		init: function() {
			// $('.modal-trigger').leanModal(Form.modal.option.form());
			$(document).on('click', ".modal-trigger[href='#modal-form']", function(e) {
				Form.modal.jenis = $(this).data('jenis');
				// $(this).leanModal(Form.modal.option.form());
				Form.modal.show.modal(Form.modal.option.form());
				Form.modal.title(Form.modal.jenis);

				if (Form.modal.jenis == 'edit') {
					Form.modal.load.editID = $(this).data('id');
					var lat = $(this).data('lat');
					var lng = $(this).data('lng');
					Form.map.form.edit(lat, lng);
				}
			});
		},
		submit: function(e) {
			e.stopPropagation();
			e.preventDefault();
			Form.modal.show.loader();
			Form.modal.hide.form();
			var self = Form.modal;
			var url = $(this).attr('action');
			var form_data = document.querySelector('#form-tempat');
			var data = new FormData(form_data);
			var xhr = new XMLHttpRequest();
			var storedFiles = Form.file.new || [];
			var imagesName = Form.file.current || [];
			for (var i = 0, len = storedFiles.length; i < len; i++) {
				data.append('images[' + i + ']', storedFiles[i]);
			}

			if (imagesName.length > 0) {
				for (var c = imagesName.length - 1; c >= 0; c--) {
					data.append('imagesName[' + c + ']', imagesName[c]);
					console.log(imagesName[c]);
					imagesName.pop();
				}
			}

			xhr.open('POST', url, true);
			xhr.onload = function(e) {
				if (this.status == 200) {
					self.hide.loader();
					self.hide.modal();
					self.show.submit();
					table[Form.page.active()].ajax.reload();
					Form.modal.resetModal();
				} else {
					self.hide.loader();
					self.show.form();
				}
			};
			xhr.send(data);
		},
		ready: function() {
			var name = Form.page.active();
			console.log($(this));
			if (Form.modal.jenis == 'edit')
				Form.modal.load.edit(name);
			else
				Form.modal.load.create(name);
		},
		exit: function() {
			var m = Form.modal;
			table[Form.page.active()].ajax.reload();
			Form.modal.resetModal();
		},
		title: function(title) {
			var name = Form.page.active();
			$("#modal-form .modal-header h5").html(title.capitalize() + " Lokasi " + name);
		},
		option: {
			form: function() {
				return {
					dismissible: false,
					opacity: '.5',
					in_duration: '300',
					out_duration: '200',
					ready: Form.modal.ready,
					complete: Form.modal.exit,
				};
			},
		},
		show: {
			modal: function(opt) {
				$('#modal-form').openModal(opt);
			},
			modalHapus: function() {
				$('#modal-hapus').openModal();
			},
			map: function() {
				$('.modal-map-content').removeClass('hide');
			},
			form: function() {
				$('.modal-form-content').removeClass('hide');
			},
			loader: function() {
				$('.modal-loader').removeClass('hide');
			},
			submit: function() {
				var jenis = Form.modal.jenis || 'Tambah';
				var name = Form.page.active();
				Materialize.toast('Selamat, Data ' + name.capitalize() + ' berhasil di ' + jenis.toLowerCase(), 3000, 'green');
			},
			foto: function(name) {
				Materialize.toast(name + ' tidak bisa ditambahkan, maksimal 5 foto', 3000, 'red');
			},
			hapus: function() {
				Materialize.toast('Selamat data berhasil di hapus', 3000, 'green');
			},
			fotoSize: function(name) {
				Materialize.toast(name + ' tidak bisa ditambahkan, ukuran maksimal foto 2 MB', 3000, 'red');
			},
			fotoType: function(name) {
				Materialize.toast(name + ' tidak bisa ditambahkan, bukan file gambar', 3000, 'red');
			},
			tableError: function(name) {
				Materialize.toast('Tabel ' + name + ' Terjadi kesalahan pada server, tidak dapat menampilkan data', 3000, 'red');
			},
		},
		hide: {
			modal: function() {
				$('#modal-form').closeModal();
			},
			modalHapus: function() {
				$('#modal-hapus').closeModal();
			},
			map: function() {
				$('.modal-map-content').addClass('hide');
			},
			form: function() {
				$('.modal-form-content').addClass('hide');
			},
			loader: function() {
				$('.modal-loader').addClass('hide');
			},
		},
		resetModal: function() {
			var m = Form.modal;
			m.hide.map();
			m.hide.form();
			m.show.loader();
			m.button.disable('next');
			Form.file.current = [];
			Form.file.new = [];
		},
		load: {
			bindEvent: function() {
				var name = Form.page.active();
				var fn = Form.modal.load.jenis[name] || false;
				Form.modal.select.init();
				Form.modal.typeahead.init();
				Form.file.current = Form.file.getCurrentImage();
				if (fn) fn();
			},
			jenis: {
				wisata: function() {
					Form.modal.tiket.kategori();
					Form.modal.tiket.operasional();
					Form.modal.button.tiket();
					CKEDITOR.replace('keterangan');
				},
				restoran: function() {
					Form.modal.select.restoran();
				},
				event: function() {
					$('.date').pickadate({
						selectMonths: true,
						selectYears: +10,
						format: 'dd/mm/yyyy',
						closeOnSelect: true,
						onSet: function(arg) {
							if ('select' in arg) {
								this.close();
								console.log(arg);
							}
						}
					});
				},
				belanja: function() {
					Form.modal.select.belanja();
				}
			},
			create: function(name) {
				$.get(name + "/create", this.onLoadSuccess).fail(this.onLoadFail);
			},
			edit: function(name) {
				var id = Form.modal.load.editID;
				$.get(name + "/" + id + "/edit", this.onLoadSuccess).fail(this.onLoadFail);
			},
			onLoadSuccess: function(data) {
				var m = Form.modal;
				m.show.map();
				m.hide.loader();
				$('.modal-form-content').html(data);
				Form.map.form.initMap();
				m.load.bindEvent();
			},
			onLoadFail: function() {
				Materialize.toast('Maaf terjadi kesalahan pada server', 3000, 'red');
				Form.modal.hide.modal();
			},
		},
		button: {
			next: function(e) {
				// console.log($(e.target).hasClass('disabled'));
				if (!$(e.target).hasClass('disabled')) {

					var m = Form.modal;
					m.hide.map();
					m.show.form();
					var lat = Form.map.form.marker.position.lat();
					var lng = Form.map.form.marker.position.lng();
					$('#latitude').val(lat);
					$('#longitude').val(lng);

				}
			},
			back: function() {
				var m = Form.modal;
				m.hide.form();
				m.show.map();
			},
			disable: function(name) {
				$('.btn-' + name).addClass('disabled');
			},
			enable: function(name) {
				$('.btn-' + name).removeClass('disabled');
			},
			tiket: function(e) {
				if ($("#is_tiket").prop('checked')) {
					Form.modal.button.showTiket();
				} else {
					Form.modal.button.hideTiket();
				}
			},
			showTiket: function() {
				var tp = $("#tiket-panel");

				var tiket = tp.has('.tiket-item');
				if (tiket.length === 0) {
					this.addTiket();
				}
				$('.tiket-item input').prop('disabled', false);
				tp.show();
			},
			hideTiket: function() {
				var jenis = Form.modal.jenis;
				$("#tiket-panel").hide();
				$('.tiket-item input').prop('disabled', true);

			},
			addTiket: function() {
				var n = Form.modal.button.indexTiket()[0] + 1 || 0;
				console.log(n);
				var dom = Form.modal.button.tiketDom(n);
				$("#tiket-panel .card-panel").append(dom);
			},
			hapusTiket: function(e) {
				var parent = $(this).parents('.tiket-item');
				if ($('.tiket-item').length > 1) {
					parent.remove();
				}
			},
			indexTiket: function() {
				return $('.tiket-item').map(function(a) {
					return $(this).data('id');
				}).reverse();
			},
			tiketDom: function(n) {
				var dom = '<div class="row tiket-item" data-id="' + n + '"><div class="input-field col s4">' +
					'<input class="kategori" type="text" name="tiket[' + n + '][nama]" placeholder="nama tiket">' +
					'<label for ="tiket[' + n + '][nama]" class="active">Jenis Tiket</label>' +
					'</div><div class="input-field col s4">' +
					'<input type="number" class="validate harga" name="tiket[' + n + '][harga]" required="required">' +
					'<label for="harga">Harga</label> </div>' +
					'<div class="input-field col s3">' +
					'<input class="operasional" type="text" name="tiket[' + n + '][operasional]" placeholder="untuk hari...">' +
					'<label class="active">Hari</label> </div> <div class="input-field col s1">' +
					'<button type="button" class="btn-small btn-tiket-clear btn red"><i class="material-icons">cancel</i></button> </div> </div>';
				return dom;
			},
		},
		tiket: {
			kategori: function() {
				$(".typeahead.kategori").typeahead({
					minLength: 0,
					highlight: true,
					hint: true,
				}, {
					name: 'nama',
					source: 'json/optiket',
				});
			},
			operasional: function() {
				$(".typeahead.operasional").typeahead({
					minLength: 0,
					highlight: true,
					hint: true,
				}, {
					name: 'operasional',
					source: 'json/optiket',
				});
			},
		},
		select: {
			init: function() {
				this.default();
				this.fasilitas();
			},
			options: {
				default: function() {
					return {
						sortField: 'text',
					};
				},
				fasilitas: function() {
					return {
						plugins: ['remove_button'],
						create: function(input) {
							return {
								value: input,
								text: input
							};
						},
					};
				},
				create: function() {
					return {
						plugins: ['remove_button'],
						sortField: 'text',
						create: true,
					};
				},
			},
			default: function(opt) {
				// $('.jenis-wisata').selectize(this.options.default());		
				var def = ".materialize-select";
				$(def).material_select();
			},
			fasilitas: function() {
				$('.fasilitas').selectize(this.options.fasilitas());
			},
			restoran: function() {
				$('.selectize').selectize(this.options.create());
			},
			belanja: function() {
				$('.selectize').selectize(this.options.create());
			}
		},
		typeahead: {
			bloodhund: function(n) {
				var alamat = new Bloodhound({
					datumTokenizer: Bloodhound.tokenizers.obj.nonword(n),
					queryTokenizer: Bloodhound.tokenizers.nonword,
					identify: function(obj) {
						return obj.village;
					},
					prefetch: {
						cache: false,
						url: 'json/alamat',
						filter: function(data) {
							return $.map(data, function(z) {
								return $.map(z.districts, function(m) {
									return $.map(m.villages, function(n) {
										return {
											regencie: z.name.toLowerCase().capitalize(),
											district: 'Kecamatan ' + m.name.toLowerCase().capitalize(),
											village: 'Desa ' + n.name.toLowerCase().capitalize(),
										};
									});
								});
							});
						}
					}
				});
				alamat.initialize();
				return alamat;
			},
			init: function() {
				var villageSrc = Form.modal.typeahead.bloodhund('village');
				villageSrc.initialize();
				var regencieSrc = Form.modal.typeahead.bloodhund('regencie');
				regencieSrc.initialize();
				var districtSrc = Form.modal.typeahead.bloodhund('district');
				districtSrc.initialize();

				$('.alamat.typeahead').typeahead({
					minLength: 3,
					highlight: true,
					hint: true,
					classNames: {
						suggestion: 'collection-item',
						menu: 'collection',
					}
				}, {
					name: 'village',
					limit: 15,
					display: function(data) {
						return data.village + ', ' + data.district + ', ' + data.regencie;
					},
					source: villageSrc.ttAdapter(),
					templates: {
						// empty: [
						// 	'<div class="empty-message">',
						// 	'alamat yang dicari tidak ada',
						// 	'</div>'
						// ].join('\n'),
						suggestion: function(data) {
							return '<div>' + data.village + ', ' + data.district + ', ' + data.regencie + '</div></br>';
						},
					}
				}, {
					name: 'regencie',
					limit: 15,
					display: function(data) {
						return data.village + ', ' + data.district + ', ' + data.regencie;
					},
					source: regencieSrc.ttAdapter(),
					templates: {
						// empty: [
						// 	'<div class="empty-message">',
						// 	'alamat yang dicari tidak ada',
						// 	'</div>'
						// ].join('\n'),
						suggestion: function(data) {
							return '<div>' + data.village + ', ' + data.district + ', ' + data.regencie + '</div></br>';
						},
					}
				}, {
					name: 'district',
					limit: 15,
					display: function(data) {
						return data.village + ', ' + data.district + ', ' + data.regencie;
					},
					source: districtSrc.ttAdapter(),
					templates: {
						// empty: [
						// 	'<div class="empty-message">',
						// 	'alamat yang dicari tidak ada',
						// 	'</div>'
						// ].join('\n'),
						suggestion: function(data) {
							return '<div>' + data.village + ', ' + data.district + ', ' + data.regencie + '</div></br>';
						},
					}
				});
			}
		},

	}
};


Form.onDeleteReview = function(e) {
	var id = $(this).data('id');
	console.log(id);
	if (confirm('apakah anda yakin ingin menghapus review ini ?')) {
		$.ajax({
			url: 'wisata/'+id+'/deleteReview',
			data: {
				_method: 'DELETE',
			},
			type : 'POST',			
		}).done(function(data,text,xhr){
			Form.detail.loadReview();
		}).fail(function(xhr,text,err){
			Materialize.toast('Terjadi kesalahan pada server', 3000, 'red');
			console.log(xhr.status);
		});
	}
};