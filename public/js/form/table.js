function formatDate(str) {
	var tgl = new Date(str);
	var d = tgl.getDay();
	var m = tgl.getMonth();
	var y = tgl.getFullYear();
	return d + '/' + m + '/' + y;
}
window.table = {
	retry: {},
	init: function(name) {
		$.fn.dataTable.ext.errMode = table.error;
		table[name] = $('#table_' + name).DataTable(this.option(name));
		table.bindEvent(name);
		return table[name];
	},
	bindEvent: function(name) {
		table.eventListener.xhr(name);
		table.eventListener.init(name);
	},
	eventListener: {
		xhr: function(name) {
			table[name].on('xhr', function(e, setting, json, xhr) {
				if (xhr.status === 200 && typeof google !== 'undefined') {
					Form.map.main.marker.init(name, json);
				} else if (xhr.status === 500) {
					table.retry[name] = table.retry[name] || 1;
					console.log('retry ' + name + ' ke: ' + table.retry[name]);
					if (table.retry[name] < 3) {
						setTimeout(function() {
							table.retry[name]++;
							table[name].destroy();
							table.init(name);
						}, 3000);
					} else {
						Form.modal.show.tableError(name);
					}
				}
			});
		},
		init: function(name) {
			table[name].on('init', function(e) {
				Form.hapus.init();
			});
		},
	},
	error: function(settings, techNote, message) {
		console.log(message);
	},
	option: function(name) {
		return {
			// "responsive": true,
			// "processing": true,
			// "serverSide": true,
			// dom : '<"btn-add"f>rtip',
			"ajax": {
				url: table.url(name),
				dataSrc: "data",
			},
			columns: table.kolom[name](),
			initComplete: table.button.initButton,
		};
	},
	kolom: {
		map: function(a) {
			return {
				data: a,
				name: a
			};
		},
		aksi: function() {
			return {
				data: null,
				render: table.button.aksi,
			};
		},
		tempat: function(b) {
			var a = [{
				data: 'tempat.nama',
				name: 'tempat.nama'
			}];
			var c = [{
				// data: 'tempat.user.email',
				data: function(data) {
					return data.tempat.user.email + ' (' + data.tempat.user.level.nama + ')';
				},
				name: 'email',
			}, {
				data: function(data) {
					var status = data.tempat.publish === 1 ? 'checked' : ' ';
					var a = '<div class="switch"><label >' +
						'Tidak ' +
						'<input class="publish" type = "checkbox" name ="tempat[publish]" value = 1 ' + status + ' data-id="' + data.tempat.id + '"/>' +
						'<span class = "lever" > </span>' +
						'Ya' +
						'</label></div>';
					return a; //data.tempat.publish === 1 ? 'published' : 'not publish';
				},
			}];
			var temp = a.concat(b, c);
			// temp = temp.concat(b);
			temp.push(this.aksi());
			return temp;
		},
		atm: function() {
			var b = [{
				data: 'banks',
				name: 'banks',
				render: function(data) {
					return data.map(function(x) {
						return x.nama;
					}).join(', ');
				},
			}];
			return this.tempat(b);
		},
		spbu: function() {
			var b = [{
				data: 'fasilitas',
				name: 'fasilitas',
			}];
			return this.tempat(b);
		},
		wisata: function() {
			var b = [{
				data: 'jeniswisata.nama',
				name: 'jeniswisata.nama',
			}];
			return this.tempat(b);
		},
		penginapan: function() {
			var b = [{
				data: null,
				name: 'harga',
				render: function(d) {
					return d.harga_min + " - " + d.harga_max;
				}
			}];
			return this.tempat(b);
		},
		restoran: function() {
			var b = [{
				data: 'telepon',
				name: 'telepon',
			}];
			return this.tempat(b);
		},
		event: function() {
			var b = [{
				data: null,
				name: 'Tanggal',
				render: function(d) {
					return formatDate(d.tgl_mulai) + " - " + formatDate(d.tgl_selesai);
				}
			}, {
				data: null,
				name: 'Jam',
				render: function(d) {
					return d.waktu_mulai.slice(0, 5) + " - " + d.waktu_selesai.slice(0, 5);
				}
			}];
			return this.tempat(b);
		},
		belanja: function() {
			var b = [{
				data: 'jenis',
				name: 'jenis',
			}];
			return this.tempat(b);
		},
		transportasi: function() {
			var b = [{
				data: 'kategori.nama',
				name: 'kategori.nama',
			}];
			return this.tempat(b);
		},
	},
	button: {
		aksi: function(data, type, full, meta) {
			var btn_show = '<a href="#modal-detail" data-model="' + data.tempat.tempatable_type.split('\\')[2].toLowerCase() + '" data-id="' + data.id + '" data-lat="' + data.tempat.latitude + '"  data-lng="' + data.tempat.longitude + '" class="modal-trigger btn-floating"><i class="material-icons blue cyan">pageview</i></a>';


			var btn_edit = '<a href="#modal-form" data-jenis="edit" data-id="' + data.id + '" data-lat="' + data.tempat.latitude + '"  data-lng="' + data.tempat.longitude + '" class="modal-trigger btn-floating green"><i class="material-icons green">edit</i></a>';
			var btn_hapus = '<a href="#modal-hapus" data-id="' + data.id + '" class="modal-trigger btn-floating red"><i class="material-icons">delete_forever</i></a>';
	
				return btn_show + btn_edit + btn_hapus;
			
		},
		initButton: function() {

		}
	},
	url: function(name) {
		return "json/" + name;
	},
};