
window.peta = {
	map: [],
	latLng: function(lat, lng) {
		return new google.maps.LatLng(lat, lng);
	},
	options: {

		map: function() {
			return {
				center: peta.latLng(-6.9486877, 109.5421919),
				zoom: 11,
				disableDoubleClickZoom: true,
				'streetViewControl': false,
				'zoomControlOptions': {
					'position': google.maps.ControlPosition.LEFT_CENTER,
					'style': google.maps.ZoomControlStyle.SMALL
				}
			};
		},
		icon: {
			wisata: 'images/icon/wisata.png',
			restoran: 'images/icon/restoran.png',
			atm: 'images/icon/atm.png',
			spbu: 'images/icon/spbu.png',
			penginapan: 'images/icon/penginapan.png',
			transportasi: 'images/icon/transportasi.png',
			belanja: 'images/icon/belanja.png',
			event: 'images/icon/event.png',
			me: 'images/icon/me.png',
		},
		marker: function(latLng, data) {
			var tipe = data ? data.tempatable_type.split('\\')[2].toLowerCase() : 'me';
			var url = this.icon[tipe];
			return {
				position: latLng,
				icon: {
					url: url,
					size: new google.maps.Size(30, 35),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(18, 42)
				},
			};
		},
		infoWindow: function(data) {
			var pic = data.gambars[0] ? data.gambars[0].nama : 'image404.png';
			var dom = "<img src='img/small/" + pic + "' align='left'>" +
				"<b>" + data.nama + "</b>" +
				"<b>Alamat</b> : <p>" + data.keterangan + "</p>" +
				"<a href='#detail-tempat' data-id='" + data.id + "'>lihat detail..</a>";
			return {
				content: dom,
				title: data.nama
			};
		},
		infoBox: function(data) {
			var pic = data.gambars[0] ? data.gambars[0].nama : 'image404.png';
			var latLng = peta.latLng(data.latitude,data.longitude);
			var dom = '<ul class="collection">' +
				'<li class="collection-item avatar">' +
				'< img src = "img/small/' + pic + '" class = "circle" > ' +
				'< span class = "title" >' + data.nama + '< /span>' +
				' < p > '+ data.alamat +' < br >< /p>  ' +
				' < p > '+ data.keterangan +' < br >< /p>  ' +
				// '< a href = "#!" class = "secondary-content" > < i class = "material-icons" >< /i></a >' +
				'</li>' +
				'</ul>';
				return {
					content : dom,
					disableAutoPan : false,
					maxWidth : 0,
					position : latLng,
					boxStyle : {
						background : '',
					}
				};
		},
	},
	init: function(id) {
		var server = this.server;
		var a = new google.maps.Map(document.getElementById(id), peta.options.map());
		// server.get().done(server.success).fail(server.fail);
		this.map.push(a);
		return a;
	},
	server: {
		get: function(name) {
			return $.getJSON('json/' + name);
		},
		success: function(data) {
			var marker = peta.marker;
			var latLng;
			for (var i = 0; i < data.length; i++) {
				latLng = peta.latLng(data[i].latitude, data[i].longitude);
				marker.add(latLng, data[i]);
			}
		},
		fail: function(e) {

		},
	},
	marker: {

		onClick: function(m, data) {
			var iw;
			m.addListener('click', function() {
				// iw = peta.marker.infoWindow(data);
				// console.log(iw);
				// iw.open(Form.map.main.canvas, m);
			});
		},
		infoWindow: function(data) {
			var opt = peta.options.infoWindow(data);
			var infoWindow = new google.maps.InfoWindow(opt);
			return infoWindow;
		},
		create: function(latLng, data) {
			var marker = new google.maps.Marker(peta.options.marker(latLng, data));
			// if (data) {
			// 	peta.marker.onClick(marker, data);
			// }
			return marker;
		},
		add: function(latLng, data) {
			var marker = this.create(latLng, data);
			marker.setMap(peta.map);
			this.markers.push(marker);
		},
		show: function(markers, map) {
			var m = markers || [];
			for (var i = 0; i < m.length; i++) {
				m[i].setMap(map);
			}
		},
		hide: function(markers) {
			var m = markers || [];
			for (var i = 0; i < m.length; i++) {
				m[i].setMap(null);
			}
		}

	},
};