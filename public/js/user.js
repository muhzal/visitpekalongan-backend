$.validator.setDefaults({
	errorClass: 'invalid',
	validClass: "valid",
});
table.kolom.user = function() {
	var b = [{
		data: 'name',
		name: 'name',
	}, {
		data: 'email',
		name: 'email',
	}, {
		data: 'level.nama',
		name: 'level',
	}, {
		data: null,
		render: function(data, type, full, meta) {
			var btn_edit = '<a href="#modal-user" data-jenis="edit" data-id="' + data.id + '" class="modal-trigger modal-edit btn-floating green"><i class="material-icons green">edit</i></a>';
			var btn_hapus = '<a href="#modal-hapus" data-id="' + data.id + '" class="modal-trigger btn-floating red"><i class="material-icons">delete_forever</i></a>';
			return btn_edit + btn_hapus;
		}
	}, ];
	return b;
};
table.option = function(name) {
	return {
		"responsive": true,
		"ajax": {
			url: 'json/' + name,
			dataSrc: "data",
		},
		columns: table.kolom.user(),
		// initComplete: table.button.initButton,
	};
};

window.user = {
	init: function() {
		this.event.bindEvent();
	},
	event: {
		bindEvent: function() {
			this.editButton();
			this.addButton();
			user.form.validate();
			$(document).on('submit', '#form-user', user.form.submit);
			$(document).on('submit', '#form-hapus', user.hapus.submit);
			$(document).on('click', 'a[href="#modal-hapus"]', user.hapus.click);
	
		},
		editButton: function() {
			$(document).on('click', '.modal-edit', function(e) {
				user.modal.open();
				user.modal.jenis = $(this).data('jenis');
				user.id = $(this).data('id');
			});
		},
		addButton: function() {
			$(document).on('click', 'a[href="#modal-user"].modal-trigger', function(e) {
				user.modal.jenis = $(this).data('jenis');
				user.modal.open();
			});
		},
	},
	hapus: {
		click: function(e) {
			user.hapus.id = $(this).data('id');
			user.hapus.open();
		},
		open: function() {
			$('#modal-hapus').openModal({
				ready: function() {
					$('#modal-hapus .modal-content h5').html('Apakah Anda yakin menghapus User ini ?');
				}
			});
		},
		close: function() {
			$('#modal-hapus').closeModal();
		},
		submit: function(e) {
			$('#form-hapus buton[type="submit"]').prop('disabled', true);
			e.preventDefault();
			$.ajax({
					url: 'user/' + user.hapus.id,
					type: 'POST',
					data: {
						_method: 'DELETE'
					},
				})
				.done(user.hapus.done)
				.fail(user.hapus.fail)
				.always(user.hapus.always);
		},
		done: function(data) {
			user.show.sukses.hapus(data.name);

			table[user.active()].ajax.reload();
		},
		always: function() {
			user.hapus.close();
			$('buton[type="submut"]').prop('disabled', false);
		},
		fail: function() {
			user.show.error.hapus();
		},

	},
	modal: {
		options: {
			close: function() {
				return {
					out_duration: '200',
					complete: this.exit,
				};
			},
			open: function() {
				return {
					dismissible: false,
					opacity: '.5',
					in_duration: '300',
					out_duration: '200',
					ready: user.modal.ready,
					complete: user.modal.exit,
				};
			},
		},
		close: function() {
			$('#modal-user').closeModal(this.options.open());
		},
		open: function() {
			$('#modal-user').openModal(this.options.open());
		},
		titleEdit: function() {
			$('#modal-user .modal-header h5').html('Ubah Data ' + user.active());
		},
		titleTambah: function() {
			$('#modal-user .modal-header h5').html('Tambah Data ' + user.active());
		},
		ready: function() {
			if (user.modal.jenis == 'edit') {
				user.load.edit();
				user.modal.titleEdit();		
				$('.password-row').addClass('hide');
			} else {
				user.show.form();
				user.form.reset();
				user.hide.loader();
				user.modal.titleTambah();				
				$('.password-row').removeClass('hide');
			}
			if (user.active() == 'user' ) {
				$("#level-row").addClass('hide');
			}else {
				$("#level-row").removeClass('hide');
			}
			$('#form-user select').removeClass('browser-default');
			$('#form-user select').material_select();
		},		
		exit: function() {
			user.modal.reset();
		},
		reset: function() {
			user.hide.form();
			user.show.loader();
			user.form.reset();
			user.modal.jenis = null;
			user.id = null;
		},
	},
	form: {
		submit: function(e) {
			e.preventDefault();
			var url = user.modal.jenis == 'tambah' ? 'user' : 'user/' + user.id;
			var data = $(this).serializeArray();
			user.show.loader();
			user.hide.form();
			$.ajax({
				url: url,
				data: data,
				type: 'POST'
			}).done(user.form.done).fail(user.form.fail);
		},
		done: function(data, text, xhr) {
			var jenis = user.modal.jenis;
			user.hide.loader();
			user.modal.close();
			user.show.sukses[jenis](data.name);
			// table.user.ajax.reload();
			table.user.ajax.reload();
			table.admin.ajax.reload();
		},
		fail: function(xhr, text, errTrow) {
			if (xhr.status == 422) {
				user.show.error.response(xhr.responseJSON);
			}

			user.show.form();
			// user.show.error.submit();
			user.hide.loader();
			console.log(xhr);
		},
		setValue: function(data) {
			var dom = this.dom();
			dom.name.val(data.name);
			dom.email.val(data.email);
			// console.log(data);
			dom.level.material_select('destroy');
			dom.level.find('option[value='+data.level.id+']').prop('selected',true);
			dom.level.material_select();
			dom.password.val('*********').prop('disabled', true);
			dom.method.val('PUT');
		},
		reset: function() {
			var dom = this.dom();
			dom.name.val('');
			dom.email.val('');
			dom.password.val('').prop('disabled', false);
			dom.method.val('POST');
			$('.invalid').removeClass('invalid');
		},
		dom: function() {
			var dom = {
				name: $('form #name'),
				email: $('form #email'),
				level: $('form #level'),
				password: $('form #password'),
				password_confirmation: $('form #password_confirmation'),
				method: $('form #_method'),
			};
			return dom;
		},
		validate: function() {
			$("#form-user").validate({
				rules: {
					name: {
						required: true
					},
					email: {
						required: true,
						email: true
					},
					password: {
						required: true,
						minlength: 5
					},
					password_confirmation: {
						required: true,
						minlength: 5,
						equalTo: "#form-user #password",
					}
				},
				//For custom messages
				messages: {
					name: {
						required: "Masukan nama lengkap user"
					},
					password_confirmation: {
						equalTo: 'Masukan password yang sama dengan sebelumnya'
					}
				},
				// errorElement: 'div',
				errorPlacement: function(error, element) {
					$(element).addClass('invalid')
						.removeClass('valid')
						.closest("form")
						.find("label[for='" + element.attr("id") + "']")
						.attr('data-error', error.text());
				}
			});
		}
	},
	load: {
		edit: function() {
			$.get('user/' + user.id + '/edit').done(this.done).fail(this.fail);
		},
		done: function(data, textStatus, jqXHR) {
			user.form.setValue(data);
			user.show.form();
			user.hide.loader();
		},
		fail: function(jqXHR, textStatus, errorThrown) {
			user.modal.close();
			user.show.error.load();
		},
	},
	show: {
		loader: function() {
			$('#modal-user .modal-loader').removeClass('hide');
		},
		form: function() {
			$('#modal-user .modal-content').removeClass('hide');
		},
		sukses: {
			tambah: function(name) {
				Materialize.toast('<i class="material-icons">check</i> User ' + name + ' berhasil di tambahkan', 4000, 'green');
			},
			edit: function(name) {
				Materialize.toast('<i class="material-icons">check</i> User ' + name + ' berhasil di ubah', 4000, 'green');
			},
			hapus: function(name) {
				Materialize.toast('<i class="material-icons">check</i> User ' + name + ' berhasil di hapus', 4000, 'green');
			}
		},
		error: {
			load: function() {
				Materialize.toast('Terjadi kesalahan pada server', 4000, 'red');
			},
			response: function(data) {
				for (var id in data) {
					for (var i = 0; i < data[id].length; i++) {
						$('#form-user #' + id).addClass('invalid');
						$('#form-user #' + id)
							.closest("form")
							.find("label[for='" + id + "']")
							.attr('data-error', data[id][i]);
					}
				}
			},
			submit: function() {
				Materialize.toast('<i class="material-icons">warning</i>Oops, Terjadi kesalahan<i class="material-icons">warning</i> ', 4000, 'red');
			},
			hapus: function() {
				Materialize.toast('Terjadi kesalahan pada server', 4000, 'red');
			},

		}
	},
	hide: {
		form: function() {
			$('#modal-user .modal-content').addClass('hide');
		},
		loader: function() {

			$('#modal-user .modal-loader').addClass('hide');
		},
	},
	active: function() {
		return $(".tabs .tab a.active").data('model');
	}
};