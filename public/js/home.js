window.peta = {
	latLng: function(lat, lng) {
		return new google.maps.LatLng(lat, lng);
	},
	options: {
		map: function() {
			return {
				center: peta.latLng(-6.9486877, 109.5421919),
				zoom: 11,
				disableDoubleClickZoom: true,
				'streetViewControl': false,
				'zoomControlOptions': {
					'position': google.maps.ControlPosition.LEFT_CENTER,
					'style': google.maps.ZoomControlStyle.SMALL
				}
			};
		},
		icon: {
			wisata: 'images/icon/wisata.png',
			restoran: 'images/icon/restoran.png',
			atm: 'images/icon/atm.png',
			spbu: 'images/icon/spbu.png',
			penginapan: 'images/icon/penginapan.png',
			transportasi: 'images/icon/wisata.png',
			belanja: 'images/icon/belanja.png',
			event: 'images/icon/event.png',
			me: 'images/icon/me.png',
		},
		marker: function(latLng, data) {
			var tipe = data ? data.tempatable_type.split('\\')[2].toLowerCase() : 'me';
			var url = this.icon[tipe];
			return {
				position: latLng,
				icon: {
					url: url,
					size: new google.maps.Size(30, 35),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(0, 32)
				},
			};
		},
		infoWindow: function(data) {
			var dom = "<img src='" + app.server.url + "/img/" + data.gambar + "' align='left'>" +
				"<h3>" + data.nama + "</h3>" +
				"<b>Alamat</b> : <p>" + data.alamat + "</p>" +
				"<a href='#detail-tempat' data-id='" + data.id + "'>lihat detail..</a>";
			return {
				content: dom,
				title: data.nama
			};
		},
	},
	init: function() {
		var server = this.server;
		peta.map = new google.maps.Map(document.getElementById('map-canvas'), peta.options.map());
		server.get().done(server.success).fail(server.fail);
	},
	server: {
		get: function() {
			return $.getJSON('json/tempat');
		},
		success: function(data) {
			var marker = peta.marker;
			var latLng;
			for (var i = 0; i < data.length; i++) {
				latLng = peta.latLng(data[i].latitude, data[i].longitude);
				marker.add(latLng, data[i]);
			}
		},
		fail: function(e) {

		},
	},
	marker: {
		markers: [],
		onClick: function(marker, data) {
			marker.addListener('click', function() {
				this.infoWindow(data).open(peta.map, marker);
			});
		},
		infoWindow: function(data) {
			var opt = peta.options.infoWindow(data);
			var infoWindow = new google.maps.InfoWindow(opt);
			return infoWindow;
		},
		create: function(latLng, data) {
			var marker = new google.maps.Marker(peta.options.marker(latLng, data));
			if (data) {
				this.onClick(marker, data);
			}
			return marker;
		},
		add: function(latLng, data) {
			var marker = this.create(latLng, data).setMap(peta.map);
			this.markers.push(marker);
		},
	},
};

$(function() {
	if (typeof google !== 'undefined')
		peta.init();

});