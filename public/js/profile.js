var Profile = {
	init: function() {
		$(document).on('click', 'a[href="#modal-profile"]', this.openModal);
		$(document).on('submit', '#form-profile', this.submit);
		$(document).on('change', '#form-profile #old_password', this.password);
		Profile.validate();
	},
	openModal: function(e) {
		$('#modal-profile').openModal({
			dismissible: false,
			opacity: '.5',
			in_duration: '300',
			out_duration: '200',
			ready: Profile.onModalReady,
			complete: Profile.onModalExit,
		});
	},
	closeModal: function(e) {
		$('#modal-profile').closeModal();
	},
	onModalReady: function() {

	},
	onModalExit: function() {
		$('#form-profile input[type="password"]').val('');
		$('.invalid').removeClass('invalid');
	},
	password: function() {
		$('.invalid').removeClass('invalid');
		if ($(this).val()) {
			$('#form-profile #password').prop('required', true);
			$('#form-profile #password_confirmation').prop('required', true);
		} else {
			$('#form-profile #password').prop('required', false);
			$('#form-profile #password_confirmation').prop('required', false);
		}
	},
	submit: function(e) {
		e.preventDefault();
		var _ = Profile;
		var url = $(this).attr('action');
		var data = $(this).serializeArray();
		_.showLoader();
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data,
		}).done(_.done).fail(_.fail).always(function() {
			_.hideLoader();
		});
	},
	done: function(data, text, xhr) {
		Materialize.toast('Berhasil Mengubah Profile', 3000, 'green');
		Profile.closeModal();
	},
	fail: function(xhr, text, errTrow) {
		Materialize.toast('Terjadi kesalahan', 3000, 'red');
		var data = xhr.responseJSON;
		for (var id in data) {
			for (var i = 0; i < data[id].length; i++) {
				$('#' + id).addClass('invalid');
				$('#' + id)
					.closest("#form-profile")
					.find("label[for='" + id + "']")
					.attr('data-error', data[id][i]);
			}
		}
	},
	showLoader: function() {
		$('#modal-profile .modal-loader').addClass('active');
		$('#form-profile').addClass('hide');
	},
	hideLoader: function() {
		$('#modal-profile .modal-loader').removeClass('active');
		$('#form-profile').removeClass('hide');
	},
	validate: function() {
		$("#form-profile").validate({
			rules: {
				name: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				password: {
					minlength: 5,
				},
				password_confirmation: {
					minlength: 5,
					equalTo: "#password",
				}
			},
			//For custom messages
			messages: {
				name: {
					required: "Masukan nama lengkap user"
				},
				password_confirmation: {
					equalTo: 'Masukan password yang sama dengan sebelumnya'
				}
			},
			// errorElement: 'div',
			errorPlacement: function(error, element) {
				console.log(error.text());
				$(element)
					.closest("#form-profile")
					.find("label[for='" + element.attr("id") + "']")
					.attr('data-error', error.text());
			}
		});
	}
};

$(document).ready(function() {
	Profile.init();
});