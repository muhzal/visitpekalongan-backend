<?php

return array(

	/*
		    |--------------------------------------------------------------------------
		    | Name of route
		    |--------------------------------------------------------------------------
		    |
		    | Enter the routes name to enable dynamic imagecache manipulation.
		    | This handle will define the first part of the URI:
		    |
		    | {route}/{template}/{filename}
		    |
		    | Examples: "images", "img/cache"
		    |
	*/

	'route' => 'img',

	/*
		    |--------------------------------------------------------------------------
		    | Storage paths
		    |--------------------------------------------------------------------------
		    |
		    | The following paths will be searched for the image filename, submited
		    | by URI.
		    |
		    | Define as many directories as you like.
		    |
	*/

	'paths' => array(
		public_path('images/Spbu'),
		public_path('images/Atm'),
		public_path('images/Penginapan'),
		public_path('images/Restoran'),
		public_path('images/Wisata'),
		public_path('images/Event'),
		public_path('images/other'),
		public_path('images/Belanja'),
		public_path('images/Transportasi'),
	),
	// 'paths' => array(
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/spbu',
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/atm',
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/penginapan',
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/restoran',
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/wisata',
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/event',
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/other',
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/belanja',
	// 	$_SERVER[DOCUMENT_ROOT] . 'images/transportasi',
	// ),

	/*
		    |--------------------------------------------------------------------------
		    | Manipulation templates
		    |--------------------------------------------------------------------------
		    |
		    | Here you may specify your own manipulation filter templates.
		    | The keys of this array will define which templates
		    | are available in the URI:
		    |
		    | {route}/{template}/{filename}
		    |
		    | The values of this array will define which filter class
		    | will be applied, by its fully qualified name.
		    |
	*/

	'templates' => array(
		'small' => 'Intervention\Image\Templates\Small',
		'medium' => 'Intervention\Image\Templates\Medium',
		'large' => 'Intervention\Image\Templates\Large',
	),

	/*
		    |--------------------------------------------------------------------------
		    | Image Cache Lifetime
		    |--------------------------------------------------------------------------
		    |
		    | Lifetime in minutes of the images handled by the imagecache route.
		    |
	*/

	'lifetime' => 43200,

);
