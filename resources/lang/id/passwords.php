<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Password harus lebih dari 6 karakter dan sama dengan konfirmasi password.',
    'reset' => 'Password Anda telah berhasil di reset!',
    'sent' => 'Kami sudah mengirimkan email tautan password reset Anda',//We have e-mailed your password reset link!',
    'token' => 'Token reset password ini salah',//This password reset token is invalid.',
    'user' => "Email yang Anda masukan tidak terdaftar",//We can't find a user with that e-mail address.",

];
