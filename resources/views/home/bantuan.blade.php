@extends('layouts.master',['page_title'=>'Kalongcation : Bantuan'])
@section('master-content')
<ul class="collapsible" data-collapsible="accordion">
	<li>
		<div class="collapsible-header light-blue light-blue-text text-lighten-5"><i class="material-icons">help_outline</i>Bagaimana cara menghapus review ?</div>
		<div class="collapsible-body light-blue lighten-5">
			<ol>
				<li>1. Masuk ke menu daftar <a href="{{ url('tempat') }}" title="">Lokasi</a> atau <a href="{{ url('tempat') }}" title="">Usulan</a></li>
				<li>2. Masuk ke dalam tab wisata</li>
				<li>3. Kemudian tekan tombol detail <a href="#" class="btn-floating small"><i class="material-icons md-18 blue cyan">pageview</i></a></li>
				<li>4. Pilih tab review dan tekan tombol delete <i class="material-icons small red-text">delete_forever</i></li>
			</ol>
		</div>
	</li>
	<li>
		<div class="collapsible-header light-blue light-blue-text text-lighten-5"><i class="material-icons">help_outline</i>Bagaimana tambah lokasi baru ? </div>
		<div class="collapsible-body light-blue lighten-5">
			<ol>
				<li>1. Masuk ke menu daftar <a href="{{ url('tempat') }}" title="">Lokasi</a></li>
				<li>2. Pilih tab sesuai kategori tempat yang ingin ditambahkan</li>
				<li>3. Kemudian tekan tombol tambah <a href="#!" class="btn green small"><i class="material-icons left">add</i>Tambah Lokasi Baru</a> dan sebuah jendela akan muncul</li>
				<li>4. Klik pada peta lokasi tempat yang akan ditambahkan kemudian tekan selanjutnya</li>
				<li>5. isikan setiap inputan pada form tambah tempat</li>
				<li>6. tekan tombol simpan</li>
			</ol>
		</div>
	</li>
	<li>
		<div class="collapsible-header light-blue light-blue-text text-lighten-5"><i class="material-icons">help_outline</i>Bagaimana mengubah data lokasi atau data usulan ?</div>
		<div class="collapsible-body light-blue lighten-5">
			<ol>
				<li>1. Masuk ke menu daftar <a href="{{ url('tempat') }}" title="">Lokasi</a> atau <a href="{{ url('tempat') }}" title="">Usulan</a></li>
				<li>2. Pilih tab sesuai kategori tempat yang ingin ditambahkan</li>
				<li>3. Kemudian tekan tombol ubah <a href="#" class="btn-floating small"><i class="material-icons md-18 green cyan">edit</i></a>  dan sebuah jendela akan muncul</li>
				<li>4. Klik pada peta untuk mengubah lokasi tempat kemudian tekan selanjutnya</li>
				<li>5. Ubah data yang diinginkan</li>
				<li>6. tekan tombol simpan</li>
			</ol>
		</div>
	</li>
	<li>
		<div class="collapsible-header light-blue light-blue-text text-lighten-5"><i class="material-icons">help_outline</i>Bagaimana cara menghapus data lokasi atau usulan ?</div>
		<div class="collapsible-body light-blue lighten-5">
			<ol>
				<li>1. Masuk ke menu daftar <a href="{{ url('tempat') }}" title="">Lokasi</a> atau <a href="{{ url('tempat') }}" title="">Usulan</a></li>
				<li>2. Pilih tab kategori tempat yang akan dihapus</li>
				<li>3. Kemudian tekan tombol delete <a href="#" class="btn-floating small"><i class="material-icons md-18 red">delete_forever</i></a></li>
				<li>4. Jendela konfirmasi akan muncul. kemudian tekan tombol Hapus <a href="#!" title="" class="btn small red"><i class="material-icons small ">delete_forever</i> Hapus</a></li>
			</ol>
		</div>
	</li>
	<li>
		<div class="collapsible-header light-blue light-blue-text text-lighten-5"><i class="material-icons">help_outline</i>Bagaimana cara menerbitkan lokasi agar terlihat user ?</div>
		<div class="collapsible-body light-blue lighten-5">
			<ol>
				<li>1. Masuk ke menu daftar <a href="{{ url('tempat') }}" title="">Lokasi</a> atau <a href="{{ url('tempat') }}" title="">Usulan</a></li>
				<li>2. Pilih tab kategori tempat yang akan diterbitkan</li>
				<li>3. Kemudian tekan tombol switch <img src="images/other/help-switch.png" height="30" alt=""> pada kolom terbitkan agar bergeser menjadi ya</li>				
			</ol>
		</div>
	</li>
</ul>
@stop