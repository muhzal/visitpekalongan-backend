@extends('layouts.master',['page_title'=>'Beranda'])
@push('jsplugin')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcNQvwKsFaKwIziGDYCBFRBB5cXDETX_0"></script>
<script src="{{ asset('js/home.js') }}"></script>
@endpush
@push('cssplugin')
<link type="text/css" rel="stylesheet" href="{{ asset('css/home.css') }}"/>
@endpush
@section('master-content')
<div class="row">
	<div class="col l8 m12 s12"><h5 class="header"> Peta Lokasi Pekalongan </h5>
		<div class="row">
			<div class="col s12">
				<div id="map-canvas">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<ul class="collection with-header" id="review">
					<li class="collection-item green white-text"><h5><i class="material-icons left">credit_card</i>Review Terbaru</h5></li>
					@foreach ($reviews as $review)
					<li class="collection-item">
						<!-- <i class="material-icons circle">person</i> -->
						<a href="#!" class="secondary-content">
							<i>{{ $review->created_at->format('d/m/Y G:i') }}</i>							
						</a>
							<span class="title"><strong>{{ $review->user->name }}</strong></span><br>
							<i>{{ $review->judul }}</i><br>
							@for ($i = 0; $i < $review->rating; $i++)
							<i class="material-icons tiny orange-text">star</i>
							@endfor
							@for($i=0;$i< (int) (5 - $review->rating);$i++ )
								<i class="material-icons tiny orange-text">star_border</i>
							@endfor
							<br>
							{{ $review->review }}
							- at <b><i>{{ $review->wisata->tempat->nama }}</i></b>
							<a href="#" title=""><span class="badge"><i class="material-icons tiny">delete_forever</i></span></a>
						
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<div class="col l4"><h5 class="header"> Daftar Lokasi Terbaru </h5>
		<ul class="collection">
			@foreach ($tempats as $tempat)
			<li class="collection-item avatar">
				<img src="img/small/{{ $tempat->gambars->pluck('nama')->first() }}" alt="" class="circle">
				<p class="truncate"><b><i>{{ $tempat->user->name }}</i></b></p>
				<span class="title">menambahkan {{ class_basename($tempat->tempatable_type) }}</span>
				<p><b>{{ $tempat->nama }}</b><br></p>
				<a href="#!" class="secondary-content">{{ $tempat->updated_at->format('d/m/Y G:i') }}</i></a>
			</li>
			@endforeach
			<li class="collection-item center"><a href="tempat" class="btn">Kelola data lokasi</a></li>
		</ul>
	</div>
</div>
@stop