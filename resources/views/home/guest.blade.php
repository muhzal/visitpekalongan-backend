<!DOCTYPE html>
<html>
  <head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <title>{{ $page_title or 'Kalongcation' }}</title>
    <link rel="shortcut icon" href="images/other/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/other/favicon.ico" type="image/x-icon">
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('plugins/materialize/css/materialize.min.css')}}"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css" media="screen">
    html,body,.slider,.slides{
    height : 100% !important;
    }
    .download-text{
      color: #fff;
      position: absolute;
      top: 15%;
      left: 15%;
      width: 70%;
      z-index : 1000;
    }
    </style>
  </head>
  <body>
    <div class="caption center-align download-text">
      <h3>Download Aplikasi Kalongcation</h3>
      <h5 class="light grey-text text-lighten-3">Temukan tempat menarik yang ada didaerah pekalongan</h5>
      <a href="http://visitpekalongan.id/downloads" class="btn btn-large blue" target="_blank"><i class="material-icons left">android</i> Download</a>
    </div>
    <div class="slider">
      <ul class="slides">
        <li>
          <img src="{{ url('img/large/slide-1.jpg') }}"> <!-- random image -->
        </li>
        <li>
          <img src="{{ url('img/large/slide-2.jpg') }}"> <!-- random image -->
        </li>
             <li>
          <img src="{{ url('img/large/slide-3.jpg') }}"> <!-- random image -->
        </li>
        <li>
          <img src="{{ url('img/large/slide-4.jpg') }}"> <!-- random image -->
        </li>
      </ul>
    </div>
    <script type="text/javascript" src="{{asset('js/jquery-1.11.2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/materialize/js/materialize.min.js')}}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    $('.slider').slider({full_width: true});
    });
    
    </script>
  </body>
</html>