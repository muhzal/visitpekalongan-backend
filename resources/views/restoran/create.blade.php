
@section('form-tempat')
    <div class="row">
      <div class="input-field col s12">
        <input type="number" class="validate" name="telepon" required="required">
        <label for="telepon">Telepon</label>
      </div>
    </div>
   <div class="row">
      <div class="input-field col s12 m6 l6">
        <select id="selectize" class="validate materialize-select" name="waktu_buka" required="required">
          @for ($i = 0; $i <= 24; $i++)
            <option value="{{ $i+00 }}:00" @if ($i == 7) selected @endif>{{ $i+00 }}:00</option>
          @endfor
        </select>
        <label for="bank">Waktu Buka</label>
      </div>
      <div class="input-field col s12 m6 l6">
        <select id="selectize" class="validate materialize-select" name="waktu_tutup" required="required">
            @for ($i = 0; $i <= 24; $i++)
              <option value="{{ $i+00 }}:00" @if ($i == 20) selected @endif>{{ $i+00 }}:00</option>
            @endfor
        </select>
        <label for="bank">Waktu Tutup</label>
      </div>
   </div>
   <div class="row">
      <div class="input-field col s12">
        <select id="selectize" class="selectize validate default-browser" name="menus[]" multiple required="required">

        <option value="" disabled></option>
            @foreach ($menus as $menu)
                <option value="{{ $menu->nama }}">{{ $menu->nama }}</option>
            @endforeach
        </select>
        <label for="menus" class="active">Menu Utama </label>
      </div>
   </div>
    <div class="row">
      <div class="input-field col s12 m6 l6">
        <input type="number" class="validate" name="harga_min" required="required">
        <label for="harga-min">Harga Terendah</label>
      </div>
      <div class="input-field col s12 m6 l6">
        <input type="number" class="validate" name="harga_max" required="required">
        <label for="harga-max">Harga Termahal</label>
      </div>
    </div>
@stop
@include('tempat.form-tambah-tempat', array('action' => url('restoran')))