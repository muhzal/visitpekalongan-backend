
@section('form-tempat')
    <div class="row">
      <div class="input-field col s12">
        <input type="number" class="validate" name="telepon" value="{{ $data->telepon }}" required="required">
        <label for="telepon"  class="active">Telepon</label>
      </div>
    </div>
   <div class="row">
      <div class="input-field col s12 m6 l6">
        <select class=" validate  materialize-select" name="waktu_buka" required="required">
          @for ($i = 0; $i < 24; $i++)
                {{ $time = date_format(date_create($i.':00'),'H:i') }}
                <option value="{{ $time }}" @if($time == $data->waktu_buka) selected @endif > {{ $time }}</option>
          @endfor
        </select>
        <label for="bank" class="active">Waktu Buka</label>
      </div>
      <div class="input-field col s12 m6 l6">
        <select  class=" validate  materialize-select" name="waktu_tutup" required="required">
            @for ($i = 0; $i <= 24; $i++)
                  {{ $time = date_format(date_create($i.':00'),'H:i') }}
                  <option value="{{ $time }}" @if($time == $data->waktu_tutup) selected @endif > {{ $time }}</option>
            @endfor
        </select>
        <label for="bank" class="active">Waktu Tutup</label>
      </div>
   </div>
   <div class="row">
      <div class="input-field col s12">
        <select  class=" selectize validate default-browser" name="menus[]" multiple required="required">
        @foreach ($menus as $menu)
            <option value="{{ $menu->nama }}" @if ($data->menus->contains('nama',$menu->nama)) selected @endif>{{ $menu->nama }}</option>
        @endforeach
        </select>
        <label for="bank" class="active">Menu Utama</label>
      </div>
   </div>
    <div class="row">
      <div class="input-field col s12">
        <input type="number" class="validate" value="{{ $data->harga_min }}" name="harga_min" required="required">
        <label for="harga-min" class="active">Harga Terendah</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <input type="number" class="validate"  value="{{ $data->harga_max }}" name="harga_max" required="required">
        <label for="harga-max" class="active">Harga Termahal</label>
      </div>
    </div>
@stop
@include('tempat.form-edit-tempat', array('action'=>route('restoran.update',['id' => $data->id]),'tempat'=>$data->tempat))