@extends('tempat.show')
@section('detail-tempat')

		<li class="collection-item avatar">
			<i class="material-icons circle cyan">contact_phone</i>
			<span class="title">Telepon</span>
			<p><b>{{ $data->telepon }}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">local_atm</i>
			<span class="title">Range Harga</span>
			<p><b>{{ $data->harga_min.' - '.$data->harga_max }}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">access_time</i>
			<span class="title">Waktu Buka</span>
			<p><b>{{ $data->waktu_buka.' - '.$data->waktu_tutup }}</b></p>
		</li>

		<li class="collection-item avatar">
			<i class="material-icons circle cyan">restaurant_menu</i>
			<span class="title">Menu Utama</span>
			<p><b>{{ $data->menus->implode('nama', ', ') }}</b></p>
		</li>
@endsection