
@section('form-tempat')
	 <div class="row">
      <div class="input-field col s12">
        <input type="text" class="validate" name="fasilitas" value="{{ $data->fasilitas }}" required="required">
        <label for="alamat" class="active">Fasilitas</label>
      </div>
    </div>
@stop
@include('tempat.form-edit-tempat', array('action'=>route('spbu.update',['id' => $data->id]),'tempat'=>$data->tempat))