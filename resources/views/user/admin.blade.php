@extends('layouts.master')
@section('master-content')
@include('tag.modal-user')
@include('tag.modal-hapus')
<div class="row">
	<div class="col s8">
		<div class="card-panel ">
			<a href="#modal-user" class="modal-trigger btn btn-medium green modal-tambah" data-jenis='tambah' title="">Tambah Admin</a>
			<table id="table_admin" class="responsive compact hover stripe order-column row-border dataTable cell-border" width="100%">
				<thead>
					<th>Nama</th>
					<th>Email</th>
					<th>Kategori</th>
					<th>Aksi</th>
				</thead>
			</table>
		</div>
	</div>
	<div class="col s4">
		<div class="card-panel ">
			<div class="card-title">
				<h5>Aktivitas Terakhir</h5>
			</div>
			<ul class="collection">
				@foreach ($tempats as $tempat)
				<li class="collection-item avatar">
					<img src="img/small/{{ $tempat->gambars->pluck('nama')->first() }}" alt="" class="circle">
					<span class="title">{{ $tempat->nama }}</span>
					<p>{{ class_basename($tempat->tempatable_type) }}<br></p>
					<p class="truncate">
						Ditambahkan oleh {{ $tempat->user ? $tempat->user->name : $tempat }}
					</p>
					<a href="#!" class="secondary-content">{{  $tempat->tempatable->created_at->diffForHumans() }}</i></a>
				</li>
				@endforeach
			</ul>
			<div class="card-action">
				<ul class="pagination">
					<li class="waves-effect"><a href="{{ $tempats->previousPageUrl() }}" id="page-back"><i class="material-icons left">chevron_left</i></a>Prev </li>
					<li class="waves-effect right"><a href="{{ $tempats->nextPageUrl() }}" id="page-next"><i class="material-icons right">chevron_right</i></a> Next</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@stop
@push('cssplugin')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{ asset('css/user.css') }}"/>
@endpush
@push('jsplugin')
<script type ="text/javascript" src="{{asset('plugins/datatables/datatables.min.js')}}"></script>
<script type ="text/javascript" src="{{asset('plugins/validation/dist/jquery.validate.min.js')}}"></script>
<script type ="text/javascript" src="{{asset('plugins/validation/dist/localization/messages_id.min.js')}}"></script>
<script type ="text/javascript" src="{{asset('vendor/datatables/buttons.server-side.js')}}"></script>
<script src="{{ asset('plugins/selectize/js/selectize.min.js') }}"></script>
<script src="{{ asset('js/form/table.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
<script type="text/javascript">
	$(function() {

		$.fn.dataTable.ext.errMode = table.error;
		table.admin = $('#table_admin').DataTable(table.option('admin'));
		user.init();

	});//end of ready
</script>
@endpush