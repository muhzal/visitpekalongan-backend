@extends('layouts.master')
@section('master-content')
@include('tag.modal-user')
@include('tag.modal-hapus')
<ul class="tabs card">
	<li class="tab col s3 hoverable">
		<a class="cyan-text active tooltipped" href="#admin-page" data-model="admin" data-position="bottom" data-delay="50" data-tooltip="Admin">
			<i class="small material-icons" alt="Wisata">person_outline</i>
		</a>
	</li>
	<li class="tab col s3 hoverable">
		<a class="cyan-text tooltipped" href="#user-page" data-model="user" data-position="bottom" data-delay="50" data-tooltip="User">
			<i class="small material-icons" alt="Wisata">groupe_outline</i>
		</a>
	</li>
</ul>
<div class="row">
	<div class="col s12" id="admin-page">
		<div class="card-panel ">

			<a href="#modal-user" class="modal-trigger btn btn-medium green modal-tambah" data-jenis='tambah' title="">Tambah Admin</a>
			<table id="table_admin" class="responsive compact hover stripe order-column row-border dataTable cell-border" width="100%">
				<thead>
					<th>Nama</th>
					<th>Email</th>
					<th>Level</th>
					<th>Aksi</th>
				</thead>
			</table>
		</div>
	</div>
	<div class="col s12" id="user-page">
		<div class="card-panel ">
			<table id="table_user" class="responsive compact hover stripe order-column row-border dataTable cell-border" width="100%">
				<thead>
					<th>Nama</th>
					<th>Email</th>
					<th>Level</th>
					<th>Aksi</th>
				</thead>
			</table>
		</div>
	</div>
</div>
@stop
@push('cssplugin')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{ asset('css/user.css') }}"/>
@endpush
@push('jspluginlast')
<script type ="text/javascript" src="{{asset('plugins/datatables/datatables.min.js')}}"></script>
<script type ="text/javascript" src="{{asset('plugins/validation/dist/localization/messages_id.min.js')}}"></script>
<script type ="text/javascript" src="{{asset('vendor/datatables/buttons.server-side.js')}}"></script>
<script src="{{ asset('plugins/selectize/js/selectize.min.js') }}"></script>
<script src="{{ asset('js/form/table.js') }}"></script>
<script src="{{ asset('js/user.js') }}"></script>
<script type="text/javascript">
	$(function() {
		
		$.fn.dataTable.ext.errMode = table.error;
		table.admin = $('#table_admin').DataTable(table.option('admin'));
		table.user = $('#table_user').DataTable(table.option('user'));
		user.init();
	});//end of ready
</script>
@endpush