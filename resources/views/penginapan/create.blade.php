
@section('form-tempat')
      <div class="row">
        <div class="input-field col s12">
          <input type="number" class="validate" name="telepon" required="required">
          <label for="telepon">Telepon</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="number" class="validate" name="harga_min" required="required">
          <label for="harga-min">Harga Terendah</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="number" class="validate" name="harga_max" required="required">
          <label for="harga-max">Harga Termahal</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="text" class="validate" name="fasilitas" required="required">
          <label for="fasilitas">Fasilitas</label>
        </div>
      </div>
@stop
@include('tempat.form-tambah-tempat', array('action' => url('penginapan')))