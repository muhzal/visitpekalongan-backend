@extends('tempat.show')
@section('detail-tempat')

		<li class="collection-item avatar">
			<i class="material-icons circle cyan">contact_phone</i>
			<span class="title">Telepon</span>
			<p><b>{{ $data->telepon }}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">local_atm</i>
			<span class="title">Range Harga</span>
			<p><b>{{ $data->harga_min.' - '.$data->harga_max }}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">playlist_add_check</i>
			<span class="title">Fasilitas</span>
			<p><b>{{ $data->fasilitas }}</b></p>
		</li>
@endsection