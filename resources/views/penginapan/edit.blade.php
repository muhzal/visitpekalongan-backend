
@section('form-tempat')
      <div class="row">
        <div class="input-field col s12">
          <input type="number" class="validate" name="telepon" required="required" value="{{ $data->telepon }}">
          <label for="telepon" class="active">Telepon</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="number" class="validate" name="harga_min" required="required" value="{{ $data->harga_min }}">
          <label for="harga-min" class="active">Harga Terendah</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="number" class="validate" name="harga_max" required="required" value="{{ $data->harga_max }}">
          <label for="harga-max" class="active">Harga Termahal</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="text" class="validate" name="fasilitas" required="required" value="{{ $data->fasilitas }}">
          <label for="fasilitas" class="active">Fasilitas</label>
        </div>
      </div>
@stop
@include('tempat.form-edit-tempat', array('action'=>route('penginapan.update',['id' => $data->id]),'tempat'=>$data->tempat))