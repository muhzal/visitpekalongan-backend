
@section('form-tempat')
	 <div class="row">
      <div class="input-field col s12">
      	<select id="selectize" class="validate materialize-select" placeholder="Pilih nama bank" name="banks[]" multiple required="required" placeholder="Pilih Bank..,">
        <option value="" disabled></option> 
      		@foreach ($banks as $bank)
      			<option value="{{ $bank->id }}">{{ $bank->nama }}</option>      			
      		@endforeach
      	</select>
        <label for="bank">Bank</label>
      </div>
    </div>
@stop
@include('tempat.form-tambah-tempat', array('action' => url('atm')))