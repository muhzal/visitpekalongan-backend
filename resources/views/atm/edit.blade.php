
@section('form-tempat')
	 <div class="row">
      <div class="input-field col s12">
      	<select id="bank" class="validate materialize-select" name="banks[]" multiple required="required" placeholder="Pilih Bank...">
      	@foreach ($banks as $bank)
      			<option value="{{ $bank->id }}" @if ($data->banks->contains($bank))
      				selected="selected"
      			@endif>{{ $bank->nama }}</option>
      	@endforeach
      	</select>
        <label for="bank">Bank</label>
      </div>
    </div>
@stop
@include('tempat.form-edit-tempat', array('action'=>route('atm.update',['id' => $data->id]),'tempat'=>$data->tempat))