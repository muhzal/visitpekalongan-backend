@extends('tempat.show')
@section('detail-tempat')
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">local_atm</i>
			<span class="title">Bank</span>
			<p><b>{{ $data->banks->implode('nama', ', ') }}</b></p>
		</li>
@endsection