<ul class="collection">
	@foreach ($tempats as $tempat)
	<li class="collection-item avatar">
		<img src="img/small/{{ $tempat->gambars->pluck('nama')->first() }}" alt="" class="circle">
		<span class="title">{{ $tempat->nama }}</span>
		<p>{{ class_basename($tempat->tempatable_type) }}<br></p>
		<p class="truncate">{{ $tempat->keterangan }} </p>
		<a href="#!" class="secondary-content">{{  time() - $tempat->created_at->format('U') <=3600000 ?  $tempat->updated_at->format('H:i') :  $tempat->updated_at->format('d/m/y') }}</i></a>
	</li>
	@endforeach
</ul>