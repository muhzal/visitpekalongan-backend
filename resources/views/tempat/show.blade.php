<div class="detail-tempat">
	<div class="swiper-container">
		<div class="swiper-wrapper">
			@foreach ($data->tempat->gambars as $gambar)
			<div class="swiper-slide">
				<img data-src="{{ url('img/large/'.$gambar->nama)}}" class="swiper-lazy">
				<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
			</div>
			@endforeach
		</div>
		<div class="swiper-pagination swiper-pagination-white">
		</div>
		<div class="swiper-button-next swiper-button-white">
		</div>
		<div class="swiper-button-prev swiper-button-white">
		</div>
	</div>
		@yield('rating')
	<ul class="collection">
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">loyalty</i>
			<span class="title">Nama</span>
			<p><b>{{ $data->tempat->nama }}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">place</i>
			<span class="title">Alamat</span>
			<p><b>{{ $data->tempat->alamat }}</b></p>
		</li>
		
		@yield('detail-tempat')

		<li class="collection-item avatar">
			<i class="material-icons circle cyan">person_outline</i>
			<span class="title">Author</span>
			<p><b>{{ $data->tempat->user->name.' ( '.$data->tempat->user->level->nama.' )'}}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">date_range</i>
			<span class="title">Tanggal Ditambahkan</span>
			<p><b>{{ $data->tempat->created_at->format('d M Y') }}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">info_outline</i>
			<span class="title">Keterangan</span>
			<p>{!! $data->tempat->keterangan !!}</p>
		</li>
	</ul>
	
	<div class="fixed-action-btn">
		<a  href="#modal-form" id="btn-edit-detail" class="modal-trigger btn-floating green btn-large" data-jenis="edit" data-id="{{ $data->id }}" data-lat="{{ $data->tempat->latitude  }}"  data-lng="{{ $data->tempat->longitude  }}">
			<i class="large material-icons">mode_edit</i>
		</a>
	</div>
</div>