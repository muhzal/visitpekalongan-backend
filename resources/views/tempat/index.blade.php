@extends('layouts.master')
@section('master-content')
@include('tag.modal-form')
@include('tag.modal-hapus')
@include('tag.modal-detail')
<div class="card ">
	<div class="card-content">
		<div id="map_canvas" style="height: 200px">
		</div>
	</div>
</div>
<ul class="data-tempat tabs card">
	<li class="tab col s3 hoverable"><a class="active tooltipped cyan-text" href="#wisata-page" data-model="wisata" data-position="bottom" data-delay="50" data-tooltip="Wisata"><i class="small material-icons" alt="Wisata">terrain</i></a></li>
	<li class="tab col s3 hoverable"><a class="tooltipped cyan-text" data-position="bottom" data-delay="50" data-tooltip="SPBU" href="#spbu-page" data-model="spbu"><i class="small material-icons">local_gas_station</i></a></li>
	<li class="tab col s3 hoverable"><a class="tooltipped cyan-text" data-position="bottom" data-delay="50" data-tooltip="ATM" href="#atm-page" data-model="atm"><i class="small material-icons">local_atm</i></a></li>
	<li class="tab col s3 hoverable"><a class="tooltipped cyan-text" data-position="bottom" data-delay="50" data-tooltip="Penginapan" href="#penginapan" data-model="penginapan"><i class="small material-icons">hotel</i></a></li>
	<li class="tab col s3 hoverable"><a class="tooltipped cyan-text" data-position="bottom" data-delay="50" data-tooltip="Restoran" href="#restoran" data-model="restoran"><i class="small material-icons">restaurant</i></a></li>
	<li class="tab col s3 hoverable"><a class="tooltipped cyan-text" data-position="bottom" data-delay="50" data-tooltip="Event" href="#event" data-model="event"><i class="small material-icons">date_range</i></a></li>
	<li class="tab col s3 hoverable"><a class="tooltipped cyan-text" data-position="bottom" data-delay="50" data-tooltip="Belanja" href="#belanja" data-model="belanja"><i class="small material-icons">local_grocery_store</i></a></li>
	<li class="tab col s3 hoverable"><a class="tooltipped cyan-text" data-position="bottom" data-delay="50" data-tooltip="Transportasi" href="#transportasi" data-model="transportasi"><i class="small material-icons">directions_bike</i></a></li>
</ul>
<div id="wisata-page" class="col s12">
	<div class="card-panel ">
		@include('wisata.table')
	</div>
</div>
<div id="spbu-page" class="col s12">
	<div class="card-panel ">
		@include('spbu.table')
	</div>
</div>
<div id="atm-page" class="col s12">
	<div class="card-panel ">
		@include('atm.table')
	</div>
</div>
<div id="penginapan" class="col s12">
	<div class="card-panel ">
		@include('penginapan.table')
	</div>
</div>
<div id="restoran" class="col s12">
	<div class="card-panel ">
		@include('restoran.table')
	</div>
</div>
<div id="event" class="col s12">
	<div class="card-panel ">
		@include('event.table')
	</div>
</div>
<div id="belanja" class="col s12">
	<div class="card-panel ">
		@include('belanja.table')
	</div>
</div>
<div id="transportasi" class="col s12">
	<div class="card-panel ">
		@include('transportasi.table')
	</div>
</div>
@stop
@push('jspluginlast')
<!-- <script src="{{ asset('js/map.js') }}"></script> -->
@endpush
@push('jsplugin')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcNQvwKsFaKwIziGDYCBFRBB5cXDETX_0"></script>
<script type ="text/javascript" src="{{asset('plugins/datatables/datatables.min.js')}}"></script>
<script type ="text/javascript" src="{{asset('vendor/datatables/buttons.server-side.js')}}"></script>
<!-- <script src="{{ asset('plugins/infobox/infobox.min.js') }}"></script> -->
<script src="{{ asset('plugins/selectize/js/selectize.min.js') }}"></script>
<script src="{{ asset('plugins/typeahead.js/typeahead.bundle.min.js') }}"></script>
<script src="{{ asset('plugins/ckeditor-full/ckeditor.js') }}"></script>
<script src="{{ asset('plugins/swiper/js/swiper.min.js') }}"></script>
<script src="{{ asset('js/form/table.js') }}"></script>
<script src="{{ asset('js/form/map-form.js') }}"></script>
<script src="{{ asset('js/form/form.js') }}" media="screen,projection"></script>
@endpush
@push('cssplugin')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/swiper/css/swiper.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{ asset('plugins/selectize/css/selectize.default.css') }}"/>
<link type="text/css" rel="stylesheet" href="{{ asset('css/form.css') }}"/>
@endpush