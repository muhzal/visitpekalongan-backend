<form action="{{ $action }}" method="POST" enctype="multipart/form-data" id="form-tempat" class="small col s12">
  <div class="modal-content">
    {!! csrf_field() !!}
    {!! method_field('POST') !!}
    <input type="hidden" id="latitude" name="tempat[latitude]" value="0">
    <input type="hidden" id="longitude" name="tempat[longitude]" value="0">
    <div class="row">
      <div class="input-field col s12">
        <input type="text" name="tempat[nama]" class="validate" required="required">
        <label for="nama">Nama Tempat</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <input type="text" class="validate alamat typeahead" name="tempat[alamat]" required="required">
        <input type="hidden" name="tempat[user_id]" required="required" value="{{ Auth::user()->id }}">
        <label for="alamat" class="active">Alamat</label>
      </div>
    </div>
    @yield('form-tempat')
    <div class="row">
      <div class="col s12">
        <label for="">Terbitkan Lokasi </label>
        <div class="switch">
          <label>
            Tidak
            <input type="checkbox" name="tempat[publish]" value=1>
            <span class="lever"></span>
            Ya
          </label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <p for="keterangan">Keterangan</p>
        <textarea class="mtextarea materialize-textarea" required="required" id="keterangan" name="tempat[keterangan]"></textarea>
      </div>
    </div>
    <!-- <input type="hidden" name="tempat[publish]" value="1"> -->
    {{-- @include('tag.image-upload') --}}
    <div class="row">
      <div class="file-field input-field col s12">
        <div class="btn cyan">
          <!-- <i class="mdi-image-camera-alt prefix"></i> -->
          <i class="large material-icons">camera_alt</i>
          <input type="file" name="images[]" id="images" accept="image.*" multiple>
        </div>
        <div class="file-path-wrapper">
          <input class="file-path validate" type="text" placeholder="Upload one or more files">
        </div>
      </div>
    </div>
    <div class="row" id="images-preview">
    </div>
    </div><!-- modal-content -->
    <div class="modal-footer">
      <button class="btn blue white-text waves-effect waves-light btn-submit" type="submit">
      <i class="small mdi-content-save"></i> Simpan
      </button>
      <button class="btn btn-flat waves-effect waves-light btn-back" type="button">
      <i class="small mdi-navigation-arrow-back"></i> Kembali
      </button>
    </div>
  </form>