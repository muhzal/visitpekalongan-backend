<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css--> <title>Reset Password</title>
        <link type="text/css" rel="stylesheet" href="{{asset('plugins/materialize/css/materialize.min.css')}}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{asset('css/custom.css')}}"  media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style type="text/css" media="screen">
        html {
        display: table;
        margin: auto;
        }
        html, body {
        height: 100%;
        }
        body {
        display: table-cell;
        vertical-align: middle;
        }
        html, body {
        height: 100%;
        }
        .login-form {
        width: 400px;
        }
        input:not([type])+label:after, input[type=text]+label:after, input[type=password]+label:after, input[type=email]+label:after, input[type=url]+label:after, input[type=time]+label:after, input[type=date]+label:after, input[type=datetime]+label:after, input[type=datetime-local]+label:after, input[type=tel]+label:after, input[type=number]+label:after, input[type=search]+label:after, textarea.materialize-textarea+label:after {
            display: block;
            content: "";
            position: absolute;
            top: 35px;
            opacity: 0;
            transition: .2s opacity ease-out, .2s color ease-out;
        }
        </style>
    </head>
    <body class="cyan">
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form class="login-form form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
            <div class="row">
                <div class="input-field col s12 center">
                    <!-- <img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login"> -->
                    <h5>Reset Password</h5>
            
                </div>
            </div>
            {{ csrf_field() }}
            @if(count($errors) > 0)                    
                    <div class="card red lighten-5">
                        <div class="card-content red-text">
                            @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    </div>
            @elseif (session('status'))
                <div class="card green lighten-5">
                    <div class="card-content green-text">
                         {{ session('status') }}
                    </div>                           
                </div>
            @else
                    <p class="center login-form-text green-text">Silahkan Masukan Email yang didaftarkan</p>
            @endif
            <div class="row margin">
                <div class="input-field col s12">
                    <!-- <i class="mdi-social-person-outline prefix"></i> -->
                    <i class="material-icons prefix">person_outline</i>
                    <input id="email" type="email" class="validate {{ $errors->has('email') ? ' invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    <label for="email">E-mail</label>                            
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button type="submit" class="btn waves-effect waves-light col s12">Send Password Reset Link</button>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <p class="margin medium-small">Kembali ke halaman <a href="{{ url('/login') }}">login</a></p>
                </div>
            </div>
        </form>
    </div>
</div>
        <script type="text/javascript" src="{{asset('js/jquery-1.11.2.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('plugins/materialize/js/materialize.min.js')}}"></script>
    </body>
</html>