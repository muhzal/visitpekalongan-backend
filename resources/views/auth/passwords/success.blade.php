<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="{{asset('plugins/materialize/css/materialize.min.css')}}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{asset('css/custom.css')}}"  media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <title>Reset Password</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style type="text/css" media="screen">
        html {
        display: table;
        margin: auto;
        }
        html, body {
        height: 100%;
        }
        body {
        display: table-cell;
        vertical-align: middle;
        }
        html, body {
        height: 100%;
        }
        .login-form {
        width: 400px;
        }
        input:not([type])+label:after, input[type=text]+label:after, input[type=password]+label:after, input[type=email]+label:after, input[type=url]+label:after, input[type=time]+label:after, input[type=date]+label:after, input[type=datetime]+label:after, input[type=datetime-local]+label:after, input[type=tel]+label:after, input[type=number]+label:after, input[type=search]+label:after, textarea.materialize-textarea+label:after {
            display: block;
            content: "";
            position: absolute;
            top: 35px;
            opacity: 0;
            transition: .2s opacity ease-out, .2s color ease-out;
        }
        </style>
    </head>
    <body class="cyan">
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form class="login-form form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
            <div class="row">
                <div class="input-field col s12 center">
                    <!-- <img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login"> -->
                    <h5>Reset Password</h5>
            
                </div>
            </div>
                    
            <div class="row margin">
                <div class="input-field col s12">
                    <div class="card green lighten-5">
                    <div class="card-content green-text">
                       Password Berhasil diubah
                    </div>                           
                </div>                      
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m12 l12">
                    <!-- <p class="margin medium-small">Kembali ke halaman <a href="{{ url('/login') }}">login</a></p> -->
                </div>
            </div>
        </form>
    </div>
</div>
        <script type="text/javascript" src="{{asset('js/jquery-1.11.2.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('plugins/materialize/js/materialize.min.js')}}"></script>
    </body>
</html>