<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css--> <title>Reset Password</title>
        <link type="text/css" rel="stylesheet" href="{{asset('plugins/materialize/css/materialize.min.css')}}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{asset('css/custom.css')}}"  media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style type="text/css" media="screen">
        html {
        display: table;
        margin: auto;
        }
        html, body {
        height: 100%;
        }
        body {
        display: table-cell;
        vertical-align: middle;
        }
        html, body {
        height: 100%;
        }
        .login-form {
        width: 400px;
        }
        </style>
    </head>
    <body class="cyan">
        <div id="login-page" class="row">
            <div class="col s12 z-depth-4 card-panel">
                <form class="login-form form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                    <div class="row">
                        <div class="input-field col s12 center">
                            <h5>Password Baru</h5>                           
                            @if (count($errors) > 0)
                            <div class="card red lighten-5">
                                <div class="card-content red-text">
                                    @foreach ($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                            @else
                             <p class="center login-form-text green-text">Silahkan Masukkan Password Baru</p>
                            @endif
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="hidden" name="email" value="{{ $email or old('email') }}">
<!--                     <div class="row margin">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person_outline</i>
                            <input id="email" type="email" class="validate {{ $errors->has('email') ? ' invalid' : '' }}" name="email" value="{{ $email or old('email') }}" required disabled/>
                            <label for="email" >E-mail</label>
                        </div>
                    </div> -->
                    <div class="row margin">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">lock_outline</i>
                            <input type="password" class="form-control" name="password"  class="validate {{ $errors->has('password') ? ' invalid' : '' }}"  required>
                            <label for="password" >Password Baru</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">lock_outline</i>
                            <input type="password" class="form-control" name="password_confirmation"  class="validate {{ $errors->has('password_confirmation') ? ' invalid' : '' }}"  required>
                            <label for="password_confirmation" >Konfirmasi Password Baru</label>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="input-field col s12">
                            <button type="submit" class="btn waves-effect waves-light col s12">Simpan Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="{{asset('js/jquery-1.11.2.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('plugins/materialize/js/materialize.min.js')}}"></script>
    </body>
</html>
