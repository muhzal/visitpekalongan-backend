
	<table cellspacing="0" cellpadding="0" style="padding:0;width:100%;font-family:Verdana,Tahoma,Arial,Helvetica,sans-serif;font-size:14px">
		<tbody>
			<tr>
				<td style="background-color:#fff;border-bottom:8px solid #f2f3f7;padding:0px 10px 0px;color:#fff;font-weight:bold;text-align:center;color:#000">
					<h2>Pekalongan Vacation</h2>
				</td>
			</tr>
			<tr>
				<td style="background-color:#fff;border:1px solid #fff;padding:20px">
					<p>Hi, {{ $user->name }}</p>
					<p>Kami telah menerima permintaan Anda untuk memulihkan informasi akun kalongcation Anda.</p>
					<p>
						Untuk mengganti kata sandi silakan klik tautan ini: <br>
						<a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}" style="color:#3081a3" target="_blank">{{ $link }}</a>
					</p><br>
					<p>Jika Anda tidak pernah meminta ini, abaikan email ini. jangan kawatir, password anda tidak berubah</p><br>
					<p>Terima Kasih</p>
					<p>Kalongacation</p>
				</td>
			</tr>
		</tbody>
	</table>
<!-- Click here to reset your password: <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a> -->