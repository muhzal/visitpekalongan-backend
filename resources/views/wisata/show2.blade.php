@extends('layouts.master')
@section('master-content')
<div class="row">
	<div class="col s12 m6 l4">
		<div class="row">
			<div class="col s12">
				<div class="row">						
						<div class="col s8">
							<select name="id" id="select-wisata">
								@foreach ($wisata as $w)
								<option value="{{ $w->id }}" @if($w->id == $data->id) selected @endif>{{ $w->tempat->nama }}</option>
								@endforeach
							</select>
						</div>
						<div class="col s4">
							<button type="button" class="waves-effect waves-teal btn large cyan pindah-wisata">Pilih</button>
						</div>						
				</div>
				<div class="card">
					<div class="card-image">
						<div class="carousel">
							@if ($data->tempat->gambars->count())
							@foreach ( $data->tempat->gambars as $gambar)
							<a class="carousel-item" href="#!"><img src="../img/medium/{{  $gambar->nama }}"></a>
							@endforeach
							@endif
						</div>
						<span class="card-title activator">{{ $data->tempat->nama }}</span>
					</div>
					<div class="card-content">
						<p class="center orange-text">
							@if ($data->rate)
								@for ($i = 0; $i < (int) $data->rate; $i++)
								<i class="material-icons">star</i>
								@endfor
								
								@if ( 5 - (int) $data->rate > 5 - $data->rate)
								<i class="material-icons">star_half</i>
								@endif
								@if ( (int) (5 - $data->rate) > 0)
								@for ($i = 0; $i < (int) (5 - $data->rate); $i++)
								<i class="material-icons">star_border</i>
								@endfor
								@endif
							@else
								@for ($i = 0; $i < 5; $i++)
								<i class="material-icons">star_border</i>
								@endfor
							@endif
						</p>
						<h5 class="green-text center">{{ $data->rate }}</h5>
						<br>
						<p><i class="material-icons small left">loyalty</i>{{ $data->jeniswisata->nama }}</p><br>
						<p><i class="material-icons small left">group</i>{{ $data->pengelola or '-' }}</p><br>
						<p><i class="material-icons small left">phone</i>{!! $data->telepon ? $data->telepon : '  -  <br>' !!}</p>
					</div>
					<div class="card-reveal">
						<span class="card-title grey-text text-darken-4">{{ $data->tempat->nama }}<i class="material-icons right">close</i></span>
						<p>{{ $data->tempat->alamat }}</p>
						<p><i class="material-icons small left">loyalty</i>{{ $data->jeniswisata->nama }}</p>
						<p><i class="material-icons small left">group</i>{!! $data->pengelola ? $data->pengelola : '  -  <br>' !!}</p>
						<p><i class="material-icons small left">phone</i>{!! $data->telepon ? $data->telepon : '  -  <br>' !!}</p>
						<p><i class="material-icons small left">access_time</i> {{ $data->waktu_buka }} - {{ $data->waktu_tutup }}</p>
						@if (!$data->is_tiket)
						<p><i class="material-icons small left">money_off</i> Tidak ada tiket</p>
						@endif
						@if($data->fasilitas->count() AND $data->fasilitas->contains('Tidak Ada'))
						<ul class="collection with-header">
							<li class="collection-item"><i class="material-icons small left">list</i>Fasilitas</li>
							@foreach ($data->fasilitas as $fas)
							<li class="collection-item"><i class="material-icons small left">check</i>{{ $fas->nama }}</li>
							@endforeach
						</ul>
						@endif
					</div>
					<div class="card-action right-align">
						<a href="#" class="activator  black-text"><i class="material-icons small right">keyboard_arrow_down</i></a>
					</div>
				</div>
			</div>
		</div>
		@if ($data->is_tiket)
		<div class="row">
			<div class="col s12">
				<ul class="collection with-header" id="tiket">
					<li class="collection-item green white-text"><h5><i class="material-icons left">credit_card</i>Tiket</h5></li>
					@foreach ($data->tikets as $tiket)
					
					<li class="collection-item">
						<span class="title">{{ $tiket->nama }}</span>
						<p>
							Rp {{ $tiket->harga }} {{ $tiket->operasional }}
						</p>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
		@endif
	</div>
	<div class="col s12 m6 l4">
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<span class="card-title"><strong>About {{ $data->tempat->nama }}</strong></span>
						{!! $data->tempat->keterangan !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col s12 m6 l4">
		<ul class="collection with-header avatar" id="review">
			<li class="collection-item  orange white-text">
				<span class="badge green white-text">Total {{ $reviews->total() }}</span>
				<h5>
				<i class="material-icons left">message</i>Review User
				</h5>
			</li>
			<div id="review-loader" class="valign-wrapper hide">
				<div class="preloader-wrapper big active">
					<div class="spinner-layer spinner-blue">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
					<div class="spinner-layer spinner-red">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
					<div class="spinner-layer spinner-yellow">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
					<div class="spinner-layer spinner-green">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
			</div>
			<div id="review-item">
				@if ($reviews->total())
				@include('wisata.review')
				@else
				<p class="center" style="margin :10px auto">Tidak ada review</p>
				@endif
			</div>
		</ul>
	</li>
</ul>
</div>
</div>
</div>
@endsection
@push('cssplugin')
<link type="text/css" rel="stylesheet" href="{{ asset('css/detail.css') }}"/>
@endpush
@push('jsplugin')
<script src="{{ asset('js/detail.js') }}"></script>
@endpush