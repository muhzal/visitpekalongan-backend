@foreach ($reviews as $review)
<li class="collection-item">
	<!-- <i class="material-icons circle">person</i> -->
	<a href="#!" class="secondary-content">
		@if ($review)
		<i>{{ $review->created_at->format('d M Y G:i') }}</i>
		@else		
		<i>{{ $review->created_at->diffForHumans() }}</i>
		@endif
	</a>
	<span class="title"><strong>{{ $review->user->name }}</strong></span><br>
	<i>{{ $review->judul }}</i><br>
	@for ($i = 0; $i < $review->rating; $i++)
	<i class="material-icons tiny orange-text">star</i>
	@endfor
	@for($i=0;$i< (int) (5 - $review->rating);$i++ )
	<i class="material-icons tiny orange-text">star_border</i>
	@endfor
	<br>
	{{ $review->review }}
	<a href="#" title="" class="btn-delete-review" data-id="{{ $review->id }}"><span class="badge"><i class="material-icons small red-text">delete_forever</i></span></a>
	
</li>
@endforeach
{{-- <!-- <li class="collection-item center">
	<ul class="pagination">
		@if($reviews->previousPageUrl())
		<li>
			<a href="{{ $reviews->previousPageUrl() }}">
				<i class="material-icons">chevron_left</i>
			</a>
		</li>
		@else
		<li class="disabled">
			<a   disabled='disabled'>
				<i class="material-icons">chevron_left</i>
			</a>
		</li>
		@endif
		@for ($i = 1; $i <= $reviews->lastPage(); $i++)
		@if($reviews->currentPage() == $i)
		<li class="active">
			<a disabled='disabled'>{{ $i }}</a>
		</li>
		@else
		<li>
			<a href="{{ $reviews->url($i) }}">{{ $i }}</a>
		</li>
		@endif
		@endfor
		
		@if($reviews->nextPageUrl())
		<li>
			<a href="{{ $reviews->nextPageUrl() }}">
				<i class="material-icons">chevron_right</i>
			</a>
		</li>
		@else
		<li class="disabled">
			<a  disabled='disabled' >
				<i class="material-icons">chevron_right</i>
			</a>
		</li>
		@endif
	</ul>
</li> -->
 --}}