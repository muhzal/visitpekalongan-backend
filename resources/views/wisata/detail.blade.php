@extends('layouts.master')
@section('master-content')
<div class="row">
	<div class="col s12 m6 l4">
		<div class="row">
			<div class="col s12">
				<div class="row">
					<div class="col s8">
						<select name="">
							<option value="">Museum Batik</option>
							<option value="">Desa Wisata Lolong</option>
						</select>
					</div>
					<div class="col s4">
						<button type="button" class="waves-effect waves-teal btn large cyan">Pilih</button>
					</div>
				</div>
				<div class="card">
					<div class="card-image">
						<div class="carousel">
							<a class="carousel-item" href="#one!"><img src="../img/medium/material5.png"></a>
							<a class="carousel-item" href="#two!"><img src="../img/medium/material6.png"></a>
							<a class="carousel-item" href="#three!"><img src="../img/medium/material7.png"></a>
							<a class="carousel-item" href="#four!"><img src="../img/medium/material8.png"></a>
							<a class="carousel-item" href="#five!"><img src="../img/medium/material9.png"></a>
						</div>
						<span class="card-title activator">Curug Muncar</span>
					</div>
					<div class="card-content">
						<p class="center orange-text">
							<i class="material-icons">star</i>
							<i class="material-icons">star</i>
							<i class="material-icons">star</i>
							<i class="material-icons">star_half</i>
							<i class="material-icons">star_border</i>
						</p>
						<h5 class="green-text center">3.5</h5>
						<br>
						<p><i class="material-icons small left">loyalty</i>Wisata Alam</p><br>
						<p><i class="material-icons small left">group</i>Dinas Pariwisata</p><br>
						<p><i class="material-icons small left">phone</i>021 455442</p>
					</div>
					<div class="card-reveal">
						<span class="card-title grey-text text-darken-4">Curug Muncar<i class="material-icons right">close</i></span>
						<p>Desa Curugmuncar, Kecamatan Petungkriono, Kabupaten Pekalongan</p>
						<p><i class="material-icons small left">loyalty</i>Wisata Alam</p>
						<p><i class="material-icons small left">group</i>Dinas Pariwisata</p>
						<p><i class="material-icons small left">phone</i>021 455442</p>
						<p><i class="material-icons small left">access_time</i> 07.00 - 17.00</p>
						<p><i class="material-icons small left">money_off</i> Tidak ada tiket</p>
					</div>
					<div class="card-action right-align">
						<a href="#" class="activator  black-text"><i class="material-icons small right">keyboard_arrow_down</i></a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<ul class="collection with-header" id="tiket">
					<li class="collection-item green white-text"><h5><i class="material-icons left">credit_card</i>Tiket</h5></li>
					<li class="collection-item">
						<span class="title">Parkir</span>
						<p>
							Rp 2.000 Hari Biasa
						</p>
					</li>
					<li class="collection-item">
						<span class="title">Masuk</span>
						<p>
							Rp 12.000 Hari Biasa
						</p>
					</li>
					<li class="collection-item">
						<span class="title">Masuk</span>
						<p>
							Rp 20.000 Hari Libur
						</p>
					</li>
					<li class="collection-item">
						<span class="title">Wahana Air</span>
						<p>
							Rp 12.000 Hari Biasa
						</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col s12 m6 l4">
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<span class="card-title"><strong>About Curug Muncar</strong></span>
						<p>I am a very simple card. I am good at containing small bits of information.
							I am convenient because I require little markup to use effectively. I am a very simple card. I am good at containing small bits of information.
							I am convenient because I require little markup to use effectively. I am a very simple card. I am good at containing small bits of information.
							I am convenient because I require little markup to use effectively. I am a very simple card. I am good at containing small bits of information.
							I am convenient because I require little markup to use effectively. I am a very simple card. I am good at containing small bits of information.
							I am convenient because I require little markup to use effectively. I am a very simple card. I am good at containing small bits of information.
						I am convenient because I require little markup to use effectively.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col s12 m6 l4">
		<ul class="collection with-header avatar" id="review">
			<li class="collection-item  orange white-text">
				<span class="badge green white-text">Total 40</span>
				<h5>
				<i class="material-icons left">message</i>Review User
				</h5>
			</li>
			<li class="collection-item avatar">
				<i class="material-icons circle">person</i>
				<span class="title"><strong>Good</strong></span>
				<p>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<br>
					Tempat yang sungguh menakjubkan
					<a href="#" title=""><span class="badge"><i class="material-icons tiny">delete_forever</i></span></a>
				</p>
				<a href="#!" class="secondary-content">
					<i>5 hari yang lalu</i>
				</a>
			</li>
			<li class="collection-item avatar">
				<i class="material-icons circle">person</i>
				<span class="title"><strong>Good</strong></span>
				<p>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<br>
					Tempat yang sungguh menakjubkan
					<a href="#" title=""><span class="badge"><i class="material-icons tiny">delete_forever</i></span></a>
				</p>
				<a href="#!" class="secondary-content">
					<i>5 hari yang lalu</i>
				</a>
			</li>
			<li class="collection-item avatar">
				<i class="material-icons circle">person</i>
				<span class="title"><strong>Good</strong></span>
				<p>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star</i>
					<i class="material-icons tiny">star_half</i>
					<i class="material-icons tiny">star_border</i>
					<br>
					Tempat yang sungguh menakjubkan
					<a href="#" title=""><span class="badge"><i class="material-icons tiny">delete_forever</i></span></a>
				</p>
				<a href="#!" class="secondary-content">
					<i>5 hari yang lalu</i>
				</a>
			</li>
			<li class="collection-item center">
				<ul class="pagination">
					<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
					<li class="active"><a href="#!">1</a></li>
					<li class="waves-effect"><a href="#!">2</a></li>
					<li class="waves-effect"><a href="#!">3</a></li>
					<li class="waves-effect"><a href="#!">4</a></li>
					<li class="waves-effect"><a href="#!">5</a></li>
					<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>
</div>
@endsection
@push('cssplugin')
<link type="text/css" rel="stylesheet" href="{{ asset('css/detail.css') }}"/>
@endpush
@push('jsplugin')
<script src="{{ asset('js/detail.js') }}"></script>
@endpush