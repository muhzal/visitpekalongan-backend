
@section('form-tempat')
    <div class="row">
      <div class="input-field col s12">
        <select  placeholder="pilih jenis wisata..."  class="validate jenis-wisata materialize-select" name="jeniswisata_id" required="required">
            <option value="" disabled selected>Pilih jenis wisata</option>
          @foreach ($jeniswisata as $jw)
            <option value="{{ $jw->id }}">{{ $jw->nama }}</option>
          @endforeach

        </select>
        <label for="jenis wisata" class="">Jenis Wisata</label>
      </div>
   </div>
    <div class="row">
      <div class="input-field col s12">
        <input type="number" class="validate" name="telepon" >
        <label for="telepon">Telepon</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <input type="text" class="validate" name="pengelola" required="required">
        <label for="pengelola">Pengelola</label>
      </div>
    </div>
   <div class="row">
      <div class="input-field col s12 m6 l6">
        <select id="selectize"  placeholder="pilih waktu buka..."  class="validate waktu materialize-select" name="waktu_buka" required="required">
          <option value="" disabled selected>Pilih waktu buka</option>
          @for ($i = 0; $i <= 24; $i++)
            <option value="{{ $i+00 }}:00" >{{ $i+00 }}:00</option>
          @endfor
        </select>
        <label for="open" class="active">Waktu Buka</label>
      </div>
      <div class="input-field col s12 m6 l6">
        <select id="selectize"  placeholder="pilih waktu tutup..." class="validate waktu materialize-select" name="waktu_tutup" required="required">
              <option value="" disabled selected>Pilih waktu tutup</option>
            @for ($i = 0; $i <= 24; $i++)
              <option value="{{ $i+00 }}:00" >{{ $i+00 }}:00</option>
            @endfor
        </select>
        <label for="tutup" class="active">Waktu Tutup</label>
      </div>
   </div>
   <div class="row">
      <div class="input-field col s12">
        <select id="selectize"  placeholder="Masukkan fasilitas wisata..." data-role="materialtags" class="selectize validate fasilitas" name="fasilitas[]" multiple required="required">
          <option value="" disabled>Pilih fasilitas wisata</option>
          @if (isset($fasilitas))
            @foreach ($fasilitas as $fas)
              <option value="{{ $fas->nama }}">{{ $fas->nama }}</option>
            @endforeach
          @endif
        </select>
        <label for="fasilitas" class="active">Fasilitas </label>
      </div>
   </div>
  <div class="row">
    <div class="col s12">
          <label for="">Tiket </label>
       <div class="switch">
          <label>
            Tidak Ada
            <input type="checkbox" name="is_tiket" id="is_tiket" value="1">
            <span class="lever"></span>
            Ada
          </label>
        </div>
    </div>
  </div>

  <div class="row" id="tiket-panel" style="display: none;">
    <div class="col s12">
      <div class="card-panel collection">



      </div>
            <button type="button" class="btn btn-medium btn-tiket-add">Tambah tiket</button>
    </div>
  </div>
@stop
@include('tempat.form-tambah-tempat', array('action' => url('wisata')))