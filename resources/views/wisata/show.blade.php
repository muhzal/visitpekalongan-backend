@extends('tempat.show')
@section('rating')
		<p class="center"> rating tempat ini</p>
		<p class="center orange-text">			
			@if ($data->rate)
				@for ($i = 0; $i < (int) $data->rate; $i++)
				<i class="material-icons">star</i>
				@endfor
				
				@if ( 5 - (int) $data->rate > 5 - $data->rate)
				<i class="material-icons">star_half</i>
				@endif
				@if ( (int) (5 - $data->rate) > 0)
				@for ($i = 0; $i < (int) (5 - $data->rate); $i++)
				<i class="material-icons">star_border</i>
				@endfor
				@endif
			@else
				@for ($i = 0; $i < 5; $i++)
				<i class="material-icons">star_border</i>
				@endfor
			@endif
		</p>
		<h5 class="green-text center">{{sprintf('%0.1f', $data->rate) }}</h5>
@stop

@section('detail-tempat')
<li class="collection-item avatar">
	<i class="material-icons circle cyan">loyalty</i>
	<span class="title">Jenis wisata</span>
	<p><b>{{ $data->jeniswisata->nama }}</b></p>
</li>
<li class="collection-item avatar">
	<i class="material-icons circle cyan">contact_phone</i>
	<span class="title">Telepon</span>
	<p><b>{{ $data->telepon or "-" }}</b></p>
</li>
<li class="collection-item avatar">
	<i class="material-icons circle cyan">group</i>
	<span class="title">Pengelola</span>
	<p><b>{{ $data->pengelola }}</b></p>
</li>
<li class="collection-item avatar">
	<i class="material-icons circle cyan">access_time</i>
	<span class="title">Jam Buka</span>
	<p><b>{{ $data->waktu_buka }} - {{ $data->waktu_tutup }}</b></p>
</li>
 @if ($data->is_tiket)   
<ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header collection-item avatar">
      	<i class="material-icons circle cyan white-text">credit_card</i>Tiket
        		
			<br><b>Ada</b><a href="#!" class="secondary-content">
			<i class="material-icons black-text">arrow_drop_down_circle</i></a>

      </div>
      <div class="collapsible-body">
      	<ul class="collection" id="tiket">
		@foreach ($data->tikets as $tiket)			
			<li class="collection-item avatar">
				<span class="title">Tiket {{ $tiket->nama }} : </span>				
					Rp {{ $tiket->harga }} ( {{ $tiket->operasional }} )
			</li>
		@endforeach
		</ul>
      </div>
    </li>
  </ul>		
  @else
<li class="collection-item avatar">
	<i class="material-icons circle cyan">credit_card</i>
	<span class="title">Tiket</span>
	<br><b>Tidak Ada</b>
</li>

  @endif
@endsection