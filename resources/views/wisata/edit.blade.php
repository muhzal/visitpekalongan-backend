@section('form-tempat')
<div class="row">
  <div class="input-field col s12">
    <select  placeholder="pilih jenis wisata..."  class="validate jenis-wisata materialize-select" name="jeniswisata_id" required="required">
      <option value="" disabled selected>Pilih jenis wisata</option>
      @foreach ($jeniswisata as $jw)
      <option value="{{ $jw->id }}" @if ($jw->id==$data->jeniswisata->id) selected="selected" @endif>{{ $jw->nama }}</option>
      @endforeach
    </select>
    <label for="jeniswisata_id" class="active">Jenis Wisata</label>
  </div>
</div>
<div class="row">
  <div class="input-field col s12">
    <input type="number" class="validate" name="telepon" id="telepon" value="{{ $data->telepon }}">
    <label for="telepon" class="active">Telepon</label>
  </div>
</div>
<div class="row">
  <div class="input-field col s12">
    <input type="text" class="validate" name="pengelola" required="required" value="{{ $data->pengelola }}">
    <label for="pengelola" class="active">Pengelola</label>
  </div>
</div>
<div class="row">
  <div class="input-field col s12 m6 l6">
    <select id="selectize"  placeholder="pilih waktu buka..."  class="validate waktu materialize-select" name="waktu_buka" required="required">
      <option value="" disabled selected>Pilih waktu buka</option>
      @for ($i = 0; $i < 24; $i++)
      {{ $time = date_format(date_create($i.':00'),'H:i') }}
      <option value="{{ $time }}" @if($time.':00' == $data->waktu_buka) selected @endif > {{ $time }}</option>
      @endfor
    </select>
    <label for="bank" class="active">Waktu Buka</label>
  </div>
  <div class="input-field col s12 m6 l6">
    <select id="selectize"  placeholder="pilih waktu tutup..." class="validate waktu materialize-select" name="waktu_tutup" required="required">
      <option value="" disabled selected>Pilih waktu tutup</option>
      @for ($i = 0; $i <= 24; $i++)
      {{ $time = date_format(date_create($i.':00'),'H:i') }}
      <option value="{{ $time }}" @if($time.':00' == $data->waktu_tutup) selected @endif > {{ $time }}</option>
      @endfor
    </select>
    <label for="bank" class="active">Waktu Tutup</label>
  </div>
</div>
<div class="row">
  <div class="input-field col s12">
    <select id="selectize"  placeholder="Masukkan fasilitas wisata..." data-role="materialtags" class="selectize validate fasilitas" name="fasilitas[]" multiple required="required">
      <option value="" disabled>Pilih fasilitas wisata</option>
      @if (isset($fasilitas))
      @foreach ($fasilitas as $fas)
      <option value="{{ $fas->nama }}" @if($data->fasilitas->contains('nama',$fas->nama)) selected  @endif>{{ $fas->nama }}</option>
      @endforeach
      @endif
    </select>
    <label for="fasilitas" class="active">Fasilitas </label>
  </div>
</div>
<div class="row">
  <div class="col s12">
    <label for="">Tiket </label>
    <div class="switch">
      <label>
        Tidak Ada
        <input type="checkbox" name="is_tiket" id="is_tiket" value="1" @if ($data->is_tiket) checked="checked"  @endif>
        <span class="lever" ></span>
        Ada
      </label>
    </div>
  </div>
</div>
<div class="row" id="tiket-panel" style="display: none;">
  <div class="col s12">
    <div class="card-panel">
      @for($i=0; $i<$data->tikets->count(); $i++)
      <div class="row tiket-item" data-id="{{ $i }}">
        <div class="input-field col s4">
          <input type="hidden" name="tiket[{{ $i }}][id]" value="{{ $data->tikets[$i]->id }}">
          <input class="kategori" type="text" name="tiket[{{ $i }}][nama]" placeholder="nama tiket" id="nama-tiket" value="{{ $data->tikets[$i]->nama }}">
          <label for="nama-tiket" class="active">Jenis Tiket</label>
        </div>
        <div class="input-field col s4">
          <input type="number" class="validate harga" name="tiket[{{ $i }}][harga]" required="required" value="{{ $data->tikets[$i]->harga }}">
          <label for="harga" class="active">Harga</label>
        </div>
        <div class="input-field col s3">
          <input class="operasional" type="text" name="tiket[{{ $i }}][operasional]" placeholder="untuk hari..." value="{{ $data->tikets[$i]->operasional }}">
          <label class="active">Hari</label>
        </div>
        <div class="input-field col s1">
          <button type="button" class="btn-small btn-tiket-clear btn red"><i class="material-icons">cancel</i></button>
        </div>
      </div>
      @endfor
    </div>
    <button type="button" class="btn btn-medium btn-tiket-add">Tambah tiket</button>
  </div>
</div>
@stop
@include('tempat.form-edit-tempat', array('action'=>route('wisata.update',['id' => $data->id]),'tempat'=>$data->tempat))