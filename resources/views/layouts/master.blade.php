 <!DOCTYPE html>
  <html>
    <head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">    
    <title>{{ $page_title or 'Admin Kalongcation' }}</title>
    <link rel="shortcut icon" href="images/other/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/other/favicon.ico" type="image/x-icon">
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">

      @stack('cssplugin')
      <link type="text/css" rel="stylesheet" href="{{asset('css/custom.css')}}"  media="screen,projection"/>
      @stack('css')
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
        <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <div class="navbar">
      <nav class="cyan">
        <div class="nav-wrapper">

          <a href="#" data-activates="slide-out" class="left sidebar-collapse">
            <!-- <i class="mdi-navigation-menu"></i> -->
            <i class="material-icons">menu</i>
          </a>
          <a href="#" class="center brand-logo">Kalongcation</a>
          <ul class="right hide-on-me-and-down">
           <li> 
              <a class='dropdown-button cyan waves-effect waves-light' href='#' data-activates='dropdown-setting'>
              <!-- <i class="medium mdi-action-settings"></i> --><i class="material-icons">settings</i></li></a>     
              <ul id='dropdown-setting' data-constrainwidth="false" data-alignment="center" class='dropdown-content'>
                <li><a href="#modal-profile">Profile</a></li>
                <!-- <li><a href="#">Akun</a></li> -->
                <li><a href="{{url('logout')}}">Logout</a></li>
              </ul>           
          </ul>
        </div>
      </nav>
    </div>

  


    @if(Auth::check())
    @include('tag.modal-profile')
    @endif
    <div id="main">
      <div class="wrapper">
        <aside id="left-sidebar-nav">          
            <ul id="slide-out" class="side-nav fixed leftside-navigation">
              <li><a href="{{ url('/') }}" class="waves-effect waves-light tooltipped" data-position="right" data-delay="50" data-tooltip="Beranda">
              <!-- <i class="small mdi-action-home"> -->
                <i class="material-icons">home</i>
              </i>Beranda</a></li>
              <li><a href="{{ url('usulan') }}" class="waves-effect waves-light tooltipped" data-position="right" data-delay="50" data-tooltip="Daftar Usulan">
              <i class="material-icons">assignment</i> Usulan</a>
              </li>
               <li><a href="{{ url('tempat') }}" class="waves-effect waves-light tooltipped" data-position="right" data-delay="50" data-tooltip="Daftar Lokasi">
              <i class="material-icons">place</i>Lokasi</a>
              </li>
              @if (Auth::check() && Auth::user()->level_id == 1)
              <li>
                <a href="{{ url('user') }}" class="waves-effect waves-light tooltipped"  data-position="right" data-delay="50" data-tooltip="Daftar User"><!-- <i class="small mdi-social-people"></i> --><i class="material-icons">people</i>User</a>
              </li>                            
              @endif
              <li><a href="{{ url('bantuan') }}" class="waves-effect waves-light tooltipped"  data-position="right" data-delay="50" data-tooltip="Help"><!-- <i class="small mdi-social-person"></i> --><i class="material-icons">help</i>Bantuan</a></li>
            </ul>
        </aside> <!-- left-sidebar-nav -->



        <section id="content">
        <div class="container">
          <div class="section">
    
            @yield('master-content')

          </div> <!-- container -->
        </div> <!-- section -->
        </section><!-- Content -->



      </div><!-- wrapper -->
    </div><!-- main-->


      <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>

      <script>        
          var cookie = document.cookie;
          $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                      // 'X-XSRF-TOKEN': cookie,
                  }
          });
      </script>
      @stack('jsplugin')

      <!-- <script type="text/javascript" src="{{asset('js/custom.js')}}"></script> -->
      <script type="text/javascript" src="{{asset('js/master.js')}}"></script>

      <script type="text/javascript">      
          @stack('jsinit')

          var model_id;          
      </script>

      <script type="text/javascript">
      $(function() {
         "use strict";        
          @stack('js')
      });
          
      </script>
      @stack('jspluginlast')
    </body>
  </html>