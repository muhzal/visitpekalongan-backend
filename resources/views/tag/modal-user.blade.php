<div id="modal-user" class="modal modal-fixed-footer modal-form">
	<div class="modal-header">
		<a href="#!" class="modal-action modal-close right btn-floating red" title="Close"><i class="material-icons">cancel</i></a>
		<h5 style="text-transform:capitalize;">Tambah User Baru</h5>
	</div>
	<div class="modal-content valign-wrapper modal-loader">
		<div class="preloader-wrapper big active">
			<div class="spinner-layer spinner-blue">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-red">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-yellow">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-green">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>
	<form class="col s12" action="user" method="POST" id="form-user" name="form-user" autocomplete="off">
		<div class="modal-content hide">
			<div class="row">
				{!! csrf_field() !!}
				<input type="hidden" name="_method" value="POST" id="_method">
				<div class="row">
					<div class="input-field col s12">
						<input placeholder="Masukan nama lengkap" id="name" type="text" class="validate" name="name" required aria-required="true">
						<label class="active" for="name">Name</label>
					</div>
				</div>
				<div class="row">					
					<div class="input-field col s12">
						<input placeholder="Masukan email aktif" id="email" type="text" class="validate" name="email"  required aria-required="true">
						<label class="active" for="email">Email</label>
					</div>
				</div>
				<div class="row" id="level-row">
					<div class="input-field col s12">
						<!-- <input placeholder="Masukan kata sandi" id="password" type="password" class="validate" name="password"  required aria-required="true"> -->
						<select name="level_id" id="level">
							<option value="2" selected>Admin</option>
							<option value="1">Super Admin</option>
							<option value="3">Member</option>
						</select>
						<label for="level">Level</label>
					</div>
				</div>
				<div class="row password-row">
					<div class="input-field col s12">
						<input placeholder="Masukan kata sandi" id="password" type="password" class="validate" name="password"  required aria-required="true">
						<label class="active" for="password">Password</label>
					</div>
				</div>
				<div class="row password-row">
					<div class="input-field col s12">
						<input placeholder="Masukan ulang kata sandi" id="password_confirmation" type="password" name="password_confirmation" class="validate" required aria-required="true">
						<label class="active" for="password_confirmation">Retype Password</label>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button class="btn-next waves-effect waves-light white-text blue btn" type="submit">Simpan</button>
			<button class=" modal-action modal-close waves-effect waves-light  btn-flat">Batal</button>
		</div>
	</form>
</div>