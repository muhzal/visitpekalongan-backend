<div id="modal-detail" class="modal modal-fixed-footer">
	<div class="modal-header">
		<a href="#!" class="modal-action modal-close right btn-small btn red" title="x"><i class="material-icons">cancel</i></a>
		<h5 style="text-transform:capitalize;">Detail Tempat</h5>
		<div class="col s9">
			<ul class="tabs cyan">
				<li class="tab col s3"><a class="active white-text" href="#tab-info">Info</a></li>
				<li class="tab col s3"><a class="white-text" href="#tab-peta">Peta</a></li>
				<li class="tab col s3"><a class="white-text hide" href="#tab-review">review</a></li>
			</ul>
		</div>
	</div>
	<div class="modal-content valign-wrapper modal-loader hide">
		<div class="preloader-wrapper big active">
			<div class="spinner-layer spinner-blue">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-red">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-yellow">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-green">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="detail-content">
		<div id="tab-info" class="col s12">
			
		</div>
		<div id="tab-peta" class="col s12 modal-content">
			<div id="peta-detail" style="height: calc(100% - 56px);">
				
			</div>
		</div>
		<div id="tab-review" class="col s12 modal-content" style="height: calc(100% - 95px);padding: 0;">
			<ul class="collection">		

			</ul>
		</div>
	</div>
</div>
</div>