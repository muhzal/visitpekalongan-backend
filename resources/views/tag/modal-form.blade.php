
<div id="modal-form" class="modal modal-fixed-footer modal-form">
	<div class="modal-header">
		<a href="#!" class="modal-action modal-close right btn-small btn red" title="x"><i class="material-icons">cancel</i></a>
		<h5 style="text-transform:capitalize;">Tambah Tempat Baru</h5>
	</div>
	<div class="modal-content valign-wrapper modal-loader">
		<div class="preloader-wrapper big active">

	      <div class="spinner-layer spinner-blue">
	        <div class="circle-clipper left">
	          <div class="circle"></div>
	        </div><div class="gap-patch">
	          <div class="circle"></div>
	        </div><div class="circle-clipper right">
	          <div class="circle"></div>
	        </div>
	      </div>
	      <div class="spinner-layer spinner-red">
	        <div class="circle-clipper left">
	          <div class="circle"></div>
	        </div><div class="gap-patch">
	          <div class="circle"></div>
	        </div><div class="circle-clipper right">
	          <div class="circle"></div>
	        </div>
	      </div>

	      <div class="spinner-layer spinner-yellow">
	        <div class="circle-clipper left">
	          <div class="circle"></div>
	        </div><div class="gap-patch">
	          <div class="circle"></div>
	        </div><div class="circle-clipper right">
	          <div class="circle"></div>
	        </div>
	      </div>

	      <div class="spinner-layer spinner-green">
	        <div class="circle-clipper left">
	          <div class="circle"></div>
	        </div><div class="gap-patch">
	          <div class="circle"></div>
	        </div><div class="circle-clipper right">
	          <div class="circle"></div>
	        </div>
	      </div>
		</div>
	</div>
	<div class="modal-map-content  hide">
		<div class="modal-content">
			<div id="map_form" style="height: 100%"></div>
		</div>
	    <div class="modal-footer">
	      <button class="btn-next waves-effect waves-light white-text blue btn disabled">Selanjutnya</button>
	      <button class=" modal-action modal-close waves-effect waves-light  btn-flat">Batal</button>
		</div>
	</div>
	<div class="modal-form-content hide">

	</div>

	  <div class="progress hiddendiv">
	      <div class="indeterminate"></div>
	  </div>
</div>