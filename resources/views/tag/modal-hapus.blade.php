		<div id="modal-hapus" class="modal">
			<div class="modal-content center teal lighten-1 grey-text text-lighten-5">
				<h5>Apakah Anda yakin akan menghapus lokasi ini ?</h5>
			</div>
			<div class="modal-footer">
				<form method="POST" accept-charset="utf-8" id="form-hapus">
					
					{!! method_field('DELETE') !!}
				<div class="col s5 offset-s2">
					<button type="submit" class="btn waves-effect waves-red btn-flat modal-action btn-submit red white-text"><i class="material-icons left">delete_forever</i> hapus</button>
				</div>
				<div class="col s5">
				<a href="#" class="btn waves-effect waves-green btn-flat modal-action modal-close"><i class="material-icons left">cancel</i> batal</a>
				</div>
				</form>
			</div>
		</div>
		
		<script type="text/javascript">
			
		@push('js')
			// $('#modal-hapus form').submit(function(e){

   //    			e.stopPropagation(); 
			// 	e.preventDefault();
			// 	disableButton(true);
			// 	var action 		= $(this).attr('action');
			// 	console.log(action);
			// 	var form_data 	=  $(this).serializeArray();
			// 	$.ajax({
			// 		url: action,
			// 		type: 'POST',
			// 		data: form_data,
			// 	})
			// 	.done(function(data, t, x){
			// 		if (x.status == 200) {						
			// 			$("#modal-hapus").closeModal();
	  //     				Materialize.toast('Selamat data berhasil di hapus', 3000,'green');
	  //     				// btn_submit.removeClass('disabled');
			// 			disableButton(false);
			// 			table[getModel()].ajax.reload();
			// 			setMarkers();
			// 			// window.LaravelDataTables["dataTableBuilder"].ajax.reload();	
			// 		}
			// 	})
			// 	.fail(function(e){
			// 		console.log(e.responseText);
			// 		$(".modal").closeModal();
			// 			disableButton(false);
			// 		Materialize.toast('oops, terdapat kesalahan. data gagal dihapus',3000,'red');
			// 	});
			// });
		@endpush
		</script>