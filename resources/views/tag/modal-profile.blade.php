<div id="modal-profile" class="modal modal-fixed-footer modal-form">
	<div class="modal-header">
		<a href="#!" class="modal-action modal-close right btn-floating red" title="Close"><i class="material-icons">cancel</i></a>
		<h5 style="text-transform:capitalize;">Ubah Profile</h5>
	</div>
	<div class="modal-content valign-wrapper modal-loader">
		<div class="preloader-wrapper big" style="margin:auto">
			<div class="spinner-layer spinner-blue">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-red">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-yellow">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
			<div class="spinner-layer spinner-green">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>
	<form class="col s12" action="user/change" method="POST" id="form-profile" name="form-profile" autocomplete="off">
		<div class="modal-content">
			<div class="row">
				{!! csrf_field() !!}
				<input type="hidden" name="_method" value="POST" id="_method">
				<div class="row">
					<div class="input-field col s12">
						<input placeholder="Masukan nama lengkap" id="name" type="text" class="validate" name="name" required aria-required="true" value="{{ Auth::user()->name }}">
						<label class="active" for="name">Name</label>
					</div>
				</div>
				<div class="row">
					
					<div class="input-field col s12">
						<input placeholder="Masukan email aktif" id="email" type="text" class="validate" name="email"  required aria-required="true" value="{{ Auth::user()->email}}">
						<label class="active" for="email">Email</label>
					</div>
				</div>
				<div class="row password-row">
					<div class="input-field col s12">
						<input placeholder="Masukan kata sandi" id="old_password" type="password" class="validate" name="old_password"  >
						<label class="active" for="old_password">Password Lama</label>
					</div>
				</div>
				<div class="row password-row">
					<div class="input-field col s12">
						<input placeholder="Masukan kata sandi" id="password" type="password" class="validate" name="password"   >
						<label class="active" for="password">Password Baru</label>
					</div>
				</div>
				<div class="row password-row">
					<div class="input-field col s12">
						<input placeholder="Masukan ulang kata sandi" id="password_confirmation" type="password" name="password_confirmation" class="validate"  >
						<label class="active" for="password_confirmation">Retype Password Baru</label>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button class="btn-next waves-effect waves-light white-text blue btn" type="submit">Simpan</button>
			<button class="modal-close waves-effect waves-light  btn-flat">Batal</button>
		</div>
	</form>
</div>

@push('jsplugin')
<script type ="text/javascript" src="{{asset('plugins/validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/profile.js') }}"></script>
@endpush


