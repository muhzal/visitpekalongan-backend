@extends('tempat.show')
@section('detail-tempat')
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">contact_phone</i>
			<span class="title">Telepon</span>
			<p><b>{{ $data->telepon }}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">local_atm</i>
			<span class="title">Kategori</span>
			<p><b>{{ $data->kategori->nama }}</b></p>
		</li>
@endsection