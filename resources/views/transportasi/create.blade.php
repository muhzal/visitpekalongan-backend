
@section('form-tempat')
	 <div class="row">
      <div class="input-field col s12">
      	<select id="kategori" class="validate materialize-select" placeholder="Pilih kategori transportasi" name="kategori_id" required="required">
        <option value="" disabled></option>
      		@foreach ($kategories as $kategori)
      			<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
      		@endforeach
      	</select>
        <label for="kategori">Kategori</label>
      </div>
    </div>
@stop
@include('tempat.form-tambah-tempat', array('action' => url('transportasi')))