
@section('form-tempat')
	 <div class="row">
      <div class="input-field col s12">
        <input type="text" class="validate selectize" name="jenis[]" required="required" value="{{ $data->jenis }}">
        <label for="jenis" class="active">Jenis</label>
      </div>
    </div>
@stop
@include('tempat.form-edit-tempat', array('action'=>route('belanja.update',['id' => $data->id]),'tempat'=>$data->tempat))