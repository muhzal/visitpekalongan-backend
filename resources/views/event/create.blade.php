
@section('form-tempat')

    <div class="row">
      <div class="input-field col s6">
        <input type="text" class=" date tgl-mulai" name="tgl_mulai" required="required">
        <label for="telepon">Tanggal Mulai</label>
      </div>
      <div class="input-field col s6">
        <input type="text" class=" date tgl-selesai" name="tgl_selesai" required="required">
        <label for="telepon">Tanggal selesai</label>
      </div>
    </div>

   <div class="row">
      <div class="input-field col s12 m6 l6">
        <select id="" class=" validate  materialize-select" name="waktu_mulai" required="required">
          @for ($i = 0; $i <= 24; $i++)
            <option value="{{ Carbon\Carbon::now()->startOfDay()->addHours($i)->format('H:i') }}" @if ($i == 7) selected @endif>{{ Carbon\Carbon::now()->startOfDay()->addHours($i)->format('H:i') }}</option>
          @endfor
        </select>
        <label for="bank">Waktu Mulai</label>
      </div>
      <div class="input-field col s12 m6 l6">
        <select id="" class=" validate  materialize-select" name="waktu_selesai" required="required">
            @for ($i = 0; $i <= 24; $i++)
               <option value="{{ Carbon\Carbon::now()->startOfDay()->addHours($i)->format('H:i') }}" @if ($i == 21) selected @endif>{{ Carbon\Carbon::now()->startOfDay()->addHours($i)->format('H:i') }}</option>
            @endfor
        </select>
        <label for="bank">Waktu Selesai</label>
      </div>
   </div>
@stop
@include('tempat.form-tambah-tempat', array('action' => url('event')))