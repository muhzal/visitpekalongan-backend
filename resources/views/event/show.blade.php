@extends('tempat.show')
@section('detail-tempat')

		<li class="collection-item avatar">
			<i class="material-icons circle cyan">date_range</i>
			<span class="title">Tanggal</span>
			<p><b>{{ $data->tgl_mulai->format('d/m/Y').' - '.$data->tgl_selesai->format('d/m/Y') }}</b></p>
		</li>
		<li class="collection-item avatar">
			<i class="material-icons circle cyan">access_time</i>
			<span class="title">Jam</span>
			<p><b>{{ $data->waktu_mulai.' - '.$data->waktu_selesai }}</b></p>
		</li>
@endsection