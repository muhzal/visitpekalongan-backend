
@section('form-tempat')
    <div class="row">
      <div class="input-field col s6">
        <input type="text" class=" date" name="tgl_mulai" required="required" value="{{ $data->tgl_mulai->format('d/m/Y') }}">
        <label for="telepon" class="active">Tanggal Mulai</label>
      </div>
      <div class="input-field col s6">
        <input type="text" class=" date" name="tgl_selesai" required="required" value="{{$data->tgl_selesai->format('d/m/Y') }}">
        <label for="telepon" class="active">Tanggal selesai</label>
      </div>
    </div>

   <div class="row">
      <div class="input-field col s12 m6 l6">
        <select id="selectize" class="validate materialize-select" name="waktu_mulai" required="required">
          @for ($i = 0; $i <= 24; $i++)
            <option value="{{ Carbon\Carbon::now()->startOfDay()->addHours($i)->format('H:i') }}" @if ($i == 7) selected @endif>{{ Carbon\Carbon::now()->startOfDay()->addHours($i)->format('H:i') }}</option>
          @endfor
        </select>
        <label for="bank" class="active">Waktu Mulai</label>
      </div>
      <div class="input-field col s12 m6 l6">
        <select id="selectize" class="validate materialize-select" name="waktu_selesai" required="required">
            @for ($i = 0; $i <= 24; $i++)
               <option value="{{ Carbon\Carbon::now()->startOfDay()->addHours($i)->format('H:i') }}" @if ($i == 21) selected @endif>{{ Carbon\Carbon::now()->startOfDay()->addHours($i)->format('H:i') }}</option>
            @endfor
        </select>
        <label for="bank" class="active">Waktu Selesai</label>
      </div>
   </div>
@stop
@include('tempat.form-edit-tempat', array('action'=>route('event.update',['id' => $data->id]),'tempat'=>$data->tempat))