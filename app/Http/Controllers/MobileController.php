<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use JWTAuth;
use App\Http\Requests;
use Carbon;
use Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\Models\Tempat;
use App\Models\Tiket;
use App\Models\Wisata;
use App\Models\Review;

use Illuminate\Support\Facades\File;
class MobileController extends Controller
{



    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        $validator = Validator::make($credentials, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['email' => ['Data tidak valid, Masukan email dan password dengan benar']],500);
        }        
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
            return response()->json(['email' => ['Email atau password Salah.']],500);
            }
        } catch (JWTException $e) {
            return response()->json(['email' => ['Tidak dapat membuat token']],500);
        }
        User::unguard();
            $user = User::where('email', $request->get('email'))->first();
        User::reguard();
            return response()->json(['token' => $token,'user' => $user]);
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
    }

    public function signup(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'email|required|unique:users',
            'password' => 'confirmed|required|min:5',
            // 'password_confirmation' =>'same:password|required|min:5',
        ]);

        $input = $request->only('email','password','name');
        $input['password'] = Hash::make($input['password']);
        $input['level_id'] = 3;
        User::unguard();
        $user = User::create($input);
        User::reguard();
        if(!$user->id) {
            return response()->json(['error'=>'tidak dapat mendaftarkan'], 500);
        }        
        
        return $this->login($request);        
    }
    public function tempat(Request $request){
        $tempat = Tempat::with('tempatable')->get();
        return response()->json($tempat);
    }
    public function cekUpdate(Request $request){
        $user_id = $request->get('user_id');
        $last   = $request->has('data_latest') ? $request->get('data_latest') : '2015-01-01 00:00:00';
        $new    = Tempat::with(['tempatable'])->where('updated_at','>',$last)->orWhere([['user_id',$user_id],['updated_at','>',$last]])->orderBy('updated_at','desc')->get(); 
        
        if (count($new) > 0) {            
            return response()->json(['tempat'=>$new]);
        }
        return response()->json(null);
    }

    public function rate(Request $request)
    {      
        $wisata_id= $request->get('wisata_id');
        $review = $this->cekUserReview($request);
        $wisata = Wisata::find($wisata_id);
        if ($wisata) {         
            return response()->json(['rate'=>$wisata->rate,'review'=>$review]);   
        }
            return response()->json(0);   
    }

    public function reviews(Request $request)
    {      
        $reviews = Review::with('user')->where('wisata_id',$request->get('wisata_id'))->orderBy('updated_at','desc')->get();
        return response()->json($reviews);
    }
    public function cekUserReview(Request $request)
    {
        $user_id = $request->get('user_id');
        $wisata_id= $request->get('wisata_id');
        $review = Review::where([['wisata_id',$wisata_id],['user_id',$user_id]])->first();

        if ($review) {
            return $review;
        }
        return 0;
    }
    public function tambahreview(Request $request)
    {
        $this->validate($request, [
            'title' => 'max:30',
            'rating' => 'required|numeric',
            'review' => 'required|max:255',
        ]);        
        $newReview = $request->only('judul','review','rating','wisata_id','user_id');
        $review = Review::find($request->get('id'));
        if ($review) {
            $review->update($newReview);
            $review->load('wisata');
            return response()->json($review);   
        }
        $review = Review::create($newReview);
        if ($review) {                    
            $review->load('wisata');
            return response()->json($review);   
        }

        return response()->json(['error'=>'gagal menyimpan review'], 500);
    }

    public function hapusreview(Request $request)
    {   
        $id = $request->get('id');
        $review = Review::find($id);
        if ($review) {
            $wisata = $review->wisata;
            $review->delete();
            return response()->json($wisata,200);
        }
        return response()->json('failed',404);
    }
    public function cekToken(Request $request){
        return true;
    }


    public function changepassword(Request $request) {
        $this->validate($request,[
            'name' => 'required|max:100',
            'email' => 'email|required',            
            'password' => 'required',
        ]);
        $user = User::find($request->get('user_id'));
        $checkEmail = User::where('email',$request->email)->first();
        if ($checkEmail && $checkEmail->id !== $user->id) {
            return response()->json(['email'=>['Email sudah digunakan']],422);
        }
        if (!Hash::check($request->password, $user->password)) {
            return response()->json(['password'=>['Password lama salah']],422);
        }
        // if ($old && $new) {      
        if ($request->new_password) {    
            $this->validate($request,[
                'new_password' => 'required|min:6|confirmed',
            ]); 
            $user->password = bcrypt($request->new_password);        
        }

        $user->fill($request->only('email','name'));        
        if ($user->saveOrFail()) {
            return response()->json($user,200);
        }
        return response()->json('gagal',500);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {         
        $jenis = 'App\Models\\'.ucfirst(strtolower($request->get('_model')));
        $tempat = new Tempat($request->get('tempat'));              
        $model = $tempat->tempatable()->createModelByType($jenis)->fill($request->except('tempat'));
 
        if ($model->save() && $tempat->tempatable()->associate($model)->save() && $this->saveGambar($request,$tempat)) {   

           if ($request->has('menus')) {
                $menus = $request->get('menus');
                $arrayMenus = explode(',',$menus);
                for ($i=0; $i < count($arrayMenus); $i++) {                 
                    $model->menus()->create(['nama'=>$arrayMenus[$i]]);
                }   
            }       
            if ($request->has('tikets')) {
                $tikets = collect($request->get('tikets'))->values()->toArray();
                $model->tikets()->createMany($tikets);
            }       
            if ($request->has('fasilitas') && $jenis =='wisata') {
                $arrayfasilitas = explode(',',$request->get('fasilitas'));
                $fasilitas = collect($arrayfasilitas)->transform(function ($item) {return ['nama' => $item];})->toArray();
                $model->fasilitas()->createMany($fasilitas);
            }
            return response()->json($tempat->fresh()->load('gambars','tempatable'));
        }

        return response($tempat);
         
    }

    protected function saveGambar(Request $request,$tempat){
        $images = $request->get('gambar');
        $model = ucfirst(strtolower($request->get('_model')));
        try{        
            for ($i=0; $i <count($images); $i++) {             
                 $file = base64_decode($images[$i]);
                 $name = date('Ymds').$i.'.jpeg';
                 $path = public_path('images').'\\'.$model.'\\'.$name;
                 file_put_contents($path, $file);  
                 $tempat->gambars()->create(['nama' => $name]);
             }     
             return true;
        }catch(Exception $e){
            return response($e->getMessage());
            // return false;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
