<?php

namespace App\Http\Controllers;

use App\Models\Tempat;
use App\Models\Review;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function home(){
        return view('home.guest');
    }

    public function download(){
        try{
            return response()->download(public_path('app/kalongcation.apk'));   
        }catch(Exception $e){
            abort(404);
        }
    }

    public function index()
    {        
        $tempats = Tempat::orderBy('created_at', 'desc')->take(5)->get();
        $reviews = Review::orderBy('updated_at', 'desc')->take(5)->get();
        return view('home.index',compact('tempats','reviews'));
    }
    public function bantuan(){
        return view('home.bantuan');
    }
}
