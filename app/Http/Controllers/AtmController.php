<?php

namespace App\Http\Controllers;

use App\Models\Atm;
use App\Models\Bank;
use App\Models\Gambar;
use App\Models\Tempat;
use Auth;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AtmController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}
	public function index() {
		//
	}

	public function create() {
		$banks = Bank::all();
		return view('atm.create', ['banks' => $banks]);
	}

	public function store(Request $request) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:2000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$tempat = $request->get('tempat');
		$tempat['user_id'] = Auth::user()->id;
		// $bank               = collect($request->get('banks'))->transform(function($item){return ['nama'=>$item];})->toArray();
		$bank = $request->get('banks');
		$images = $request->file('images');
		$new_atm = Atm::create();
		if ($new_atm->tempat()->create($tempat) && $new_atm->banks()->sync($bank) && Gambar::imageStore($images, $new_atm->tempat)) {
			return response()->json('sukses', 200);
		} else {
			return response()->json('gagal', 500);
		}
	}

	public function show($id) {
		
		$data = Atm::findOrFail($id);
		$data->load('tempat','tempat.user','banks');
		// return response()->json($data);
		return view('atm.show',compact('data'));
	}

	public function edit($id) {
		$data = Atm::find($id);
		if (isset($data)) {
			$banks = Bank::all();
			return view('atm/edit', ['data' => $data, 'banks' => $banks]);
		}
		abort(404);
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:1000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$imagesName = $request->get('imagesName');
		$images = $request->file('images');
		$tempat = $request->get('tempat');
		$tempat['publish'] =  array_key_exists('publish', $tempat ) ? $tempat['publish'] : 0;				
		$banks = $request->get('banks');
		$old = Atm::find($id);

		$old->tempat->updateImages($imagesName);

		if (isset($images[0])) {
			Gambar::imageStore($images, $old->tempat);
		}
		if ($old->tempat()->update($tempat) && $old->banks()->sync($banks)) {

			return response('sukses');
		}
	}

	public function destroy($id) {
		$atm = Atm::find($id);

		if ($atm->tempat->delete() && $atm->delete()) {
			return response()->json('sukses', 200);
		} else {
			return response()->json('gagal', 500);
		}
	}

	public function json() {
		$data = Atm::with('tempat.user', 'banks')->get();
		return Datatables::of($data)->make(true);
	}

}
