<?php

namespace App\Http\Controllers;
use App\Models\Gambar;
use App\Models\Menu;
use App\Models\Restoran;
use App\Models\Tempat;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class RestoranController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$menus = Menu::select('nama')->distinct()->get();

		return view('restoran.create', ['menus' => $menus]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:2000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$restoran = new Restoran($request->only(['telepon', 'harga_max', 'harga_min', 'waktu_buka', 'waktu_tutup']));
		$images = $request->file('images');
		$tempat = $request->get('tempat');
		$menus = collect($request->get('menus'))->transform(function ($item) {return ['nama' => $item];})->toArray();
		$tempat = new Tempat($tempat);

		if ($restoran->save() && $restoran->tempat()->save($tempat) && $restoran->menus()->createMany($menus) && Gambar::imageStore($images, $tempat)) {

			return response($restoran);
		}

		return response()->json(['status' => '500', 'responseText' => 'error']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$data = Restoran::findOrFail($id);
		$data->load('tempat','tempat.user','menus');
		// return response()->json($data);
		return view('restoran.show',compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$data = Restoran::find($id);
		if (!$data) {
			abort(404);
		}
		$menus = Menu::select('nama')->distinct()->get();

		return view('restoran.edit', ['data' => $data, 'menus' => $menus]);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:1000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$imagesName = $request->get('imagesName');
		$images = $request->file('images');
		$old = Restoran::find($id);
		// $menus      = collect($request->get('menus'))->transform(function($item){return ['nama'=>$item];})->toArray();
		$menus = $request->get('menus');
		$tempat = $request->get('tempat');
		$tempat['publish'] =  array_key_exists('publish', $tempat ) ? $tempat['publish'] : 0;		
		$baru = $request->only(['telepon', 'harga_max', 'harga_min', 'waktu_tutup', 'waktu_buka']);

		//Delete Menu

		$menuBaru = collect(array_diff($menus, $old->menus()->pluck('nama')->toArray()))->transform(function ($item) {return ['nama' => $item];})->toArray();
		$deleteMenu = $old->menus()->whereNotIn('nama', $menus);

		if ($old->update($baru) && $old->tempat()->update($tempat)) {
			if (isset($menuBaru)) {
				$old->menus()->createMany($menuBaru);
			}
			if (isset($deleteMenu)) {
				$deleteMenu->delete();
			}

			$old->tempat->updateImages($imagesName);

			if ($images[0] != null) {
				Gambar::imageStore($images, $old->tempat);
			}

			return response()->json($old, 200);
		} else {
			return response()->json($old, 500);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$restoran = Restoran::find($id);
		if ($restoran->tempat->delete() && $restoran->delete()) {

			return response()->json('sukses');
		}
		return response()->json('gagal',404);
	}

	public function json(Request $request) {

		// $data = Restoran::with('tempat')->get();
		$tanda = $request->get('usulan') ? '=' : '!=';
		$data = Restoran::join('tempats','restorans.id','=','tempats.tempatable_id')
			->join('users','tempats.user_id','=','users.id')
			->where('users.level_id', $tanda, 3)
			->select('restorans.*')
			->groupBy('restorans.id')
			->with('tempat.user')
			->get();

		return Datatables::of($data)->make(true);
	}
}
