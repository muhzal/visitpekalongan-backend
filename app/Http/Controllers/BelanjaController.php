<?php

namespace App\Http\Controllers;

use App\Models\Belanja;
use App\Models\Tempat;
use App\Models\Gambar;
use Datatables;
use Illuminate\Http\Request;

class BelanjaController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}
	public function json(Request $request) {		
		
		$tanda = $request->get('usulan') ? '=' : '!=';
		$data = Belanja::join('tempats','belanjas.id','=','tempats.tempatable_id')
			->join('users','tempats.user_id','=','users.id')
			->where('users.level_id', $tanda, 3)
			->select('belanjas.*')
			->groupBy('belanjas.id')
			->with('tempat.user')
			->get();
		return Datatables::of($data)->make(true);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('belanja.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:2000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);

		$jenis = $request->get('jenis');

		// return response()->json($request->all(), 500);
		$jenis['jenis'] = implode(', ', $jenis);

		$belanja = new Belanja($jenis);
		$images = $request->file('images');
		$tempat = $request->get('tempat');
		// return response()->json($images[0]->getClientOriginalExtension(),500);

		if ($belanja->save() && $belanja->tempat()->create($tempat) && Gambar::imageStore($images, $belanja->tempat)) {
			return response()->json($belanja, 200);
			// return $tempat;
		}

		return response()->json("error", 500);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
				
		$data = Belanja::findOrFail($id);
		$data->load('tempat','tempat.user');
		// return response()->json($data);
		return view('belanja.show',compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$data = Belanja::with('tempat')->find($id);
		if ($data) {
			return view('belanja.edit', ['data' => $data]);
		} else {
			abort(404);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:1000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);

		$imagesName = $request->get('imagesName');
		$images = $request->file('images');

		$old = Belanja::find($id);
		$tempat = $request->get('tempat');
		$tempat['publish'] =  array_key_exists('publish', $tempat ) ? $tempat['publish'] : 0;		
		$jenis = $request->get('jenis');
		$jenis['jenis'] = implode(', ', $jenis);

		if ($old->update($jenis) && $old->tempat()->update($tempat)) {

			$old->tempat->updateImages($imagesName);

			if (isset($images[0])) {
				Gambar::imageStore($images, $old->tempat);
			}

			return response()->json($old, 200);
		}

		return response()->json('gagal', 500);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
    public function destroy($id)
    {
        $data = Belanja::find($id);
        if ($data->tempat->delete() && $data->delete()) {  
                return response()->json('sukses',200);  
        }
        return response()->json('gagal',500);
    } 
}
