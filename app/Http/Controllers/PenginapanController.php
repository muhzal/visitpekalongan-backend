<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Gambar;
use App\Models\Penginapan;
use App\Models\Tempat;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PenginapanController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('penginapan.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:2000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$penginapan = new Penginapan($request->only(['telepon', 'harga_max', 'harga_min', 'fasilitas']));
		$images = $request->file('images');
		$tempat = $request->get('tempat');
		// return response($request->all());
		$tempat = new Tempat($tempat);

		if ($penginapan->save() && $penginapan->tempat()->save($tempat) && Gambar::imageStore($images, $tempat)) {

			return response($penginapan);
			// return $tempat;
		}

		return response()->json(['status' => '500', 'responseText' => 'error']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
				
		$data = Penginapan::findOrFail($id);
		$data->load('tempat','tempat.user');
		// return response()->json($data);
		return view('penginapan.show',compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$data = Penginapan::find($id);
		if (isset($data)) {
			return view('penginapan.edit', ['data' => $data]);
		}
		return abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:100000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$imagesName = $request->get('imagesName');
		$images = $request->file('images');

		$old = Penginapan::find($id);
		$tempat = $request->get('tempat');
		$tempat['publish'] =  array_key_exists('publish', $tempat ) ? $tempat['publish'] : 0;		
		$baru = $request->only(['telepon', 'harga_max', 'harga_min', 'fasilitas']);

		$old->tempat->updateImages($imagesName);

		if (isset($images[0])) {
			Gambar::imageStore($images, $old->tempat);
		}
		if ($old->update($baru) && $old->tempat()->update($tempat)) {

			return response(url('spbu'));
			// return response()->json(url('spbu');
			// return redirect()->route('spbu.index');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$penginapan = Penginapan::find($id);
		if ($penginapan->tempat->delete() && $penginapan->delete()) {

			return response()->json('sukses');
		}
		return response()->json('gagal', 500);
	}
	public function json(Request $request) {
		// $data = Penginapan::with('tempat')->get();
		
		$tanda = $request->get('usulan') ? '=' : '!=';
		$data = Penginapan::join('tempats','penginapans.id','=','tempats.tempatable_id')
			->join('users','tempats.user_id','=','users.id')
			->where('users.level_id', $tanda, 3)
			->select('penginapans.*')
			->groupBy('penginapans.id')
			->with('tempat.user')
			->get();
		return Datatables::of($data)->make(true);
	}
}
