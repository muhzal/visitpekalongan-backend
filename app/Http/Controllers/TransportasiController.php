<?php

namespace App\Http\Controllers;

use App\Models\Gambar;
use App\Models\Kategori;
use App\Models\Transportasi;
use Auth;
use Datatables;
use Illuminate\Http\Request;

// use App\Http\Requests;

class TransportasiController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	public function json() {
		$data = Transportasi::with('tempat.user')->get();
		return Datatables::of($data)->make(true);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$data = Kategori::all();
		return view('transportasi.create', ['kategories' => $data]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:2000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$tempat = $request->get('tempat');
		$tempat['user_id'] = Auth::user()->id;
		$images = $request->file('images');
		$new = new Transportasi($request->all());

		if ($new->save() && $new->tempat()->create($tempat) && Gambar::imageStore($images, $new->tempat)) {
			return response()->json('sukses', 200);
		} else {
			return response()->json('gagal', 500);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
				
		$data = Transportasi::findOrFail($id);
		$data->load('tempat','tempat.user','kategori');
		// return response()->json($data);
		return view('transportasi.show',compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$transportasi = Transportasi::find($id);
		$data = Kategori::all();

		if ($transportasi) {
			return view('transportasi.edit', ['kategories' => $data, 'data' => $transportasi]);
		}
		abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:1000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$imagesName = $request->get('imagesName');
		$images = $request->file('images');
		$tempat = $request->get('tempat');
		$tempat['publish'] =  array_key_exists('publish', $tempat ) ? $tempat['publish'] : 0;		
		$old = Transportasi::find($id);
		if ($old->update($request->only('kategori_id', 'telepon')) && $old->tempat()->update($tempat)) {
			$old->tempat->updateImages($imagesName);
			if (isset($images[0])) {
				Gambar::imageStore($images, $old->tempat);
			}
			return response('sukses');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$transportasi = Transportasi::find($id);
		if ($transportasi->tempat->delete() && $transportasi->delete()) {

			return response()->json('sukses', 200);
		}
		return response()->json('gagal', 500);
	}
}
