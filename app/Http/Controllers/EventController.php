<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Event as EventWisata;
use App\Models\Gambar;
use App\Models\Tempat;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class EventController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}
	public function edit($id) {
		$data = EventWisata::find($id);
		if (!$data) {
			abort(404);
		}

		return view('event.edit', ['data' => $data]);

	}
	public function update(Request $request, $id) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:1000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$imagesName = $request->get('imagesName');
		$images = $request->file('images');
		$old = EventWisata::find($id);
		$tempat = $request->get('tempat');
		$tempat['publish'] =  array_key_exists('publish', $tempat ) ? $tempat['publish'] : 0;		
		$baru = $request->only(['tgl_mulai', 'tgl_selesai', 'waktu_mulai', 'waktu_selesai']);
		$old->tempat->updateImages($imagesName);
		if (isset($images[0])) {
			Gambar::imageStore($images, $old->tempat);
		}
		if ($old->update($baru) && $old->tempat()->update($tempat)) {

			return response()->json($old,200);
		} else {
			return response('gagal', 500);
		}
	}
	public function create() {
		$bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		return view('event.create', compact('bulan'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:1000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$event = new EventWisata($request->only(['tgl_mulai', 'tgl_selesai', 'waktu_mulai', 'waktu_selesai']));
		$images = $request->file('images');
		$tempat = $request->get('tempat');
		$tempat = new Tempat($tempat);

		if ($event->save() && $event->tempat()->save($tempat)) {
			Gambar::imageStore($images, $tempat);
			return response($event);
		}

		return response()->json("error", 500);
	}
    public function destroy($id)
    {
        $data = EventWisata::find($id);
        if ($data->tempat->delete() && $data->delete()) {  
                return response()->json('sukses',200);  
        }
        return response()->json('gagal',500);
    } 
	public function json() {
		$event = EventWisata::with('tempat.user')->get();		
		return Datatables::of($event)->make(true);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
				
		$data = EventWisata::findOrFail($id);
		$data->load('tempat','tempat.user');
		// return response()->json($data);
		return view('event.show',compact('data'));
	}
}
