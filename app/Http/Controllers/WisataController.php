<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Fasilitaswisata;
use App\Models\Gambar;
use App\Models\Jeniswisata;
use App\Models\Tempat;
use App\Models\Tiket;
use App\Models\Wisata;
use App\Models\Review;
use Auth;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
class WisataController extends Controller {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		$this->middleware('auth');
	}
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index() {
		//
	}
	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create() {
		$tikets = Tiket::select('nama', 'id', 'operasional')->distinct()->get();
		$fasilitas = Fasilitaswisata::select('nama')->distinct()->get();
		$jeniswisata = Jeniswisata::all();
		return view('wisata.create', compact('operasionals', 'tikets', 'jeniswisata', 'fasilitas'));
	}
	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:2000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$wisata = new Wisata($request->only('telepon', 'pengelola', 'waktu_buka', 'waktu_tutup', 'is_tiket', 'jeniswisata_id'));
		$tikets = collect($request->get('tiket'))->values()->toArray();
		// return response($tikets);
		$fasilitas = collect($request->get('fasilitas'))->transform(function ($item) {return ['nama' => $item];})->toArray();
		$images = $request->file('images');
		$tempat = $request->get('tempat');
		$tempat = new Tempat($tempat);
		$tempat->user_id = Auth::user()->id;
		if ($wisata->save() && $wisata->tempat()->save($tempat) && Gambar::imageStore($images, $tempat)) {
			if (isset($fasilitas)) {
				$wisata->fasilitas()->createMany($fasilitas);
			}
			if (isset($tikets) && $wisata->is_tiket == 0) {
				$wisata->tikets()->createMany($tikets);
			}
			return response()->json($wisata, 200);
			// return $tempat;
		}
		return response()->json($wisata, 500);
	}
	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function show2(Request $request, $id) {
		$data = Wisata::with('tempat','tempat.gambars','tikets','jeniswisata')->find($id);
		$reviews = $data->reviews()->paginate(5);
		$wisata = Wisata::with('tempat')->select('id')->get();
		if ($request->ajax()) {		
			return view('wisata.review',['reviews'=>$reviews]);
		}
		return view('wisata.show2',['data'=>$data,'reviews'=>$reviews,'wisata'=>$wisata]);
	}
	public function show($id) {				
		$data = Wisata::findOrFail($id);
		$data->load('tempat','tempat.user','jeniswisata','tikets');
		// return response()->json($data);
		return view('wisata.show',compact('data'));
	}
	// public function review($id) {
							// 	$data = Review::where('wisata_id',$id)->paginate(2);
					// 	return response()->json($data);
	// }
	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function edit($id) {
		$data = Wisata::find($id);
		if (!$data) {
			abort(404);
		}
		$fasilitas = Fasilitaswisata::select('nama')->distinct()->get();
		$tikets = Tiket::select('nama', 'id', 'operasional')->distinct()->get();
		$jeniswisata = Jeniswisata::all();
		return view('wisata.edit', compact('tikets', 'jeniswisata', 'data', 'fasilitas'));
	}
	/**
	* Update the specified resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function update(Request $request, $id) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:1000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$imagesName = $request->get('imagesName');
		$images = $request->file('images');
		$old = Wisata::find($id);
		$fasilitas = $request->get('fasilitas');
		$tikets = $request->get('tiket');
		$tempat = $request->get('tempat');
		$tempat['publish'] =  array_key_exists('publish', $tempat ) ? $tempat['publish'] : 0;		
		$baru = $request->only('telepon', 'pengelola', 'waktu_buka', 'waktu_tutup', 'is_tiket', 'jeniswisata_id');
		// return response()->json($tikets, 500);
		// return response($delete);
		if ($old->update($baru) && $old->tempat()->update($tempat)) {
			if ($old->is_tiket == 0) {
				$old->tikets()->delete();
			} else {
				$old->updateTikets($tikets);
			}
			$old->updateFasilitas($fasilitas);
			$old->tempat->updateImages($imagesName);
			if (isset($images[0])) {
				Gambar::imageStore($images, $old->tempat);
			}
			return response($old);
		}
		return response($old);
	}
	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function destroy($id) {
		$wisata = Wisata::find($id);
		if ($wisata->tempat->delete() && $wisata->delete()) {
			return response()->json('sukses');
		}
		return response()->json('gagal');
	}
	// public function json() {
		// $data = Wisata::with('tempat', 'jeniswisata', 'tikets', 'fasilitas')->get();
	public function json(Request $request) {

		// $data = Restoran::with('tempat')->get();
		$tanda = $request->get('usulan') ? '=' : '!=';
		$data = Wisata::join('tempats','wisatas.id','=','tempats.tempatable_id')
			->join('users','tempats.user_id','=','users.id')
			->where('users.level_id', $tanda, 3)
			->select('wisatas.*')
			->groupBy('wisatas.id')
			->with('tempat.user', 'jeniswisata', 'tikets', 'fasilitas')
			->get();
		return Datatables::of($data)->make(true);
	}
	public function jsonOpTiket() {
		$tiket = Tiket::select('nama', 'id', 'operasional')->distinct()->get();
		$tiket = Tiket::all();
		return response()->json($tiket);
	}
	public function reviews(Request $request){
		$data = Review::with('user','wisata')->where('wisata_id',$request->id)->get();
		return view('wisata.review',['reviews' => $data]);
	}
	public function deleteReview($id){
		$review = Review::findOrFail($id);
		if ($review && $review->delete()) {
			return response()->json(true,200);
		}
		abort(404);
	}
}