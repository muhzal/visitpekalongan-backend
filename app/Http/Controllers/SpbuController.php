<?php

namespace App\Http\Controllers;
use App\DataTables\SpbuDataTable;
use App\Http\Controllers\Controller;
use App\Models\Gambar;
use App\Models\Spbu;
use App\Models\Tempat;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SpbuController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(SpbuDataTable $datatable) {
		return $datatable->render('spbu/index');
		// return view('spbu/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('spbu.create');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */

	public function store(Request $request) {
		// DB::beginTransaction();
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:2000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$spbu = new Spbu($request->except('tempat'));
		$images = $request->file('images');
		$tempat = $request->get('tempat');
		// return response($request->all());
		$tempat = new Tempat($tempat);

		if ($spbu->save() && $spbu->tempat()->save($tempat) && Gambar::imageStore($images, $tempat)) {

			return response($spbu);
			// return $tempat;
		}

		return response()->json(['status' => '500', 'responseText' => 'error']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$data = Spbu::findOrFail($id);
		$data->load('tempat','tempat.user');
		// return response()->json($data);
		return view('spbu.show',compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$data = Spbu::find($id);
		if (isset($data)) {
			return view('spbu.edit', ['data' => $data]);
		}
		return abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'tempat.nama' => 'required|max:100',
			'tempat.latitude' => 'required|numeric',
			'tempat.longitude' => 'required|numeric',
			'images.*' => 'mimes:jpeg,bmp,png|max:1000',
			'tempat.alamat' => 'string',
			'tempat.keterangan' => 'string',
			'tempat.publish' => 'boolean',
		]);
		$imagesName = $request->get('imagesName');
		$images = $request->file('images');

		$old = Spbu::find($id);
		$tempat = $request->get('tempat');
		$tempat['publish'] =  array_key_exists('publish', $tempat ) ? $tempat['publish'] : 0;		
		$fasilitas = $request->except('tempat');

		if ($old->update($fasilitas) && $old->tempat()->update($tempat)) {
			$old->tempat->updateImages($imagesName);

			if (isset($images[0])) {
				Gambar::imageStore($images, $old->tempat);
			}

			return response($old);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$spbu = Spbu::findOrFail($id);
		if (isset($spbu)) {
			$spbu->tempat->delete();
			$spbu->delete();

			return response()->json("deleted");
		}


	}

	public function json() {

		$spbu = Spbu::with('tempat.user')->get();
		return Datatables::of($spbu)->make(true);
	}
}
