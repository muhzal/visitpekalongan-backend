<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Regencie;
use App\Models\Tempat;
use Illuminate\Http\Request;

class TempatController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct() {
		$this->middleware('auth');
	}
	public function index() {
		return view('tempat.index');
	}
	public function usulan() {
		return view('tempat.usulan');
	}
	public function json() {
		$tempat = Tempat::all();
		return response()->json($tempat);
	}
	public function alamat() {
		$alamat = Regencie::all();
		return response()->json($alamat->flatten());
	}
	public function terbitkan(Request $r){
		$tempat = Tempat::findOrFail($r->get('id'));
		$tempat->publish = $r->get('status');	
		$tempat->save();
		return response()->json($tempat,200);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request) {
		$form = $request->get('form');
		return view('tempat.create', ['form' => $form]);
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
	public function listtempat() {
		$tempats = Tempat::with('user')->orderBy('updated_at', 'desc')->simplaPaginate(5);
		return view('tempat.listtempat', ['tempats' => $tempats]);
	}
}