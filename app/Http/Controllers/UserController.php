<?php

namespace App\Http\Controllers;

use App\Models\Tempat;
use App\User;
use Datatables;
use Illuminate\Http\Request;
use Auth;
use Hash;
class UserController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct() {
		//$this->middleware(['auth','superadmin']);
	}
	public function index() {
		$tempat = Tempat::with(['user' => function($query){
			$query->where('level_id',3);
		}])->orderBy('created_at', 'desc')->paginate(5);
		return view('user.user',['tempats'=>$tempat]);
	}
	public function jsonAdmin() {
			$data = User::where([
				['level_id','!=',3],
				['active', 1]
				])->get();
			return Datatables::of($data)->make(true);
	}
	public function jsonUser() {
			$data = User::where([
				['level_id', 3],
				['active', 1]
				])->get();
			return Datatables::of($data)->make(true);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request,[
			'name' => 'required|max:100',
			'email' => 'email|required|unique:users',
			'password' => 'confirmed|required|min:5',
			'password_confirmation'	=>'same:password|required|min:5',
			'level_id' => 'required|exists:levels,id',
			]);
		$data = $request->only('name','email','level_id');
		// $data['level_id'] = 2;
		$data['password'] = bcrypt($request->get('password'));
		$admin = User::create($data);
		if ($admin) {
			return response()->json($admin,200);
		}
		else return response()->json('error',500);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$data = User::find($id);
		if (isset($data)) {
			return response()->json($data,200);
		}

			return response()->json('gagal',404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request,[
			'name' => 'required|max:100',
			'email' => 'email|required',
			]);

		$user = User::findOrFail($id);
		$checkEmail = User::where('email',$request->email)->first();
		if ($checkEmail && $checkEmail->id !== $user->id) {
			return response()->json(['email'=>['Email sudah digunakan']],422);
		}
		
		$user->fill($request->only('email','name','level_id'));
		if ($user->saveOrFail()) {
			return response()->json($user,200);
		}
		return response()->json('gagal',500);
		
	}
	public function change(Request $request) {
		$this->validate($request,[
			'name' => 'required|max:100',
			'email' => 'email|required',
		]);

		$user = Auth::user();

		$checkEmail = User::where('email',$request->email)->first();
		if ($checkEmail && $checkEmail->id !== $user->id) {
			return response()->json(['email'=>['Email sudah digunakan']],422);
		}
		// if ($old && $new) {		
		if (array_key_exists('old_password', $request)) {	
			$this->validate($request,[
				'password' => 'required|min:6|confirmed',
			]);	
			
			if (!Hash::check($request->old_password, $user->password)) {
                return response()->json(['old_password'=>['Password lama tidak cocok']],422);
            } else {
                $user->password = bcrypt($request->password);
            }	
		}

		$user->fill($request->only('email','name'));		
		if ($user->saveOrFail()) {
			return response()->json($user,200);
		}

		return response()->json('gagal',500);
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$user = User::find($id);
		if ($user) {
			$user->active = false;
			if ($user->save()) {
				return response()->json($user,200);
			}
			return response()->json('gagal',500);
		}
		return response()->json('notfound',404);
	}
}
