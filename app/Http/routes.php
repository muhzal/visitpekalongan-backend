<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
 */

// Route::group(['middleware' => ['web']], function () {
//     //

// 	Route::get('/', function () {
// 	    return view('home.index');
// 	});

// });
 //Route::any('x', function (Illuminate\Http\Request $request) {
  		// $request;
   //          $jenis = "App\Models\penginapan";
     //        $model = new $jenis;
     //return response()->json($model->fill(['telepon'=>'2222']));
// });
// Route::any('y',['middleware'=>"auth", function (Illuminate\Http\Request $request) {
//     return view('layouts.map-layout');
// }]);

Route::group(['middleware' => ['web']], function () {
	Route::get('/download',['uses' => 'HomeController@download']);
	Route::get('/home', ['uses' => 'HomeController@home']);
	Route::auth();
	Route::get('/', ['middleware' => "auth", 'uses' => 'HomeController@index']);

	Route::get('json/spbu', ['middleware' => "auth", 'uses' => 'SpbuController@json']);
	Route::get('json/atm', ['middleware' => "auth", 'uses' => 'AtmController@json']);
	Route::get('json/penginapan', ['middleware' => "auth", 'uses' => 'PenginapanController@json']);
	Route::get('json/restoran', ['middleware' => "auth", 'uses' => 'RestoranController@json']);
	Route::get('json/wisata', ['middleware' => "auth", 'uses' => 'WisataController@json']);
	Route::get('json/optiket', ['middleware' => "auth", 'uses' => 'WisataController@jsonOpTiket']);
	Route::get('json/event', ['middleware' => "auth", 'uses' => 'EventController@json']);
	Route::get('json/tempat', ['middleware' => "auth", 'uses' => 'TempatController@json']);
	Route::get('json/alamat', ['middleware' => "auth", 'uses' => 'TempatController@alamat']);
	Route::get('json/transportasi', ['middleware' => "auth", 'uses' => 'TransportasiController@json']);
	Route::get('json/belanja', ['middleware' => "auth", 'uses' => 'BelanjaController@json']);
	Route::get('json/user', ['middleware' => "auth", 'uses' => 'UserController@jsonUser']);
	Route::get('json/admin', ['middleware' => "auth", 'uses' => 'UserController@jsonAdmin']);
	Route::get('admin', ['middleware' => "auth", 'uses' => 'UserController@admin']);
	// Route::get('/home', ['middleware' => "auth", 'uses' => 'HomeController@index']);
	Route::get('/listtempat', ['middleware' => "auth", 'uses' => 'TempatController@listtempat']);
	Route::get('/json/review/{id}', ['middleware' => "auth", 'uses' => 'WisataController@review']);
	Route::get('/usulan', ['middleware' => "auth", 'uses' => 'TempatController@usulan']);
	Route::get('/bantuan', ['middleware' => "auth", 'uses' => 'HomeController@bantuan']);
	Route::post('/terbitkan', ['middleware' => "auth", 'uses' => 'TempatController@terbitkan']);
	Route::post('/user/change', ['middleware' => "auth", 'uses' => 'UserController@change']);
	Route::post('/wisata/reviews', ['middleware' => "auth", 'uses' => 'WisataController@reviews']);
	Route::delete('/wisata/{id}/deleteReview', ['middleware' => "auth", 'uses' => 'WisataController@deleteReview']);
	// Route::resource('spbu', 'SpbuController');
	// Route::resource('tempat', 'TempatController');		
	Route::resources([
		'atm' => 'AtmController',
		'gambar' => 'GambarController',
		'penginapan' => 'PenginapanController',
		'tempat' => 'TempatController',
		'spbu' => 'SpbuController',
		'restoran' => 'RestoranController',
		'wisata' => 'WisataController',
		'event' => 'EventController',
		'transportasi' => 'TransportasiController',
		'belanja' => 'BelanjaController',
		'user' => 'UserController',
	]);
});



Route::group(['middleware' => ['web', 'cors'], 'prefix' => 'mobile'], function () {
	Route::post('login', 'MobileController@login');
	Route::post('signup', 'MobileController@signup');
	Route::post('rate', 'MobileController@rate');	
	Route::post('reset', 'Auth\\PasswordController@sendResetMemberEmail');
	// });

	Route::group(['middleware' => ['jwt.auth', 'jwt.refresh']], function () {
		Route::get('tempat', 'MobileController@tempat');
		Route::post('reviews', 'MobileController@reviews');
		Route::post('cekUpdate', 'MobileController@cekUpdate');
		Route::post('cektoken', 'MobileController@cekToken');
		Route::post('cekuserreview', 'MobileController@cekUserReview');
		Route::post('tambahreview','MobileController@tambahreview');
		Route::post('hapusreview','MobileController@hapusreview');	
		Route::post('tambahTempat','MobileController@store');
		Route::post('ubahpassword','MobileController@changepassword');


		// Route::post('x', function (Illuminate\Http\Request $request) {
		//  $images = $request->get('gambar');
		//  $data = $request->except('gambar');
  //        $jenis = $request->get('_model');
		//  $tempat = new App\Models\Tempat($request->tempat);		 
		//  for ($i=0; $i <count($images); $i++) { 		 	
  //            $file = base64_decode($images[$i]);
  //            $name = Carbon\Carbon::now()->timestamp;
  //            file_put_contents(public_path('images\Tempat').'\\'.$name.$i.'.jpeg', $file);  
		//  }
  //        $model = $tempat->tempatable()->createModelByType('App\models\\'.$jenis)->fill($request->except('tempat'));
	 //     return response($model);
		// });
	});
});	