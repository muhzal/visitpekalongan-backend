<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TempatRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	protected $aturan = [
		'tempat.nama' => 'required|max:100',
		'tempat.latitude' => 'required|numeric',
		'tempat.longitude' => 'required|numeric',
		'images' => 'mimes:jpeg,bmp,png|max:100000',
		'tempat.alamat' => 'string',
		'tempat.keterangan' => 'string',
		'tempat.publish' => 'boolean',
	];
	public function authorize() {
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return $this->aturan;
	}
}
