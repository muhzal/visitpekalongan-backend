<?php

namespace App\Providers;
use App\Models\Tempat;
use App\Models\Gambar;
use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\File;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Tempat::deleting(function($tempat){
            $tempat->updateImages(null);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
