<?php

namespace App\DataTables;

use App\Models\Spbu;
use Yajra\Datatables\Services\DataTable;

class SpbuDataTable extends DataTable
{
    // protected $printPreview  = 'path.to.print.preview.view';

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('action', 'tag.datatable-action-button')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $spbu = Spbu::with('tempat','tempat.gambars')->get();

        return $this->applyScopes($spbu);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction()
                    ->parameters([
                        'dom' => "<'btn-tambah'fr>tip",
                        'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
                        'responsive' => 'true',

                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'id',
            ['name' => 'tempat.nama', 'data'=>'tempat.nama', 'title'=>"Nama"],
            ['name' => 'tempat.alamat', 'data'=>'tempat.alamat', 'title'=>"Alamat"],                        
            'fasilitas',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'spbu';
    }
}
