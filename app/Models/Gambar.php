<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class Gambar extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
   		 'nama','tempat_id',
	];


	public static function imageStore($images, $model){
		$result 	= [];
		if ($images[0] != null) {						
			$path 		= public_path('images');
			$dir 		= $path.DIRECTORY_SEPARATOR.class_basename($model->tempatable).DIRECTORY_SEPARATOR;
	        File::exists($dir) or File::makeDirectory($dir);    
			for ($i=0; $i < count($images) ; $i++) { 
				$name = date('Ymds').$i.".".$images[$i]->getClientOriginalExtension();
				$images[$i]->move($dir, $name);
				$result[] = Gambar::create(['nama'=>$name, 'tempat_id'=>$model->id]);
			}		
			return $result;
		}

		return true;
	}

	public function deleteImage(){
		$file = $this->getFilePath();		
        if ( $this->delete() && File::delete($file)) {   	           	    
            return true;   
        }
	}


	private function getFilePath(){		

		$class      = class_basename($this->tempat->tempatable);        
        $path       = public_path('images').DIRECTORY_SEPARATOR.$class.DIRECTORY_SEPARATOR;                           
		$name    	= $this->attributes['nama'];
        $file   	= str_finish($path, $name);
        return $file;
	}

	public function tempat(){
		return $this->belongsTo('App\Models\Tempat');
	}

}
