<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Belanja extends Model {
	public $timestamps = true;
	protected $guarded = ['id'];
	protected $fillable = [ 'nama','jenis'];

	public function tempat() {
		return $this->morphOne('App\Models\Tempat', 'tempatable');
	}
}
