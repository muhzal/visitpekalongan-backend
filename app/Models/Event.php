<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model {
	public $timestamps = true;
	protected $fillable = [
		'waktu_mulai',
		'waktu_selesai',
		'tgl_mulai',
		'tgl_selesai',
	];
	protected $dates = [
		'tgl_mulai',
		'tgl_selesai',
		'created_at',
		'updated_at'
	];
	// protected $dateFormat = 'd/m/Y';	
	protected $with = ['tempat'];

	public function setTglMulaiAttribute($value) {
		$this->attributes['tgl_mulai'] = Carbon::createFromFormat('d/m/Y', $value);
	}
	// public function getTglMulaiAttribute($value) {	
	// 	return $value->format('d-m-Y');
	// }
	public function setTglSelesaiAttribute($value) {
		$this->attributes['tgl_selesai'] = Carbon::createFromFormat('d/m/Y', $value);
	}

	// public function getTglSelesaiAttribute($value) {
	// 	return Carbon::createFromFormat('Y-m-d', $value)->format('d-m-Y');
	// // }
	public function tempat() {
		return $this->morphOne('App\Models\Tempat', 'tempatable');
	}
}
