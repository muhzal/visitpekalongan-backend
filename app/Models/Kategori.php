<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model {
	public $timestamps = false;
	protected $table = 'kategories';
	protected $fillable = ['nama'];

	public function transportasi() {
		return $this->hasMany('App\Models\Transportasi');
	}

}
