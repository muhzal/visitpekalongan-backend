<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Atm extends Model {
	public $timestamps = true;
	protected $guarded = ['id'];
	protected $with = ['banks'];
	public function banks() {
		return $this->belongsToMany('App\Models\Bank');
	}
	public function tempat() {
		return $this->morphOne('App\Models\Tempat', 'tempatable');
	}
}
