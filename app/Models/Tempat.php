<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tempat extends Model {
	public $timestamps = true;
	protected $fillable = [
		'nama',
		'alamat',
		'latitude',
		'longitude',
		'keterangan',
		'publish',
		'tempatable_id',
		'tempatable_type',
		'user_id',
	];
	protected $with = ['gambars'];

	public function updateImages($imagesName = []) {
		if (count($imagesName) > 0) {
			$deletes = $this->gambars()->whereNotIn('nama', $imagesName);
			return $this->deleteFileImages($deletes->get());
		} else {
			return $this->deleteFileImages($this->gambars()->get());
		}
	}
	public function deleteFileImages($files) {
		if (isset($files)) {
			foreach ($files as $file) {
				$file->deleteImage();
			}
			return true;
		}
	}
	public function user() {
		return $this->belongsTo('App\User');
	}

	// public function reviews(){
	//     return $this->hasMany('App\Models\Review');
	// }

	public function gambars() {
		return $this->hasMany('App\Models\Gambar');
	}

	public function tempatable() {
		return $this->morphTo();
	}

}
