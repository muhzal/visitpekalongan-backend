<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    public $timestamps 		= false;   
	protected $fillable = ['nama'];

	public function atm(){
		return $this->belongsToMany('App\Models\Atm');
	}
}
