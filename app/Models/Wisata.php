<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wisata extends Model {
	public $timestamps = true;
	protected $guarded = ['id'];
	protected $fillable = [
		'jeniswisata_id',
		'waktu_buka',
		'waktu_tutup',
		'is_tiket',
		'pengelola',
		'telepon',
	];
	protected $with = ['tikets'];
	protected $appends = ['rate','fasilitaswisata'];

	public function getRateAttribute(){
		$count = $this->reviews()->getEager()->count();
		$sum = $this->reviews()->sum('rating');
		if ($count) {			
			return $sum/$count;		
		}
		return 0;
	}
	public function getFasilitaswisataAttribute(){
		return $this->fasilitas()->getEager();
	}
	public function setIsTiketAttribute($value) {
		if (empty($value)) {
			$value = 0;
		}
		$this->attributes['is_tiket'] = $value;

	}

	public function updateFasilitas($f = []) {
		$fnew = collect(array_diff($f, $this->fasilitas()->pluck('nama')->toArray()))->transform(function ($item) {return ['nama' => $item];})->toArray();
		$del = $this->fasilitas()->whereNotIn('nama', $f);
		if (isset($fnew)) {
			$this->fasilitas()->createMany($fnew);
		}
		if (isset($del)) {
			$del->delete();
		}
	}
	public function updateTikets($t) {
		$tikets = collect($t)->values();

		$array = [];
		foreach ($tikets as $value) {
			$array[] = $this->tikets()->updateOrCreate($value);
		}
		$ids = collect($array)->pluck('id');
		$delete = $this->tikets()->whereNotIn('id', $ids);

		$delete->delete();
	}

	public function tempat() {
		return $this->morphOne('App\Models\Tempat', 'tempatable');
	}
	public function jeniswisata() {
		return $this->belongsTo('App\Models\Jeniswisata');
	}
	public function fasilitas() {
		return $this->hasMany('App\Models\Fasilitaswisata');
	}
	public function tikets() {
		return $this->hasMany('App\Models\Tiket');
	}
	public function reviews() {
		return $this->hasMany('App\Models\Review');
	}
}
