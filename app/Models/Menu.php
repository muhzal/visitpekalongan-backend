<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $timestamps		= false;
    protected $fillable = [
        'nama',
        'restoran_jd',
    ];

    public function restoran(){
    	return $this->belongsTo('App\Models\Restoran');
    }

}
