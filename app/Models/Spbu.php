<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spbu extends Model {
	public $timestamps = true;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'fasilitas',
	];

	public function tempat() {
		return $this->morphOne('App\Models\Tempat', 'tempatable');
	}
}
