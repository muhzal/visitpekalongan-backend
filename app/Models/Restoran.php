<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restoran extends Model {
	public $timestamps = true;

	protected $fillable = [
		'waktu_buka',
		'waktu_tutup',
		'telepon',
		'harga_min',
		'harga_max',
	];
	protected $with = ['menus'];

	public function getWaktuBukaAttribute($value) {
		return substr($value, 0, 5);
	}
	public function getWaktuTutupAttribute($value) {
		return substr($value, 0, 5);
	}
	public function menus() {
		return $this->hasMany('App\Models\Menu');
	}
	public function tempat() {
		return $this->morphOne('App\Models\Tempat', 'tempatable');
	}
}
