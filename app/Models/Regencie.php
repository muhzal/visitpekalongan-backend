<?php

namespace App\Models;;

use Illuminate\Database\Eloquent\Model;

class Regencie extends Model
{
    protected $with = ['districts','districts.villages'];
	protected $hidden = [
	        	'id','province_id'
	    	];


    public function districts(){
    	return $this->hasMany('App\Models\District');
    }
}
