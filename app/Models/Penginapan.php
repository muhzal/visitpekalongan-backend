<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penginapan extends Model {
	public $timestamps = true;
	protected $fillable = [
		'fasilitas', 'telepon', 'harga_min', 'harga_max',
	];

	public function tempat() {
		return $this->morphOne('App\Models\Tempat', 'tempatable');
	}

}
