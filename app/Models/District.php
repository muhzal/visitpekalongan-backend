<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{	

	protected $hidden = [
	        	'id','regencie_id'
	    	];
    public function regencie(){
    	return $this->belongsTo('App\Models\Regencie');
    }
    public function villages(){
    	return $this->hasMany('App\Models\Village');
    }
}
