<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jenisbelanja extends Model
{
    public $timestamps		= false;
    protected $guarded		= ['id'];

    public function belanja(){
    	return $this->belongsToMany('App\Models\Belanja');
    }
	public function tempat(){
		return $this->morphMany('App\Models\Tempat');
	}
}
