<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
    public $timestamps		= false;
 	protected $guarded		= ['id'];
 	protected $fillable = [
 		'nama',
 		'operasional',
 		'wisata_id',
 		'harga',
 	];


 	public function wisata(){
 		return $this->belongsTo('App\Models\Wisata');
 	}
}
