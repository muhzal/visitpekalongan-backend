<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jeniswisata extends Model
{ 	
 	public function wisata(){
 		return $this->hasMany('App\Models\Tempat');
 	}
}
