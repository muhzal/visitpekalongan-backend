<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transportasi extends Model {
	public $timestamps = true;
	protected $guarded = ['id'];
	protected $fillable = [
		'nama',
		'telepon',
		'kategori_id',
	];
	protected $with = ['kategori'];

	public function tempat() {
		return $this->morphOne('App\Models\Tempat', 'tempatable');
	}

	public function kategori() {
		return $this->belongsTo('App\Models\Kategori');
	}

}
