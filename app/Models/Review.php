<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    
	public $timestamps		= true;
	protected $fillable 	= ['judul','review','rating','wisata_id','user_id','id'];
	public function user(){
		return $this->belongsTo('App\User');
	}
	// public function tempat(){
	// 	return $this->belongsTo('App\Models\Tempat');
	// }
	public function wisata(){
		return $this->belongsTo('App\Models\Wisata');
	}
}
