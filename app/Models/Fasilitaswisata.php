<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fasilitaswisata extends Model
{
	public $timestamps		= false;
 	protected $guarded		= ['id'];
 	
 	public function wisata(){
 		$this->belongsTo('App\Models\Wisata');
 	}
}
