<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operasional extends Model
{
    public $timestamps		= false;
 	protected $guarded		= ['id'];


 	public function tikets(){
 		return $this->hasMany('App\Models\Tiket');
 	}
}
