<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','level_id',
    ];
    protected $with = ['level'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','level_id',
    ];
    public function level(){
        return $this->belongsTo('App\Models\Level');
    }
    public function tempats(){
        return $this->hasMany('App\Models\Tempat');
    }
    public function reviews(){
        return $this->hasMany('App\Models\Review');
    }
}
