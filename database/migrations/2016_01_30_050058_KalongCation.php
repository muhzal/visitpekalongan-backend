<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class KalongCation extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('levels', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nama');
		});
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('email');
			$table->string('password');
			$table->boolean('active');
			$table->rememberToken();
			$table->integer('level_id')->unsigned();
			$table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('password_resets', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('token');
			$table->timestamps();
		});
		Schema::create('tempats', function ($table) {
			$table->increments('id');
			$table->string('nama', 50);
			$table->string('alamat');
			$table->double('latitude');
			$table->string('longitude');
			$table->longText('keterangan')->nullable();
			$table->boolean('publish');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->morphs('tempatable');
			$table->timestamps();
		});
		Schema::create('gambars', function ($table) {
			$table->increments('id');
			$table->string('nama');
			$table->integer('tempat_id')->unsigned();
			$table->foreign('tempat_id')->references('id')->on('tempats')->onDelete('cascade');
		});
		Schema::create('banks', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nama');
			$table->integer('code');
		});
		Schema::create('atms', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
		});
		Schema::create('atm_bank', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('atm_id')->unsigned();
			$table->integer('bank_id')->unsigned();
			$table->foreign('atm_id')->references('id')->on('atms')->onDelete('cascade');
			$table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');
		});
		Schema::create('spbus', function (Blueprint $table) {
			$table->increments('id');
			$table->string('fasilitas')->nullable();
			$table->timestamps();
		});
		Schema::create('events', function (Blueprint $table) {
			$table->increments('id');
			$table->time('waktu_mulai');
			$table->time('waktu_selesai');
			$table->timestamp('tgl_selesai')->nullable();
			$table->timestamp('tgl_mulai')->nullable();
			$table->timestamps();
		});
		Schema::create('penginapans', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('telepon')->nullable();
			$table->integer('harga_min')->nullable();
			$table->integer('harga_max')->nullable();
			$table->string('fasilitas')->nullable();
			$table->timestamps();
		});
		Schema::create('restorans', function (Blueprint $table) {
			$table->increments('id');
			$table->time('waktu_buka')->nullable();
			$table->time('waktu_tutup')->nullable();
			$table->integer('telepon')->nullable();
			$table->integer('harga_min')->nullable();
			$table->integer('harga_max')->nullable();
			$table->timestamps();
		});
		Schema::create('menus', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nama')->nullable();
			$table->integer('restoran_id')->unsigned();
			$table->foreign('restoran_id')->references('id')->on('restorans')->onDelete('cascade');
		});
		Schema::create('kategories', function (Blueprint $table) {
			$table->increments('id');
			$table->char('nama', 50)->nullable();
		});
		Schema::create('transportasis', function (Blueprint $table) {
			$table->increments('id');
			$table->char('telepon', 20)->nullable();
			$table->integer('kategori_id')->unsigned();
			$table->foreign('kategori_id')->references('id')->on('kategories')->onDelete('cascade');
			$table->timestamps();
		});

		Schema::create('belanjas', function (Blueprint $table) {
			$table->increments('id');
			$table->string('jenis')->nullable();
			$table->timestamps();
		});
		// Schema::create('jenisbelanjas', function (Blueprint $table) {
		// 	$table->increments('id');
		// 	$table->string('nama');
		// });
		// Schema::create('belanja_jenisbelanja', function (Blueprint $table) {
		// 	$table->increments('id');
		// 	$table->integer('belanja_id')->unsigned();
		// 	$table->integer('jenisbelanja_id')->unsigned();
		// 	$table->foreign('belanja_id')->references('id')->on('belanjas')->onDelete('cascade');
		// 	$table->foreign('jenisbelanja_id')->references('id')->on('jenisbelanjas')->onDelete('cascade');
		// });
		Schema::create('jeniswisatas', function (Blueprint $table) {
			$table->increments('id');
			$table->char('nama', 20);
		});
		Schema::create('wisatas', function (Blueprint $table) {
			$table->increments('id');
			$table->boolean('is_tiket');
			$table->time('waktu_buka')->nullable();
			$table->time('waktu_tutup')->nullable();
			$table->string('pengelola')->nullable();
			$table->char('telepon', 20)->nullable();
			$table->integer('jeniswisata_id')->unsigned();
			$table->foreign('jeniswisata_id')->references('id')->on('jeniswisatas')->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('fasilitaswisatas', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nama');
			$table->integer('wisata_id')->unsigned();
			$table->foreign('wisata_id')->references('id')->on('wisatas')->onDelete('cascade');
		});
		Schema::create('tikets', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nama')->nullable();
			$table->integer('harga')->nullable();
			$table->string('operasional')->nullable();
			$table->integer('wisata_id')->unsigned();
			$table->foreign('wisata_id')->references('id')->on('wisatas')->onDelete('cascade');
		});
		Schema::create('reviews', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('rating');
			$table->string('review');
			$table->integer('user_id')->unsigned();
			$table->integer('wisata_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('wisata_id')->references('id')->on('wisatas')->onDelete('cascade');
			$table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('reviews');
		Schema::dropIfExists('tikets');
		Schema::dropIfExists('fasilitaswisatas');
		Schema::dropIfExists('wisatas');
		Schema::dropIfExists('jeniswisatas');
		Schema::dropIfExists('belanjas');
		Schema::dropIfExists('transportasis');
		Schema::dropIfExists('kategories');
		Schema::dropIfExists('menus');
		Schema::dropIfExists('restorans');
		Schema::dropIfExists('penginapans');
		Schema::dropIfExists('events');
		Schema::dropIfExists('spbus');
		Schema::dropIfExists('atm_bank');
		Schema::dropIfExists('banks');
		Schema::dropIfExists('atms');
		Schema::dropIfExists('gambars');
		Schema::dropIfExists('tempats');
		Schema::dropIfExists('password_resets');
		Schema::dropIfExists('users');
		Schema::dropIfExists('levels');
	}
}
