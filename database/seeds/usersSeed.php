<?php

use Illuminate\Database\Seeder;

class usersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Tabel Level
    	DB::table('levels')->delete();
        DB::table('levels')->insert([
        	['id' => '1', 'nama' => 'admin'],
        	['id' => '2', 'nama' => 'member'],
        ]);	        
        //Tabel user
        DB::table('users')->delete();
        DB::table('users')->insert(array(
        	['id' => 1, 'nama' => 'Muhammad Rizal', 'email'=>'rizalmx@gmail.com', 'password' => bcrypt('admin'), 'level_id' => '1']
        ));

        //JENIS Belanja
        DB::table('jenisbelanjas')->delete();
        DB::table('jenisbelanjas')->insert(array(
        	['id' => 1, 'nama' => 'Batik'],
        	['id' => 2, 'nama' => 'Makanan Ringan'],
        	['id' => 3, 'nama' => 'Grosir'],
        ));
        //JENIS MENU
        DB::table('jeniswisatas')->delete();
        DB::table('jeniswisatas')->insert(array(
        	['id' => 1, 'nama' => 'alam'],
        	['id' => 2, 'nama' => 'pantai'],
        	['id' => 3, 'nama' => 'religi'],
        ));
        DB::table('banks')->delete();
        DB::table('banks')->insert(array(
            ['id' => 1, 'nama' => 'BCA'],
            ['id' => 2, 'nama' => 'Mandiri'],
            ['id' => 3, 'nama' => 'BNI'],
            ['id' => 4, 'nama' => 'BRI'],
            ['id' => 5, 'nama' => 'Bank Jateng'],
        ));
        DB::table('atms')->delete();
        DB::table('atms')->insert(array(
            ['id' => 1],
        ));
        DB::table('atm_bank')->delete();
        DB::table('atm_bank')->insert(array(
            ['id' => 1, 'atm_id' => '1', 'bank_id' => '1'],
            ['id' => 3, 'atm_id' => '1', 'bank_id' => '2'],
            ['id' => 2, 'atm_id' => '1', 'bank_id' => '3'],
        ));
        DB::table('spbus')->delete();
        DB::table('spbus')->insert(array(
            ['id' => 1, 'fasilitas' => 'WC Umum'],
        ));
        DB::table('tempats')->delete();
        DB::table('tempats')->insert(array(
            ['id' => 1,
             'nama' => 'Siwalan',
             'alamat' => "Desa Siwalan Kecamatan SIwalan",
             'latitude' => -6.888701,
             'longitude' => 109.668289,
             'keterangan' => "ini keterangan tempat",
             'publish' => 1,
             'user_id' => 1,
             'tempatable_id' => 1,
             'tempatable_type' => 'App\Models\Atm',             
            ],
            ['id' => 2,
             'nama' => 'Buaran',
             'alamat' => "Desa Kertijayan, Kecamatan Buaran",
             'latitude' => -6.888701,
             'longitude' => 109.668289,
             'keterangan' => "ini keterangan tempat isi bensin",
             'publish' => 1,
             'user_id' => 1,
             'tempatable_id' => 1,
             'tempatable_type' => 'App\Models\Spbu',             
            ],
        ));

    }
}
